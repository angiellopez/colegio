<?php $__env->startSection('content'); ?>

<?php
        if(isset($ciclo)){
            $nombre = $ciclo->nombre;
            $fecha_inicio = $ciclo->fecha_inicio;
            $fecha_fin = $ciclo->fecha_fin;
            $anio = $ciclo->anio;
        }else{
            $nombre = "";
            $fecha_inicio = "";
            $fecha_fin = "";
            $anio = "";
        }
    ?>






<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                </div>
                <div class="card-body card-block">

                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <h6>Para continuar debe corregir los siguientes errores:</h6>
                            <br>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endif; ?>

                    <form method="POST" id = "formulario" action="<?php echo e($accion); ?>" files="true" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e($metodo); ?>

                        <div class = "row">
                            <div class = "col-md-6">
                                <div class="form-group">
                                    <label>Nombre </label>
                                    <input required type="text" class="form-control" name="nombre" id="nombre" value="<?php echo e(old('nombre',$nombre)); ?>">
                                </div>
                            </div>
                            <div class = "col-md-6">
                                <div class="form-group">
                                    <label>Año </label>
                                    <select name="anio" id="anio" class = "form-control">
                                        <option value="2019">2019</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "col-md-6">
                                <div class="form-group">
                                    <label>Fecha Inicio </label>
                                    <input required type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" value="<?php echo e(old('fecha_inicio',$fecha_inicio)); ?>">
                                </div>
                            </div>
                            <div class = "col-md-6">
                                <div class="form-group">
                                    <label>Fecha Fin </label>
                                    <input required type="date" class="form-control" name="fecha_fin" id="fecha_fin" value="<?php echo e(old('fecha_fin',$fecha_fin)); ?>">
                                </div>
                            </div>
                            
                        </div>
                        <span  style = "display:none;" id = "alert-busqueda">
                                Cargando...
                                <img style="width: 30px;" src="<?php echo e(asset('uploads/cargando.gif')); ?>" />
                            </span>

                            <?php if(session()->has('mensaje')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session()->get('mensaje')); ?>

                                </div>
                            <?php endif; ?>
                            <div class="tile-footer">
                                <button type="submit" id = "btn-enviar" class="btn btn-sm btn-primary"><?php echo e($boton); ?></button>
                                <a href="<?php echo e(route('ciclos.index')); ?>"><button class="btn btn-sm btn-success" type = "button">Regresar</button></a>
                            </div>
                    </form>                                 



                </div>
            </div>
        </div>
</div>








<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function(){
            $("#alert-busqueda").show();
        })

        $('#btn_imagen').click(function(event){
            event.preventDefault();
            $('#urlimagen').click();
        })

        $('#urlimagen').change(function() {
            if($("#urlimagen").val() != ''){
                var file = $('#urlimagen')[0].files[0].name;
                $("#btn_imagen").html("Seleccionado");
            }else{
                $("#btn_imagen").html("Seleccionar");
            }
        });

        $('#btn_archivo').click(function(event){
            event.preventDefault();
            $('#urlarchivo').click();
        })

        $('#urlarchivo').change(function() {
            if($("#urlarchivo").val() != ''){
                var file = $('#urlarchivo')[0].files[0].name;
                $("#btn_archivo").html("Seleccionado");
            }else{
                $("#btn_archivo").html("Seleccionar");
            }
        });
    });
    

</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\colegio\resources\views/ciclos/create.blade.php ENDPATH**/ ?>