<?php $__env->startSection('contenido'); ?>
    <div class = "alert alert-danger"><i class = "fa fa-info-circle"></i> ¡El lugar al que intenta acceder no existe!</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout_defecto', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\colegio\resources\views/errors/404.blade.php ENDPATH**/ ?>