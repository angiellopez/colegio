<?php if(isset($personas_tabla)): ?>
    <?php if($personas_tabla->isNotEmpty()): ?>
    <div class = "table-responsive">
        <table class="table table-show table-hover table-bordered " id="sampleTable">
            

            <!--    COMIENZA CONTENIDO TABLA   -->
            <thead>
                <tr>
                    <th >DNI</th>
                    <th >Nombres</th>
                    <th >Apellidos</th>
                    <th >Celular</th>
                    <th >Email</th>
                    <th >Tipo Persona</th>
                </tr>
                </thead> 
                <tbody>
                <?php $__currentLoopData = $personas_tabla; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $persona): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr ondblclick= "agregar_persona('<?php echo e($persona->identificacion); ?>', event)">
                    <td><?php echo e($persona->identificacion); ?></td>
                    <td><?php echo e($persona->nombres); ?></td>
                    <td><?php echo e($persona->apellidos); ?></td>
                    <td><?php echo e($persona->celular); ?></td>
                    <td><?php echo e($persona->correo); ?></td>
                    <td><?php echo e($persona->tipo_persona_nombre($persona->tipo_persona)); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>


            <!--    FIN CONTENIDO TABLA   -->

        </table>
        <?php if(session()->has('mensaje')): ?>
            <div class="alert alert-success">
                <?php echo e(session()->get('mensaje')); ?>

            </div>
        <?php endif; ?>
    </div>
    <?php else: ?>
        <p>No hay personas registrados.</p>
    <?php endif; ?>
<?php endif; ?>
<?php /**PATH C:\xampp\htdocs\colegio\resources\views/cargar_tabla.blade.php ENDPATH**/ ?>