<!DOCTYPE html>
<html lang="es">

<head>
	<!-- Required meta tags-->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="au theme template">
	<meta name="author" content="Hau Nguyen">
	<meta name="keywords" content="au theme template">

	<!-- Title Page-->
	<title>Liceo Santa Teresita</title>

	<!-- Fontfaces CSS-->
	<link href="<?php echo e(asset('docs/css/font-face.css')); ?>" rel="stylesheet" media="all">
	<link href="<?php echo e(asset('docs/vendor/font-awesome-4.7/css/font-awesome.min.css')); ?>" rel="stylesheet" media="all">
	<link href="<?php echo e(asset('docs/vendor/font-awesome-5/css/fontawesome-all.min.css')); ?>" rel="stylesheet" media="all">
	<link href="<?php echo e(asset('docs/vendor/mdi-font/css/material-design-iconic-font.min.css')); ?>" rel="stylesheet" media="all">

	<!-- Bootstrap CSS-->
	<link href="<?php echo e(asset('docs/vendor/bootstrap-4.1/bootstrap.min.css')); ?>" rel="stylesheet" media="all">

	<!-- Vendor CSS-->
	<link href="<?php echo e(asset('docs/vendor/animsition/animsition.min.css')); ?>" rel="stylesheet" media="all">
	<link href="<?php echo e(asset('docs/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')); ?>" rel="stylesheet" media="all">
	<link href="<?php echo e(asset('docs/vendor/wow/animate.css')); ?>" rel="stylesheet" media="all">
	<link href="<?php echo e(asset('docs/vendor/css-hamburgers/hamburgers.min.css')); ?>" rel="stylesheet" media="all">
	<link href="<?php echo e(asset('docs/vendor/slick/slick.css')); ?>" rel="stylesheet" media="all">
	<link href="<?php echo e(asset('docs/vendor/select2/select2.min.css')); ?>" rel="stylesheet" media="all">
	<link href="<?php echo e(asset('docs/vendor/perfect-scrollbar/perfect-scrollbar.css')); ?>" rel="stylesheet" media="all">

	<!-- Main CSS-->
  <link href="<?php echo e(asset('docs/css/theme.css')); ?>" rel="stylesheet" media="all">
	<link href="<?php echo e(asset('docs/css/estilos.css')); ?>" rel="stylesheet" media="all">
	<script src="<?php echo e(asset('docs/vendor/jquery-3.2.1.min.js')); ?>"></script>
  

</head>

<body class="">
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
		<header class="header-mobile d-block d-lg-none">
			<div class="header-mobile__bar">
				<div class="container-fluid">
					<div class="header-mobile-inner">
						<a href="#"  class="logo font-bold" style="text-align:center;">
							<!--<img src="images/icon/logo.png" alt="Colegio" />-->
							LICEO SANTA TERESITA
						</a>
						<button class="hamburger hamburger--slider" type="button">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</div>
			</div>
			<nav class="navbar-mobile">
				<div class="container-fluid">
					<ul class="navbar-mobile__list list-unstyled">
						<li>
							<a href="chart.html">
								<i class="fas fa-chart-bar"></i>Charts</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- END HEADER MOBILE-->

		<!-- MENU SIDEBAR-->
		<aside class="menu-sidebar d-none d-lg-block">
			<div class="logo">
				<img src="<?php echo e(asset('uploads/logo.jpg')); ?>" style = "width:40px;" alt="COLEGIO SANTA TERESITA" />
				<a href="<?php echo e(route('principal.index')); ?>" class = "font-bold" style="text-align:center;">
					LICEO SANTA TERESITA
				</a>
			</div>
			<div class="menu-sidebar__content js-scrollbar1">
				<nav class="navbar-sidebar">
					<ul class="list-unstyled navbar__list">
						<li class = "<?php if(isset($controlador) && $controlador == 'principal') echo 'active'; ?>">
							<a href="<?php echo e(route('principal.index')); ?>"><i class="fas fa-home"></i>Principal</a>
						</li>
						<li class = "<?php if(isset($controlador) && $controlador == 'docentes') echo 'active'; ?>">
							<a href="<?php echo e(route('personas.listar',['tipo_persona'=>'1','nom_persona'=>'docentes'])); ?>"><i class="fas fa-user"></i>Docentes</a>
						</li>
						<li class = "<?php if(isset($controlador) && $controlador == 'estudiantes') echo 'active'; ?>">
							<a href="<?php echo e(route('personas.listar',['tipo_persona'=>'2','nom_persona'=>'estudiantes'])); ?>"><i class="fas fa-users"></i>Estudiantes</a>
						</li>
						<li class = "<?php if(isset($controlador) && $controlador == 'funcionarios') echo 'active'; ?>">
							<a href="<?php echo e(route('personas.listar',['tipo_persona'=>'3','nom_persona'=>'funcionarios'])); ?>"><i class="fas fa-users"></i>Funcionarios</a>
						</li>
						<li class = "<?php if(isset($controlador) && $controlador == 'matriculas') echo 'active'; ?>">
							<a href="<?php echo e(route('matriculas.index')); ?>"><i class="fas fa-book"></i>Matriculas</a>
						</li>
						<li class = "<?php if(isset($controlador) && $controlador == 'configuracion') echo 'active'; ?>">
							<a href="<?php echo e(route('configuracion.index')); ?>"><i class="fas fa-cog"></i>Configuración</a>
						</li>
					</ul>
				</nav>
			</div>
		</aside>
		<!-- END MENU SIDEBAR-->

		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
			<header class="header-desktop">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="header-wrap float-right">
							
							<div class="header-button">
								<div class="account-wrap">
									<div class="account-item clearfix js-item-menu">
										
										<div class="content">
											<a class="js-acc-btn" href="#"><?php if (Auth::id() != null) echo Auth::user()->persona->nombres." ".Auth::user()->persona->apellidos;?></a>
										</div>
										<div class="account-dropdown js-dropdown">
											
											<div class="account-dropdown__body">
												<div class="account-dropdown__item">
													<a href="#">
														<i class="zmdi zmdi-account"></i>Perfil</a>
												</div>
												<div class="account-dropdown__item">
													<a href="#">
														<i class="zmdi zmdi-settings"></i>Configuración</a>
												</div>
											</div>
											<div class="account-dropdown__footer">
												<a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
													document.getElementById('logout-form').submit();"><i class="zmdi zmdi-power"></i>Salir</a>
											</div>
										</div>
										<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
											<?php echo e(csrf_field()); ?>

										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<!-- END HEADER DESKTOP-->

			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<?php echo $__env->yieldContent('content'); ?>
					</div>
				</div>
			</div>

			<!-- modal large -->
			





				<!-- Modal -->
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document" style = "min-width:70%!important;">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Personas</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					
					
					<div class="modal-body">
							<div class = "row">
								<div class = "col-md-6">
										<div class="input-group">
												<input placeholder = "Buscar" class = "form-control" type="text"  name="identificacion" id="identificacion">
												<div class="input-group-append">
													<span style = "cursor:pointer;" class="input-group-text">
														<i class = "fa fa-search"></i>
													</span>
												</div>
										</div>
								</div>
							</div>
							<br>
							<div class = "contenido-modal">
									Cargando...
									<img style="width: 30px;" src="<?php echo e(asset('uploads/cargando.gif')); ?>" />
							</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
					</div>
					</div>
				</div>
				</div>


			<!-- end modal large -->




		</div>
		<!-- END PAGE CONTAINER-->

	</div>

	<!-- Jquery JS-->
	<!-- Bootstrap JS-->
	<script src="<?php echo e(asset('docs/vendor/bootstrap-4.1/popper.min.js')); ?>"></script>
	<script src="<?php echo e(asset('docs/vendor/bootstrap-4.1/bootstrap.min.js')); ?>"></script>
	<!-- Vendor JS       -->
	<script src="<?php echo e(asset('docs/vendor/slick/slick.min.js')); ?>">
	</script>
	<script src="<?php echo e(asset('docs/vendor/wow/wow.min.js')); ?>"></script>
	<script src="<?php echo e(asset('docs/vendor/animsition/animsition.min.js')); ?>"></script>
	<script src="<?php echo e(asset('docs/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')); ?>">
	</script>
	<script src="<?php echo e(asset('docs/vendor/counter-up/jquery.waypoints.min.js')); ?>"></script>
	<script src="<?php echo e(asset('docs/vendor/counter-up/jquery.counterup.min.js')); ?>">
	</script>
	<script src="<?php echo e(asset('docs/vendor/circle-progress/circle-progress.min.js')); ?>"></script>
	<script src="<?php echo e(asset('docs/vendor/perfect-scrollbar/perfect-scrollbar.js')); ?>"></script>
	<script src="<?php echo e(asset('docs/vendor/chartjs/Chart.bundle.min.js')); ?>"></script>
	<script src="<?php echo e(asset('docs/vendor/select2/select2.min.js')); ?>">
	</script>

	<!-- Main JS-->
	<script src="<?php echo e(asset('docs/js/main.js')); ?>"></script>

</body>

</html>
<!-- end document-->
<?php /**PATH Y:\xamppnuevo\htdocs\colegio\resources\views/layout_admin.blade.php ENDPATH**/ ?>