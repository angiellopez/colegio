<?php $__env->startSection('content'); ?>





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                </div>
                <div class="card-body card-block">



                <div class = "table-responsive">
                    <table class="table table-show table-sinbordes table-list table-striped table-bordered">
                            <tr>
                                <td class = "font-bold">IDENTIFICACIÓN</td>
                                <td><?php echo e($user->persona->identificacion); ?></td>
                            </tr>
                            <tr>
                                <td class = "font-bold">NOMBRES</td>
                                <td><?php echo e($user->persona->nombres); ?></td>
                            </tr>
                            <tr>
                                <td class = "font-bold">CELULAR</td>
                                <td><?php echo e($user->persona->celular); ?></td>
                            </tr>
                            <tr>
                                <td class = "font-bold">EMAIL</td>
                                <td><?php echo e($user->persona->correo); ?></td>
                            </tr>
                            <tr>
                                <td class = "font-bold">USUARIO</td>
                                <td><?php echo e($user->user); ?></td>
                            </tr>
                            <tr>
                                <td class = "font-bold">ESTADO</td>
                                <td><?php echo e($user->estado == '0' ? 'INACTIVO':'ACTIVO'); ?></td>
                            </tr>
                            <tr>
                                <td class = "font-bold">PERFIL</td>
                                <td><?php echo e($user->nivel_nombre($user->nivel)); ?></td>
                            </tr>
                    </table>
                </div>
                <br>
                <a href="<?php echo e(route('users.index')); ?>"><button class="btn btn-success btn-sm" type = "button">Regresar</button></a>


                
                </div>
            </div>
        </div>
</div>    

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Y:\xamppnuevo\htdocs\colegio\resources\views/users/show.blade.php ENDPATH**/ ?>