<?php $__env->startSection('content'); ?>





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                </div>
                <div class="card-body card-block">



                <div class = "table-responsive">
                    <table class="table table-show table-sinbordes table-list table-striped table-bordered">
                            <tr>
                                <td class = "font-bold">CICLO</td>
                                <td><?php echo e($matricula->ciclo->nombre); ?> :: <?php echo e($matricula->ciclo->anio); ?></td>
                            </tr>
                            <tr>
                                <td class = "font-bold">ESTUDIANTE</td>
                                <td><?php echo e($matricula->estudiante->apellidos); ?> <?php echo e($matricula->estudiante->nombres); ?></td>
                            </tr>
                            <tr>
                                <td class = "font-bold">FECHA MATRICULA</td>
                                <td><?php echo e($matricula->fecha_inicio); ?></td>
                            </tr>
                            <tr>
                                <td class = "font-bold">FECHA REGISTRO</td>
                                <td><?php echo e($matricula->created_at); ?></td>
                            </tr>
                    </table>
                </div>
                <br>
                <a href="<?php echo e(route('matriculas.index')); ?>"><button class="btn btn-success btn-sm" type = "button">Regresar</button></a>


                
                </div>
            </div>
        </div>
</div>    

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\colegio\resources\views/matriculas/show.blade.php ENDPATH**/ ?>