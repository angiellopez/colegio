<?php if(isset($ciudades)): ?>
  <option value="" selected disable>SELECCIONE</option>
  <?php if(!empty($ciudades)): ?>
    <?php $__currentLoopData = $ciudades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ciudad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <option value="<?php echo e($ciudad->id); ?>"><?php echo e(strtoupper($ciudad->titulo)); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
<?php endif; ?>


<?php if(isset($departamentos)): ?>
  <option value="" selected disable>SELECCIONE</option>
  <?php if(!empty($departamentos)): ?>
    <?php $__currentLoopData = $departamentos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $departamento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <option value="<?php echo e($departamento->id); ?>"><?php echo e(strtoupper($departamento->titulo)); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
<?php endif; ?>
<?php /**PATH Y:\xamppnuevo\htdocs\colegio\resources\views/cargar_select.blade.php ENDPATH**/ ?>