<?php $__env->startSection('content'); ?>

<?php
        if(isset($persona)){
            $persona_id = $persona->id;
            $nombres = $persona->nombres;
            $apellidos = $persona->apellidos;
            $celular = $persona->celular;
            $correo = $persona->correo;
            $identificacion = $persona->identificacion;
            $fecha = $persona->date_new;
            $ciclo_id = $matricula->ciclo_id;
            $fecha_inicio = $matricula->fecha_inicio;

            
            
            
            
        }else{
            $persona_id ="";
            $nombres ="";
            $apellidos ="";
            $celular ="";
            $correo ="";
            $identificacion = "";
            $fecha ="";
            $ciclo_id ="";
            $fecha_inicio ="";

        }
    ?>


<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                </div>
                <div class="card-body card-block">

                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <h6>Para continuar debe corregir los siguientes errores:</h6>
                            <br>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endif; ?>

                    <form method="POST" id = "formulario" action="<?php echo e($accion); ?>">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e($metodo); ?>

                        <div class = "row">
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Identificación Estudiante</label>
                                    <?php if(isset($persona)): ?>
                                    <input readonly type="text" class="form-control" value="<?php echo e(old('identificacion',$identificacion)); ?>">
                                    <?php else: ?>
                                    <div class="input-group">
                                        <input required type="text" class="form-control" name="cedula" id="cedula" value="<?php echo e(old('cedula',$identificacion)); ?>">
                                        <div id = "btn_buscar" class="input-group-append" data-toggle="modal" data-target="#exampleModal">
                                            <span style = "cursor:pointer;" class="input-group-text">
                                                <i class = "fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>   
                        </div>

                        <div class = "row">
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="hidden" id="persona_id" name="persona_id" value="<?php echo e(old('persona_id',$persona_id)); ?>">
                                    <input readonly type="text" id = "nombre_persona" name = "nombre_persona" class="form-control" value="<?php echo e(old('nombre_persona',$nombres)); ?>">
                                </div>
                            </div>   
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input readonly type="text" id = "apellido_persona" name = "apellido_persona" class="form-control" value="<?php echo e(old('apellido_persona',$apellidos)); ?>">
                                </div>       
                            </div> 
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Celular</label>
                                    <input readonly type="text" id = "celular" name = "celular" class="form-control" value="<?php echo e(old('celular',$celular)); ?>">
                                </div>
                            </div> 
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Correo Electrónico</label>
                                    <input readonly type="mail" id = "correo" name = "correo" class="form-control" value="<?php echo e(old('correo',$correo)); ?>">
                                </div>       
                            </div>  
                        </div>
                        <div class = "row">
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Ciclo</label>
                                    <select class="form-control" name="ciclo_id" id="ciclo_id" >
                                        <option value="" disabled selected>SELECCIONE</option>
                                        <?php $__currentLoopData = $ciclos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ciclo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($ciclo->id); ?>" <?php echo e(old('ciclo_id', $ciclo_id) == $ciclo->id ? 'selected' : ''); ?>><?php echo e($ciclo->nombre); ?> :: <?php echo e($ciclo->anio); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class = "col-md-3">
                                    <div class="form-group">
                                        <label>Fecha Inicio </label>
                                        <input required type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" value="<?php echo e(old('fecha_inicio',$fecha_inicio)); ?>">
                                    </div>
                                </div>
                        </div>

                        
                        <br>

                            <span  style = "display:none;" id = "alert-busqueda">
                                Cargando...
                                <img style="width: 30px;" src="<?php echo e(asset('uploads/cargando.gif')); ?>" />
                            </span>

                            <?php if(session()->has('mensaje')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session()->get('mensaje')); ?>

                                </div>
                            <?php endif; ?>
                            <?php if(session()->has('alerta')): ?>
                                <div class="alert alert-danger">
                                    <?php echo e(session()->get('alerta')); ?>

                                </div>
                            <?php endif; ?>
                            <div class="tile-footer">
                                <button type="submit" id = "btn-enviar" class="btn btn-sm btn-primary"><?php echo e($boton); ?></button>
                                <a href="<?php echo e(route('matriculas.index')); ?>"><button class="btn btn-sm btn-success" type = "button">Regresar</button></a>
                            </div>
                    </form>                                  



                </div>
            </div>
        </div>
</div>




<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function(event){
            $("#alert-busqueda").show();
            
        })

        $("#cedula").blur(function(){
            id = $("#cedula").val();
            buscar_persona(id);
        });

        $("#btn_buscar").click(function(){
            $.ajax({
                url: "<?php echo e(route('personas.cargar_personas')); ?>",
                method: 'POST',
                data: {'tipo_persona':'2',"_token": "<?php echo e(csrf_token()); ?>"},
                success: function(data) {
                    $("#exampleModal .modal-body .contenido-modal").html('');
                    $("#exampleModal .modal-body .contenido-modal").html(data.options);
                }
            });
        });
        $("#identificacion").keypress(function(){
            id = $("#identificacion").val();
            buscar_personas(id);
        });
        $("#identificacion").blur(function(){
            id = $("#identificacion").val();
            
            buscar_personas(id);
        });

    });
    


    function buscar_personas(id){
        $.ajax({
            url: "<?php echo e(route('personas.filtrar_tabla')); ?>",
            method: 'POST',
            data: {'id':id,"_token": "<?php echo e(csrf_token()); ?>"},
            beforeSend: function() {
                //alert("buscando");
            },
            success: function(data) {
                $("#exampleModal .modal-body .contenido-modal").html('');
                $("#exampleModal .modal-body .contenido-modal").html(data.options);
            }
        });
    }

    function buscar_persona(id){

        $.ajax({
            url: "<?php echo e(route('personas.buscar')); ?>",
            method: 'POST',
            data: {'id':id,"_token": "<?php echo e(csrf_token()); ?>"},
            beforeSend: function() {
            },
            success: function(data) {
                $("#persona_id").val(data.id_persona);
                $("#cedula").val(id);
                $("#nombre_persona").val(data.nombres);
                $("#apellido_persona").val(data.apellidos);
                $("#cargo_persona").val(data.cargo);
                $("#correo").val(data.correo);
                $("#celular").val(data.celular);
            }
        });
    } 
    function agregar_persona(cedula,event){
        event.preventDefault();
        buscar_persona(cedula);
        $("#exampleModal").modal("toggle");

    }


</script>
<?php $__env->stopSection(); ?>














<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\colegio\resources\views/matriculas/create.blade.php ENDPATH**/ ?>