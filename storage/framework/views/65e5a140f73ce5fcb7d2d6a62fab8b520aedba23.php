<?php $__env->startSection('content'); ?>



<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                </div>
                <div class="card-body card-block">

                    <div class = "row">
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="<?php echo e(route('configuracion.empresa')); ?>" >
                                        <span class="desc configuracion">Empresa</span>
                                        <div class="icon">
                                            <i class="fa fa-home"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="<?php echo e(route('users.index')); ?>" >
                                        <span class="desc configuracion">Usuarios</span>
                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="<?php echo e(route('cargos.index')); ?>" >
                                        <span class="desc configuracion">Cargos</span>
                                        <div class="icon">
                                            <i class="fa fa-list"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                    </div>  

                    <div class = "row">
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="<?php echo e(route('materias.index')); ?>" >
                                        <span class="desc configuracion">Materias</span>
                                        <div class="icon">
                                            <i class="fa fa-home"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="<?php echo e(route('ciclos.index')); ?>" >
                                        <span class="desc configuracion">Ciclos</span>
                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="<?php echo e(route('asignaciones.index')); ?>" >
                                        <span class="desc configuracion">Asignaciones</span>
                                        <div class="icon">
                                            <i class="fa fa-list"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                    </div>  
                    
                </div>
            </div>
        </div>
</div>    








<?php $__env->stopSection(); ?>



<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Y:\xamppnuevo\htdocs\colegio\resources\views/configuracion/index.blade.php ENDPATH**/ ?>