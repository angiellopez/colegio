<?php $__env->startSection('content'); ?>

<?php
        if(isset($persona)){
            $tipo_documento_id = $persona->tipo_documento_id;
            $identificacion = $persona->identificacion;
            $nombres = $persona->nombres;
            $apellidos = $persona->apellidos;
            $departamento_id = $persona->departamento_id;
            $ciudad_id = $persona->ciudad_id;
            $direccion = $persona->direccion;
            $barrio = $persona->barrio;
            $celular = $persona->celular;
            $correo = $persona->correo;
        }else{
            $tipo_documento_id = "";
            $identificacion = "";
            $nombres = "";
            $apellidos = "";
            $area_id = "";
            $pais_id = "";
            $departamento_id = "";
            $ciudad_id = "";
            $direccion = "";
            $barrio = "";
            $celular = "";
            $correo = "";
        }
    ?>





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                </div>
                <div class="card-body card-block">

                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <h6>Para continuar debe corregir los siguientes errores:</h6>
                            <br>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endif; ?>

                    <form method="POST" id = "formulario" action="<?php echo e($accion); ?>" files="true" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e($metodo); ?>

                        <div class = "row">

                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Tipo Documento</label>
                                    <select class="form-control" name="tipo_documento_id" id="tipo_documento_id" required aria-required="true">
                                        <option value="" disabled selected>SELECCIONE</option>
                                        <?php $__currentLoopData = $tipo_documentos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tipo_documento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($tipo_documento->id); ?>" <?php echo e(old('tipo_documento_id', $tipo_documento_id) == $tipo_documento->id ? 'selected' : ''); ?>><?php echo e($tipo_documento->titulo); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Identificación </label>
                                    <input required type="number" class="form-control" name="identificacion" id="identificacion" value="<?php echo e(old('identificacion',$identificacion)); ?>">
                                </div>
                            </div>
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Nombres </label>
                                    <input required type="text" class="form-control" name="nombres" id="nombres" value="<?php echo e(old('nombres',$nombres)); ?>">
                                </div>
                            </div>
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Apellidos </label>
                                    <input required type="text" class="form-control" name="apellidos" id="apellidos" value="<?php echo e(old('apellidos',$apellidos)); ?>">
                                </div>
                            </div>
                        </div>
                        <div class = "row">
                            
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Departamento</label>
                                    <select class="form-control" name="departamento_id" id="departamento_id" required aria-required="true">
                                        <option value="" disabled selected>SELECCIONE</option>
                                        <?php $__currentLoopData = $departamentos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $departamento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($departamento->id); ?>" <?php echo e(old('departamento_id', $departamento_id) == $departamento->id ? 'selected' : ''); ?>><?php echo e(strtoupper($departamento->titulo)); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Ciudad</label>
                                    <select class="form-control" name="ciudad_id" id="ciudad_id" required aria-required="true">
                                        <option value="" disabled selected>SELECCIONE</option>
                                        <?php $__currentLoopData = $ciudades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ciudad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($ciudad->id); ?>" <?php echo e(old('ciudad_id', $ciudad_id) == $ciudad->id ? 'selected' : ''); ?>><?php echo e(strtoupper($ciudad->titulo)); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Dirección </label>
                                    <input required type="text" class="form-control" name="direccion" id="direccion" value="<?php echo e(old('direccion',$direccion)); ?>">
                                </div>
                            </div>
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Barrio </label>
                                    <input required type="text" class="form-control" name="barrio" id="barrio" value="<?php echo e(old('barrio',$barrio)); ?>">
                                </div>
                            </div>
                            
                        </div>
                        <div class = "row">
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Celular </label>
                                    <input required type="text" class="form-control" name="celular" id="celular" value="<?php echo e(old('celular',$celular)); ?>">
                                </div>
                            </div>
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Correo electrónico </label>
                                    <input required type="text" class="form-control" name="correo" id="correo" value="<?php echo e(old('correo',$correo)); ?>">
                                </div>
                            </div>
                            
                        </div>
                                    
                        <span  style = "display:none;" id = "alert-busqueda">
                                Cargando...
                                <img style="width: 30px;" src="<?php echo e(asset('uploads/cargando.gif')); ?>" />
                            </span>

                            <?php if(session()->has('mensaje')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session()->get('mensaje')); ?>

                                </div>
                            <?php endif; ?>
                            <div class="tile-footer">
                                <button type="submit" id = "btn-enviar" class="btn btn-primary btn-sm"><?php echo e($boton); ?></button>
                                <a href="<?php echo e($regresar); ?>"><button class="btn btn-success btn-sm" type = "button">Regresar</button></a>
                            </div>
                    </form>                



                </div>
            </div>
        </div>
</div>






<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function(){
            $("#alert-busqueda").show();
        })

        $('#btn_imagen').click(function(event){
            event.preventDefault();
            $('#urlimagen').click();
        })

        $('#urlimagen').change(function() {
            if($("#urlimagen").val() != ''){
                var file = $('#urlimagen')[0].files[0].name;
                $("#btn_imagen").html("Seleccionado");
            }else{
                $("#btn_imagen").html("Seleccionar");
            }
        });

        $('#btn_archivo').click(function(event){
            event.preventDefault();
            $('#urlarchivo').click();
        })

        $('#urlarchivo').change(function() {
            if($("#urlarchivo").val() != ''){
                var file = $('#urlarchivo')[0].files[0].name;
                $("#btn_archivo").html("Seleccionado");
            }else{
                $("#btn_archivo").html("Seleccionar");
            }
        });

        $("select[name='departamento_id']").change(function(){
            var departamento = $(this).val();
            $.ajax({
                url: "<?php echo e(route('personas.cargar_ciudades')); ?>",
                method: 'POST',
                data: {id:departamento, "_token": "<?php echo e(csrf_token()); ?>"},
                success: function(data) {
                    $("select[name='ciudad_id'").html('');
                    $("select[name='ciudad_id'").html(data.options);
                }
            });
        });

        $("select[name='pais_id']").change(function(){
            var pais = $(this).val();
            $.ajax({
                url: "<?php echo e(route('personas.cargar_departamentos')); ?>",
                method: 'POST',
                data: {id:pais, "_token": "<?php echo e(csrf_token()); ?>"},
                success: function(data) {
                    $("select[name='departamento_id'").html('');
                    $("select[name='departamento_id'").html(data.options);
                }
            });
        });



    });
    

</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Y:\xamppnuevo\htdocs\colegio\resources\views/personas/create.blade.php ENDPATH**/ ?>