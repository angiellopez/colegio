<?php $__env->startSection('contenido'); ?>



        <form  method="POST" action="<?php echo e(route('login')); ?>">
            <?php echo e(csrf_field()); ?>

            <center><img style = "width:150px!important;" src="<?php echo e(asset('uploads/logo.jpg')); ?>" alt=""><br><br>
                <h2 class = "font-bold" style = "color: #007bff;font-size:20px;">LICEO SANTA TERESITA PASTO</h2>
            </center>
          <!--<h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>INICIAR SESIÓN</h3>-->
         <br><br>
          <div class="form-group <?php echo e($errors->has('user') ? ' has-error' : ''); ?>">
                <label class="control-label">USUARIO</label>
                <input id="user" type="text" class="form-control" name="user" value="<?php echo e(old('user')); ?>" autofocus required placeholder="Usuario">
                <?php if($errors->has('user')): ?>
                    <span class="help-block">
                        <?php echo e($errors->first('user')); ?>

                    </span>
                <?php endif; ?>
            </div>
            <div class="form-group <?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                <label class="control-label">CONTRASEÑA</label>
                <input type="password" class="form-control" id="password" name = "password" value="" required placeholder="Contraseña">
                <?php if($errors->has('password')): ?>
                    <span class="help-block">
                        <?php echo e($errors->first('password')); ?>

                    </span>
                <?php endif; ?>
            </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block">INICIAR SESIÓN</button>
          </div>
        </form>




<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout_login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Y:\xamppnuevo\htdocs\colegio\resources\views/auth/login.blade.php ENDPATH**/ ?>