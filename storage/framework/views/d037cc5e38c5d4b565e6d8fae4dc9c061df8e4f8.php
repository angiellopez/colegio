<?php $__env->startSection('content'); ?>

<?php
        if(isset($empresa)){
            $titulo = $empresa->titulo;
            $subtitulo = $empresa->subtitulo;
            $descripcion = $empresa->descripcion;
            $nomenlace = $empresa->nomenlace;
            $linkenlace = $empresa->linkenlace;
            $nomimagen = $empresa->nomimagen;
            $urlimagen = $empresa->urlimagen;
        }else{
            $titulo = "";
            $subtitulo = "";
            $descripcion = "";
            $nomenlace = "";
            $linkenlace = "";
            $nomimagen = "";
            $urlimagen = "";
        }
    ?>







<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                </div>
                <div class="card-body card-block">

                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <h6>Para continuar debe corregir los siguientes errores:</h6>
                            <br>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endif; ?>

                    <form method="POST" id = "formulario" action="<?php echo e($accion); ?>" files="true" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e($metodo); ?>

                        <div class = "row">
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>NIT <li class = "fa fa-asterisk red icono-asterisk"></li></label>
                                    <input required type="text" class="form-control" name="titulo" id="titulo" value="<?php echo e(old('titulo',$titulo)); ?>">
                                </div>
                            </div>
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Razón social <li class = "fa fa-asterisk red icono-asterisk"></li></label>
                                    <input required type="text" class="form-control" name="subtitulo" id="subtitulo" value="<?php echo e(old('subtitulo',$subtitulo)); ?>">
                                </div>
                            </div>
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Dirección</label>
                                    <input type="text" class="form-control" name="descripcion" id="descripcion" value="<?php echo e(old('descripcion',$descripcion)); ?>">
                                </div>
                            </div> 
                        </div>
                        <div class = "row">
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Teléfono</label>
                                    <input type="text" class="form-control" name="nomenlace" id="nomenlace" value="<?php echo e(old('nomenlace',$nomenlace)); ?>">
                                </div>
                            </div>   
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Whatsapp</label>
                                    <input type="text" class="form-control" name="linkenlace" id="linkenlace" value="<?php echo e(old('linkenlace',$linkenlace)); ?>">
                                </div>       
                            </div> 
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Correo electrónico</label>
                                    <input type="text" class="form-control" name="nomimagen" id="nomimagen" value="<?php echo e(old('nomimagen',$nomimagen)); ?>">
                                </div>       
                            </div> 
                        </div>
                        <div class = "row">
                            <div class = "col-md-4">
                                <div class="upload-btn-wrapper form-group">
                                    <label>Imagen</label><br>
                                    <input type="file" class = "form-control" name="urlimagen" id="urlimagen" />
                                </div>      
                            </div> 
                        </div>
                        <span  style = "display:none;" id = "alert-busqueda">
                            Cargando...
                            <img style="width: 30px;" src="<?php echo e(asset('uploads/cargando.gif')); ?>" />
                        </span>

                        <?php if(session()->has('mensaje')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session()->get('mensaje')); ?>

                            </div>
                        <?php endif; ?>
                        <div class="tile-footer">
                            <button type="submit" id = "btn-enviar" class="btn btn-primary"><?php echo e($boton); ?></button>
                            <a href="<?php echo e(route('configuracion.index')); ?>"><button class="btn btn-success" type = "button">Regresar</button></a>
                        </div>
                    </form>                                                 



                </div>
            </div>
        </div>
</div>







<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Imagen Cargada</strong>
                </div>
                <div class="card-body card-block">


                    <?php if($urlimagen != ""): ?>
                    <div class = "col-md-12">
                            <img style = "width:250px;" src="<?php echo e(asset('uploads/')); ?><?php echo e("/".$urlimagen); ?>" class = "img-responsive">
                    </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
</div>






<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function(){
            $("#alert-busqueda").show();
        })

        $('#btn_imagen').click(function(event){
            event.preventDefault();
            $('#urlimagen').click();
        })

        $('#urlimagen').change(function() {
            if($("#urlimagen").val() != ''){
                var file = $('#urlimagen')[0].files[0].name;
                $("#btn_imagen").html("Seleccionado");
            }else{
                $("#btn_imagen").html("Seleccionar");
            }
        });

        $('#btn_archivo').click(function(event){
            event.preventDefault();
            $('#urlarchivo').click();
        })

        $('#urlarchivo').change(function() {
            if($("#urlarchivo").val() != ''){
                var file = $('#urlarchivo')[0].files[0].name;
                $("#btn_archivo").html("Seleccionado");
            }else{
                $("#btn_archivo").html("Seleccionar");
            }
        });
    });
    

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\colegio\resources\views/configuracion/empresa.blade.php ENDPATH**/ ?>