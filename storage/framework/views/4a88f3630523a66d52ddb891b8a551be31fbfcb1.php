<?php $__env->startSection('content'); ?>

<?php
        if(isset($asignacion)){
            $ciclo_id = $asignacion->ciclo_id;
            $materia_id = $asignacion->materia_id;
            $docente_id = $asignacion->docente_id;

        }else{
            $ciclo_id = "";
            $materia_id = "";
            $docente_id = "";

        }
    ?>






<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                </div>
                <div class="card-body card-block">

                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <h6>Para continuar debe corregir los siguientes errores:</h6>
                            <br>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endif; ?>

                    <form method="POST" id = "formulario" action="<?php echo e($accion); ?>" files="true" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e($metodo); ?>

                        <div class = "row">
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Ciclo</label>
                                    <select class="form-control" name="ciclo_id" id="ciclo_id" >
                                        <option value="" disabled selected>SELECCIONE</option>
                                        <?php $__currentLoopData = $ciclos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ciclo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($ciclo->id); ?>" <?php echo e(old('ciclo_id', $ciclo_id) == $ciclo->id ? 'selected' : ''); ?>><?php echo e($ciclo->nombre); ?> :: <?php echo e($ciclo->anio); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Materia</label>
                                    <select class="form-control" name="materia_id" id="materia_id" >
                                        <option value="" disabled selected>SELECCIONE</option>
                                        <?php $__currentLoopData = $materias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $materia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($materia->id); ?>" <?php echo e(old('materia_id', $materia_id) == $materia->id ? 'selected' : ''); ?>><?php echo e($materia->nombre); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Docente</label>
                                    <select class="form-control" name="docente_id" id="docente_id" >
                                        <option value="" disabled selected>SELECCIONE</option>
                                        <?php $__currentLoopData = $docentes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $docente): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($docente->id); ?>" <?php echo e(old('docente_id', $docente_id) == $docente->id ? 'selected' : ''); ?>><?php echo e($docente->apellidos); ?> <?php echo e($docente->nombres); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            
                        </div>

                        <div class = "row">
                            <div class = "col-md-6">
                                    <button type="button" id = "btn-agregar" class="btn btn-sm btn-warning" onclick = "agregar();">Agregar</button>
                            </div>
                        </div>
                        <br>


                        <div class="row">
                            <div class="col-md-12">
                            <div class="table-responsive">
                                <table id = "tabla_registro" class="table table-show table-list table-striped table-bordered">
                                <thead style = "font-weight:bold!important;">
                                    <tr>
                                        <th>CICLO</th>
                                        <th>MATERIA</th>
                                        <th>DOCENTE</th>
                                        <th>ELIMINAR</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    
                                </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                        <br>
                        
                        <span  style = "display:none;" id = "alert-busqueda">
                                Cargando...
                                <img style="width: 30px;" src="<?php echo e(asset('uploads/cargando.gif')); ?>" />
                            </span>

                            <?php if(session()->has('mensaje')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session()->get('mensaje')); ?>

                                </div>
                            <?php endif; ?>
                            <div class="tile-footer">
                                <button type="submit" id = "btn-enviar" class="btn btn-sm btn-primary"><?php echo e($boton); ?></button>
                                <a href="<?php echo e(route('asignaciones.index')); ?>"><button class="btn btn-sm btn-success" type = "button">Regresar</button></a>
                            </div>
                    </form>                                 



                </div>
            </div>
        </div>
</div>








<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function(){
            $("#alert-busqueda").show();
        })

        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();
            $(this).closest('tr').remove();
        });
    });

    function agregar(){
      ciclo = $("#ciclo_id").val();
      ciclo_texto = $("#ciclo_id option:selected").text();
      materia = $("#materia_id").val();
      materia_texto = $("#materia_id option:selected").text();
      docente = $("#docente_id").val();
      docente_texto = $("#docente_id option:selected").text();

      if(ciclo == null || materia == null || docente == null){
        alert("¡Todos los campos son obligatorios!");
      }else{
        cadena = "<tr>";
        cadena += "<td><input type='hidden' name='ciclo[]' value='"+ciclo+"' /><span>"+ciclo_texto+"</span></td>";
        cadena += "<td><input type='hidden' name='materia[]' value='"+materia+"' /><span>"+materia_texto+"</span></td>";
        cadena += "<td><input type='hidden' name='docente[]' value='"+docente+"' /><span>"+docente_texto+"</span></td>";
        cadena +=  "<td><button type='button' class = 'btn-delete' ><i class='fa fa-times-circle red bigger-24' aria-hidden='true'></i></button></td>";
        cadena += "</tr>";

        $("#tabla_registro tbody").append(cadena);

        $("#materia_id").val("");
        $("#docente_id").val("");
      }
    }

</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Y:\xamppnuevo\htdocs\colegio\resources\views/asignaciones/create.blade.php ENDPATH**/ ?>