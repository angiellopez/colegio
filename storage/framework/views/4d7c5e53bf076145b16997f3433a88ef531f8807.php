<?php $__env->startSection('content'); ?>




<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                </div>
                <div class="card-body card-block">


                        <div class = "table-responsive">
                            <table class="table table-show table-sinbordes table-list table-striped table-bordered">
                                    <tr>
                                            <td class = "font-bold">IDENTIFICACIÓN</td>
                                            <td><?php echo e($persona->identificacion); ?></td>
                                    </tr>
                                    <tr>
                                            <td class = "font-bold">NOMBRES</td>
                                            <td><?php echo e($persona->nombres); ?></td>
                                    </tr>
                                    <tr>
                                            <td class = "font-bold">APELLIDOS</td>
                                            <td><?php echo e($persona->apellidos); ?></td>
                                    </tr>
                                    <tr>
                                        <td class = "font-bold">DIRECCION</td>
                                        <td><?php echo e($persona->direccion); ?></td>
                                    </tr>
                                    <tr>
                                        <td class = "font-bold">BARRIO</td>
                                        <td><?php echo e($persona->barrio); ?></td>
                                    </tr>
                                    <tr>
                                        <td class = "font-bold">CELULAR</td>
                                        <td><?php echo e($persona->celular); ?></td>
                                    </tr>
                                    <tr>
                                        <td class = "font-bold">CORREO ELECTRÓNICO</td>
                                        <td><?php echo e($persona->correo); ?></td>
                                    </tr>
                                    <tr>
                                        <td class = "font-bold">FECHA REGISTRO</td>
                                        <td><?php echo e($persona->created_at); ?></td>
                                    </tr>
                            </table>
                        </div>
                        <br>
                        <a href="<?php echo e($regresar); ?>"><button class="btn btn-success btn-sm" type = "button">Regresar</button></a>
                
                
                </div>
            </div>
        </div>
</div>    




<script type="text/javascript">
    jQuery(function($) {
    });
    

</script>

<?php $__env->stopSection(); ?>




<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\colegio\resources\views/personas/show.blade.php ENDPATH**/ ?>