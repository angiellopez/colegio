<?php $__env->startSection('content'); ?>





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                </div>
                <div class="card-body card-block">

                    <div class = "table-responsive">
                        <table class="table table-show table-bordered">
                                <tr>
                                        <td class = "font-bold">CARGO</td>
                                        <td><?php echo e($cargo->nombre); ?></td>
                                </tr>
                        </table>
                    </div>
                    <br>
                    <a href="<?php echo e(route('cargos.index')); ?>"><button class="btn btn-sm btn-success" type = "button">Regresar</button></a>
                    
                
                </div>
            </div>
        </div>
</div>    



<?php $__env->stopSection(); ?>




<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Y:\xamppnuevo\htdocs\colegio\resources\views/cargos/show.blade.php ENDPATH**/ ?>