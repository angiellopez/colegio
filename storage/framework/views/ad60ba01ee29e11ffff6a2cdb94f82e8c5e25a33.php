<?php $__env->startSection('content'); ?>



<div class = "row">
        <div class="col-lg-12">
                

                    <div class = "row">
                        <div class="col-md-3">
                                <div class="statistic__item">
                                    <a href="<?php echo e(route('personas.listar',['tipo_persona'=>'1','nom_persona'=>'docentes'])); ?>" >
                                        <span class="desc configuracion2">Docentes</span>
                                        <div class="icon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-3">
                                <div class="statistic__item ">
                                    <a href="<?php echo e(route('personas.listar',['tipo_persona'=>'2','nom_persona'=>'estudiantes'])); ?>" >
                                        <span class="desc configuracion2">Estudiantes</span>
                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-3">
                                <div class="statistic__item ">
                                    <a href="<?php echo e(route('personas.listar',['tipo_persona'=>'3','nom_persona'=>'funcionarios'])); ?>" >
                                        <span class="desc configuracion2">Funcionarios</span>
                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-3">
                                <div class="statistic__item ">
                                    <a href="<?php echo e(route('matriculas.index')); ?>" >
                                        <span class="desc configuracion2">Matriculas</span>
                                        <div class="icon">
                                            <i class="fa fa-book"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                    </div>  

                    <div class = "row">
                        <div class="col-md-3">
                                <div class="statistic__item ">
                                    <a href="<?php echo e(route('configuracion.index')); ?>" >
                                        <span class="desc configuracion2">Configuración</span>
                                        <div class="icon">
                                            <i class="fa fa-cog"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                    </div>  
                    
        </div>
</div>    








<?php $__env->stopSection(); ?>



<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\colegio\resources\views/principal/index.blade.php ENDPATH**/ ?>