<?php $__env->startSection('content'); ?>

<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>ACCESO DENEGADO</strong>
                </div>
                <div class="card-body card-block">
                <div class = "alert alert-danger"><i class = "fa fa-times-circle"></i> ¡Usted no tiene permisos para acceder a este lugar!</div>


                </div>
            </div>
        </div>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Y:\xamppnuevo\htdocs\colegio\resources\views/errors/access_denied_admin.blade.php ENDPATH**/ ?>