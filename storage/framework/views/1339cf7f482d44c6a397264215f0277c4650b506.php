<?php $__env->startSection('content'); ?>





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong><?php echo e($title); ?></strong>
                    <a href="<?php echo e(route('ciclos.create')); ?>" class="btn btn-primary btn-sm  "><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo</a></header>

                </div>
                <div class="card-body card-block">
                    <?php if($ciclos->isNotEmpty()): ?>
                    <div class = "table-responsive">
                        <table class="table table-hover table-bordered" id="sampleTable">
                            <thead>
                                <tr>
                                        <th>Ciclo</th>
                                        <th>Año</th>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Fin</th>

                                        <th style = "width:120px!important;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $ciclos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ciclo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($ciclo->nombre); ?></td>
                                <td><?php echo e($ciclo->anio); ?></td>
                                <td><?php echo e($ciclo->fecha_inicio); ?></td>
                                <td><?php echo e($ciclo->fecha_fin); ?></td>

                                <td><center>
                                    <form id = "form<?php echo e($ciclo->id); ?>" class = "form-table" action="<?php echo e(route('ciclos.destroy', $ciclo->id)); ?>" method="POST">
                                        <?php echo e(csrf_field()); ?>

                                        <?php echo e(method_field('DELETE')); ?>

                                        <a class="btn btn-sm btn-primary" title = "Ver" href="<?php echo e(route('ciclos.show',['id'=>$ciclo->id])); ?>"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                        <a class="btn btn-sm btn-success"  title = "Modificar" href="<?php echo e(route('ciclos.edit',['id'=>$ciclo->id])); ?>"><i class="fa green fa-pencil-square" aria-hidden="true"></i></a>
                                        <a class="btn btn-sm btn-danger"  title = "Eliminar" href="" onclick="eliminar(<?php echo e($ciclo->id); ?>,event)"><i class="fa green fa-times-circle" aria-hidden="true"></i></a>
                                    </form></center>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                        <br>
                        <?php if(session()->has('mensaje')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session()->get('mensaje')); ?>

                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if(session()->has('alerta')): ?>
                        <div class="alert alert-danger">
                            <?php echo e(session()->get('alerta')); ?>

                        </div>
                    <?php endif; ?>
                    <?php else: ?>
                        <p>No hay ciclos registrados.</p>
                    <?php endif; ?>
                    <span  style = "display:none;" id = "alert-busqueda">
                        Cargando...
                        <img style="width: 30px;" src="<?php echo e(asset('uploads/cargando.gif')); ?>" />
                    </span>
                    <br>
                    <div class="tile-footer">
                        <a href="<?php echo e(route('configuracion.index')); ?>"><button class="btn btn-success" type = "button">Regresar</button></a>
                    </div>                   


                </div>
            </div>
        </div>
</div>




		<script type="text/javascript">
			jQuery(function($) {
			});
            function eliminar(id,event){
                event.preventDefault();
                bootbox.confirm({
                    message: "Está seguro que desea eliminar el registro?",
                    buttons: {
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirmar',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if(result == true){
                            $("#alert-busqueda").show();
                            $("#form"+id).submit();
                        }
                    }
                });
            }
		</script>
<?php $__env->stopSection(); ?>










<?php echo $__env->make('layout_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH Y:\xamppnuevo\htdocs\colegio\resources\views/ciclos/index.blade.php ENDPATH**/ ?>