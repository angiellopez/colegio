<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

//*****************************RUTAS PARA MODULO ADMINISTRATIVO *********************************/

Route::get('/principal','PrincipalController@index')->name('principal.index');


Route::get('/configuracion','ConfiguracionController@index')->name('configuracion.index');
Route::get('/configuracion/empresa','ConfiguracionController@empresa')->name('configuracion.empresa');
Route::post('/configuracion/guardar_empresa','ConfiguracionController@guardar_empresa')->name('configuracion.guardar_empresa');

Route::get('/users','UserController@index')->name('users.index');
Route::get('/users/show/{id}','UserController@show')->where('id','[0-9]+')->name('users.show');
Route::get('/users/nuevo','UserController@create')->name('users.create');
Route::post('/users/guardar','UserController@store')->name('users.store');
Route::get('/users/editar/{id}','UserController@edit')->name('users.edit');
Route::put('/users/actualizar/{id}','UserController@update')->name('users.update');
Route::delete('/users/eliminar/{id}','UserController@destroy')->name('users.destroy');
Route::get('/users/perfil','UserController@perfil')->name('users.perfil');

Route::get('/cargos','CargoController@index')->name('cargos.index');
Route::get('/cargos/show/{id}','CargoController@show')->where('id','[0-9]+')->name('cargos.show');
Route::get('/cargos/nuevo','CargoController@create')->name('cargos.create');
Route::post('/cargos/guardar','CargoController@store')->name('cargos.store');
Route::get('/cargos/editar/{id}','CargoController@edit')->name('cargos.edit');
Route::put('/cargos/actualizar/{id}','CargoController@update')->name('cargos.update');
Route::delete('/cargos/eliminar/{id}','CargoController@destroy')->name('cargos.destroy');
Route::get('/cargos/perfil','CargoController@perfil')->name('cargos.perfil');

Route::get('/asignaciones','AsignacionController@index')->name('asignaciones.index');
Route::get('/asignaciones/show/{id}','AsignacionController@show')->where('id','[0-9]+')->name('asignaciones.show');
Route::get('/asignaciones/nuevo','AsignacionController@create')->name('asignaciones.create');
Route::post('/asignaciones/guardar','AsignacionController@store')->name('asignaciones.store');
Route::get('/asignaciones/editar/{id}','AsignacionController@edit')->name('asignaciones.edit');
Route::put('/asignaciones/actualizar/{id}','AsignacionController@update')->name('asignaciones.update');
Route::delete('/asignaciones/eliminar/{id}','AsignacionController@destroy')->name('asignaciones.destroy');
Route::get('/asignaciones/perfil','AsignacionController@perfil')->name('asignaciones.perfil');

Route::get('/ciclos','CicloController@index')->name('ciclos.index');
Route::get('/ciclos/show/{id}','CicloController@show')->where('id','[0-9]+')->name('ciclos.show');
Route::get('/ciclos/nuevo','CicloController@create')->name('ciclos.create');
Route::post('/ciclos/guardar','CicloController@store')->name('ciclos.store');
Route::get('/ciclos/editar/{id}','CicloController@edit')->name('ciclos.edit');
Route::put('/ciclos/actualizar/{id}','CicloController@update')->name('ciclos.update');
Route::delete('/ciclos/eliminar/{id}','CicloController@destroy')->name('ciclos.destroy');
Route::get('/ciclos/perfil','CicloController@perfil')->name('ciclos.perfil');

Route::get('/materias','MateriaController@index')->name('materias.index');
Route::get('/materias/show/{id}','MateriaController@show')->where('id','[0-9]+')->name('materias.show');
Route::get('/materias/nuevo','MateriaController@create')->name('materias.create');
Route::post('/materias/guardar','MateriaController@store')->name('materias.store');
Route::get('/materias/editar/{id}','MateriaController@edit')->name('materias.edit');
Route::put('/materias/actualizar/{id}','MateriaController@update')->name('materias.update');
Route::delete('/materias/eliminar/{id}','MateriaController@destroy')->name('materias.destroy');
Route::get('/materias/perfil','MateriaController@perfil')->name('materias.perfil');

Route::get('/matriculas','MatriculaController@index')->name('matriculas.index');
Route::get('/matriculas/show/{id}','MatriculaController@show')->where('id','[0-9]+')->name('matriculas.show');
Route::get('/matriculas/nuevo','MatriculaController@create')->name('matriculas.create');
Route::post('/matriculas/guardar','MatriculaController@store')->name('matriculas.store');
Route::get('/matriculas/editar/{id}','MatriculaController@edit')->name('matriculas.edit');
Route::put('/matriculas/actualizar/{id}','MatriculaController@update')->name('matriculas.update');
Route::delete('/matriculas/eliminar/{id}','MatriculaController@destroy')->name('matriculas.destroy');
Route::get('/matriculas/perfil','MatriculaController@perfil')->name('matriculas.perfil');

Route::get('/personas/listar/{tipo_persona}/{nom_persona}','PersonaController@listar')->name('personas.listar');
Route::get('/personas/show/{id}','PersonaController@show')->where('id','[0-9]+')->name('personas.show');
Route::get('/personas/nuevo/{tipo_persona}/{nom_persona}','PersonaController@create')->name('personas.create');
Route::post('/personas/guardar/{tipo_persona}/{nom_persona}','PersonaController@store')->name('personas.store');
Route::get('/personas/editar/{id}','PersonaController@edit')->name('personas.edit');
Route::put('/personas/actualizar/{id}','PersonaController@update')->name('personas.update');
Route::delete('/personas/eliminar/{id}','PersonaController@destroy')->name('personas.destroy');
Route::get('/personas/vehiculo/{id}','PersonaController@vehiculo')->name('personas.vehiculo');
Route::post('/personas/cargar_ciudades','PersonaController@cargar_ciudades')->name('personas.cargar_ciudades');
Route::post('/personas/cargar_departamentos','PersonaController@cargar_departamentos')->name('personas.cargar_departamentos');

Route::post('/personas/filtrar_tabla','PersonaController@filtrar_tabla')->name('personas.filtrar_tabla');
Route::post('/personas/cargar_personas','PersonaController@cargar_personas')->name('personas.cargar_personas');
Route::post('/personas/buscar','PersonaController@buscar')->name('personas.buscar');

Route::get('/home', 'HomeController@index')->name('home');

