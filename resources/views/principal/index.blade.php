@extends('layout_admin')

@section('content')



<div class = "row">
        <div class="col-lg-12">
                

                    <div class = "row">
                        <div class="col-md-3">
                                <div class="statistic__item">
                                    <a href="{{ route('personas.listar',['tipo_persona'=>'1','nom_persona'=>'docentes']) }}" >
                                        <span class="desc configuracion2">Docentes</span>
                                        <div class="icon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-3">
                                <div class="statistic__item ">
                                    <a href="{{ route('personas.listar',['tipo_persona'=>'2','nom_persona'=>'estudiantes']) }}" >
                                        <span class="desc configuracion2">Estudiantes</span>
                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-3">
                                <div class="statistic__item ">
                                    <a href="{{ route('personas.listar',['tipo_persona'=>'3','nom_persona'=>'funcionarios']) }}" >
                                        <span class="desc configuracion2">Funcionarios</span>
                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-3">
                                <div class="statistic__item ">
                                    <a href="{{ route('matriculas.index') }}" >
                                        <span class="desc configuracion2">Matriculas</span>
                                        <div class="icon">
                                            <i class="fa fa-book"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                    </div>  

                    <div class = "row">
                        <div class="col-md-3">
                                <div class="statistic__item ">
                                    <a href="{{ route('configuracion.index') }}" >
                                        <span class="desc configuracion2">Configuración</span>
                                        <div class="icon">
                                            <i class="fa fa-cog"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                    </div>  
                    
        </div>
</div>    








@endsection


