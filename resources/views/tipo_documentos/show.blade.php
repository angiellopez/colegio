@extends('layout_admin')

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> {{ $title }}
            <a href="{{ route('admin_documentos.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>Nuevo</a>
        </h1>
    </div>
</div>

<div class="col-md-9">
          <div class="tile">
              <div class="row">
                <div class="col-12">
                  <h2 class="page-header"><i class="fa fa-list"></i> {{ $title }}</h2>
                </div>
              </div>
              <br>
              <div class = "row">
                  <div class = "col-md-12">
                        <div class = "table-responsive">
                            <table class="table table-bordered">
                                    <tr>
                                            <td class = "bold">DOCUMENTO</td>
                                            <td>{{$admin_documento->titulo}}</td>
                                    </tr>
                                    <tr>
                                            <td class = "bold">ABREVIATURA</td>
                                            <td>{{$admin_documento->abreviatura}}</td>
                                    </tr>
                            </table>
                        </div>
                        <a href="{{ route('admin_documentos.index') }}"><button class="btn btn-success" type = "button">Regresar</button></a>
                   </div> 
                  <div class = "col-md-3">
                  </div>
              </div>
                
              
          </div>
</div>

@endsection



