@extends('layout_admin')

@section('content')
<div class="app-title">
    <div>
        <h1>
            <i class="fa fa-dashboard"></i> {{ $title }}
            <a href="{{ route('admin_documentos.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>Nuevo</a>
        </h1>
    </div>
</div>

<div class = "row">




<div class="col-md-9">
    <div class="tile">
    <div class="tile-body">
    @if ($admin_documentos->isNotEmpty())
    <div class = "table-responsive">
        <table class="table table-hover table-bordered" id="sampleTable">
            <thead>
                <tr>
                        <th>Tipo Documento</th>
                    <th style = "width:120px!important;">Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($admin_documentos as $admin_documento)
            <tr>
                <td>{{ $admin_documento->titulo }}</td>
                <td><center>
                    <form id = "form{{$admin_documento->id}}" class = "form-table" action="{{ route('admin_documentos.destroy', $admin_documento->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <a class="btn2 btn-primary" title = "Ver" href="{{ route('admin_documentos.show',['id'=>$admin_documento->id]) }}"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                        <a class="btn2 btn-success"  title = "Modificar" href="{{ route('admin_documentos.edit',['id'=>$admin_documento->id]) }}"><i class="fa green fa-pencil-square" aria-hidden="true"></i></a>
                        <a class="btn2 btn-danger"  title = "Eliminar" href="" onclick="eliminar({{$admin_documento->id}},event)"><i class="fa green fa-times-circle" aria-hidden="true"></i></a>
                    </form></center>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        @if(session()->has('mensaje'))
            <div class="alert alert-success">
                {{ session()->get('mensaje') }}
            </div>
        @endif
    </div>
    @if(session()->has('alerta'))
        <div class="alert alert-danger">
            {{ session()->get('alerta') }}
        </div>
    @endif
    @else
        <p>No hay admin_documentos registrados.</p>
    @endif
    <span  style = "display:none;" id = "alert-busqueda">
        Cargando...
        <img style="width: 30px;" src="{{ asset('uploads/cargando.gif') }}" />
    </span>
    <div class="tile-footer">
        <a href="{{ route('admin_configuracion.index') }}"><button class="btn btn-success" type = "button">Regresar</button></a>
    </div>
    
    </div>
    </div>
</div>

</div>


		<script type="text/javascript">
			jQuery(function($) {
                $('#sampleTable').dataTable( {
                    "order": [],
                    "iDisplayLength": 25,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "_START_ al _END_ de  _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   " - filtro de _MAX_ registros",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                } );
			});
            function eliminar(id,event){
                event.preventDefault();
                bootbox.confirm({
                    message: "Está seguro que desea eliminar el registro?",
                    buttons: {
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirmar',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if(result == true){
                            $("#alert-busqueda").show();
                            $("#form"+id).submit();
                        }
                    }
                });
            }
		</script>
@endsection









