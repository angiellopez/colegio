@extends('layout_admin')

@section('content')





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                    <a href="{{ route('cargos.create') }}" class="btn btn-primary btn-sm  "><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo</a></header>

                </div>
                <div class="card-body card-block">
                    @if ($cargos->isNotEmpty())
                    <div class = "table-responsive">
                        <table class="table table-hover table-bordered" id="sampleTable">
                            <thead>
                                <tr>
                                        <th>Cargo</th>
                                    <th style = "width:120px!important;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($cargos as $cargo)
                            <tr>
                                <td>{{ $cargo->nombre }}</td>
                                <td><center>
                                    <form id = "form{{$cargo->id}}" class = "form-table" action="{{ route('cargos.destroy', $cargo->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a class="btn btn-sm btn-primary" title = "Ver" href="{{ route('cargos.show',['id'=>$cargo->id]) }}"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                        <a class="btn btn-sm btn-success"  title = "Modificar" href="{{ route('cargos.edit',['id'=>$cargo->id]) }}"><i class="fa green fa-pencil-square" aria-hidden="true"></i></a>
                                        <a class="btn btn-sm btn-danger"  title = "Eliminar" href="" onclick="eliminar({{$cargo->id}},event)"><i class="fa green fa-times-circle" aria-hidden="true"></i></a>
                                    </form></center>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        @if(session()->has('mensaje'))
                            <div class="alert alert-success">
                                {{ session()->get('mensaje') }}
                            </div>
                        @endif
                    </div>
                    @if(session()->has('alerta'))
                        <div class="alert alert-danger">
                            {{ session()->get('alerta') }}
                        </div>
                    @endif
                    @else
                        <p>No hay cargos registrados.</p>
                    @endif
                    <span  style = "display:none;" id = "alert-busqueda">
                        Cargando...
                        <img style="width: 30px;" src="{{ asset('uploads/cargando.gif') }}" />
                    </span>
                    <br>
                    <div class="tile-footer">
                        <a href="{{ route('configuracion.index') }}"><button class="btn btn-success" type = "button">Regresar</button></a>
                    </div>                   


                </div>
            </div>
        </div>
</div>




		<script type="text/javascript">
			jQuery(function($) {
			});
            function eliminar(id,event){
                event.preventDefault();
                bootbox.confirm({
                    message: "Está seguro que desea eliminar el registro?",
                    buttons: {
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirmar',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if(result == true){
                            $("#alert-busqueda").show();
                            $("#form"+id).submit();
                        }
                    }
                });
            }
		</script>
@endsection









