@extends('layout_login')

@section('contenido')



        <form  method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <center><img style = "width:150px!important;" src="{{ asset('uploads/logo.jpg') }}" alt=""><br><br>
                <h2 class = "font-bold" style = "color: #007bff;font-size:20px;">LICEO SANTA TERESITA PASTO</h2>
            </center>
          <!--<h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>INICIAR SESIÓN</h3>-->
         <br><br>
          <div class="form-group {{ $errors->has('user') ? ' has-error' : '' }}">
                <label class="control-label">USUARIO</label>
                <input id="user" type="text" class="form-control" name="user" value="{{ old('user') }}" autofocus required placeholder="Usuario">
                @if ($errors->has('user'))
                    <span class="help-block">
                        {{ $errors->first('user') }}
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="control-label">CONTRASEÑA</label>
                <input type="password" class="form-control" id="password" name = "password" value="" required placeholder="Contraseña">
                @if ($errors->has('password'))
                    <span class="help-block">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block">INICIAR SESIÓN</button>
          </div>
        </form>




@endsection
