@extends('layout_admin')

@section('content')
<div class="app-title">
    <div>
        <h1>
            <i class="fa fa-dashboard"></i> {{ $titulo }}
            <a href="{{ route('admin_empresas.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>Nuevo</a>
        </h1>
    </div>
</div>

<div class = "row">




<div class="col-md-12">
    <div class="tile">
    <div class="tile-body">
    @if ($admin_empresas->isNotEmpty())
    <div class = "table-responsive">
        <table class="table table-hover table-bordered" id="sampleTable" style = "min-width:800px!important;">
            <thead>
                <tr>
                        <th>ID</th>
                        <th>NIT</th>
                        <th>Razon Social</th>
                        <th>Regimen</th>
                        <th>Tipo_Doc.</th>
                        <th>Num_Doc.</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Dirección</th>
                        <th>Celular</th>
                        <th>Fecha Registro</th>
                    <th style = "min-width:120px!important;">Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($admin_empresas as $admin_empresa)
            <tr>
                <td>{{ $admin_empresa->id }}</td>
                <td>{{ $admin_empresa->nit }}</td>
                <td>{{ $admin_empresa->razon }}</td>
                <td>{{ $admin_empresa->regimen($admin_empresa->regimen_id) }}</td>
                <td>{{ $admin_empresa->documento($admin_empresa->documento_id) }}</td>
                <td>{{ $admin_empresa->identificacion }}</td>
                <td>{{ $admin_empresa->nombres }}</td>
                <td>{{ $admin_empresa->apellidos }}</td>
                <td>{{ $admin_empresa->direccion }}</td>
                <td>{{ $admin_empresa->celular }}</td>
                <td>{{ $admin_empresa->created_at }}</td>
                <td><center>
                    <form id = "form{{$admin_empresa->id}}" class = "form-table" action="{{ route('admin_empresas.destroy', $admin_empresa->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <a class="btn2 btn-primary" title = "Ver" href="{{ route('admin_empresas.show',['id'=>$admin_empresa->id]) }}"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                        <a class="btn2 btn-success"  title = "Modificar" href="{{ route('admin_empresas.edit',['id'=>$admin_empresa->id]) }}"><i class="fa green fa-pencil-square" aria-hidden="true"></i></a>
                        <a class="btn2 btn-danger"  title = "Eliminar" href="" onclick="eliminar({{$admin_empresa->id}},event)"><i class="fa green fa-times-circle" aria-hidden="true"></i></a>
                    </form></center>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        @if(session()->has('mensaje'))
            <div class="alert alert-success">
                {{ session()->get('mensaje') }}
            </div>
        @endif
    </div>
    @if(session()->has('alerta'))
        <div class="alert alert-danger">
            {{ session()->get('alerta') }}
        </div>
    @endif
    @else
        <p>No hay admin_empresas registrados.</p>
    @endif
    <span  style = "display:none;" id = "alert-busqueda">
        Cargando...
        <img style="width: 30px;" src="{{ asset('dashboard/img/cargando.gif') }}" />
    </span>
    
    </div>
    </div>
</div>

</div>


		<script type="text/javascript">
			jQuery(function($) {
                $('#sampleTable').dataTable( {
                    "order": [],
                    "iDisplayLength": 25,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "_START_ al _END_ de  _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   " - filtro de _MAX_ registros",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                } );
			});
            function eliminar(id,event){
                event.preventDefault();
                bootbox.confirm({
                    message: "Está seguro que desea eliminar el registro?",
                    buttons: {
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirmar',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if(result == true){
                            $("#alert-busqueda").show();
                            $("#form"+id).submit();
                        }
                    }
                });
            }
		</script>
@endsection









