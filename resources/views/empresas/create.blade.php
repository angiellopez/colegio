@extends('layout_admin')

@section('content')

<?php
        if(isset($admin_empresa)){
            $nit = $admin_empresa->nit;
            $razon = $admin_empresa->razon;
            $regimen_id = $admin_empresa->regimen_id;
            $documento_id = $admin_empresa->documento_id;
            $identificacion = $admin_empresa->identificacion;
            $nombres = $admin_empresa->nombres;
            $apellidos = $admin_empresa->apellidos;
            $pais_id = $admin_empresa->pais_id;
            $departamento_id = $admin_empresa->departamento_id;
            $ciudad_id = $admin_empresa->ciudad_id;
            $direccion = $admin_empresa->direccion;
            $barrio = $admin_empresa->barrio;
            $celular = $admin_empresa->celular;
            $correo = $admin_empresa->correo;
        }else{
            $nit = "";
            $razon = "";
            $regimen_id = "";
            $documento_id = "";
            $identificacion = "";
            $nombres = "";
            $apellidos = "";
            $pais_id = "";
            $departamento_id = "";
            $ciudad_id = "";
            $direccion = "";
            $barrio = "";
            $celular = "";
            $correo = "";
        }
    ?>




<div class="app-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> {{ $titulo }}</h1>
    </div>
</div>

<div class = "row">
    <div class="col-md-12">
          <div class="tile">
            <h3 class="tile-title">{{ $titulo }}</h3>
            <div class="tile-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <h6>Para continuar debe corregir los siguientes errores:</h6>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" id = "formulario" action="{{ $accion }}" files="true" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ $metodo }}
                <div class= "row">
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>NIT </label>
                            <input required type="text" class="form-control" name="nit" id="nit" value="{{ old('nit',$nit) }}">
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Razón Social </label>
                            <input required type="text" class="form-control" name="razon" id="razon" value="{{ old('razon',$razon) }}">
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Regimen</label>
                            <select class="form-control" name="regimen_id" id="regimen_id" required aria-required="true">
                                <option value="" disabled selected>SELECCIONE</option>
                                @foreach($regimenes as $regimen)
                                <option value="{{$regimen->id}}" {{ old('regimen_id', $regimen_id) == $regimen->id ? 'selected' : ''}}>{{$regimen->titulo}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Tipo Doc. Rep Legal</label>
                            <select class="form-control" name="documento_id" id="documento_id" required aria-required="true">
                                <option value="" disabled selected>SELECCIONE</option>
                                @foreach($documentos as $documento)
                                <option value="{{$documento->id}}" {{ old('documento_id', $documento_id) == $documento->id ? 'selected' : ''}}>{{$documento->titulo}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class = "row">
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Identificación </label>
                            <input required type="text" class="form-control" name="identificacion" id="identificacion" value="{{ old('identificacion',$identificacion) }}">
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Nombres </label>
                            <input required type="text" class="form-control" name="nombres" id="nombres" value="{{ old('nombres',$nombres) }}">
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Apellidos </label>
                            <input required type="text" class="form-control" name="apellidos" id="apellidos" value="{{ old('apellidos',$apellidos) }}">
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Pais</label>
                            <select class="form-control" name="pais_id" id="pais_id" required aria-required="true">
                                <option value="" disabled selected>SELECCIONE</option>
                                @foreach($paises as $pais)
                                <option value="{{$pais->id}}" {{ old('pais_id', $pais_id) == $pais->id ? 'selected' : ''}}>{{strtoupper($pais->titulo)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class = "row">
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Departamento</label>
                            <select class="form-control" name="departamento_id" id="departamento_id" required aria-required="true">
                                <option value="" disabled selected>SELECCIONE</option>
                                @foreach($departamentos as $departamento)
                                <option value="{{$departamento->id}}" {{ old('departamento_id', $departamento_id) == $departamento->id ? 'selected' : ''}}>{{strtoupper($departamento->titulo)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Ciudad</label>
                            <select class="form-control" name="ciudad_id" id="ciudad_id" required aria-required="true">
                                <option value="" disabled selected>SELECCIONE</option>
                                @foreach($ciudades as $ciudad)
                                <option value="{{$ciudad->id}}" {{ old('ciudad_id', $ciudad_id) == $ciudad->id ? 'selected' : ''}}>{{strtoupper($ciudad->titulo)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Dirección </label>
                            <input required type="text" class="form-control" name="direccion" id="direccion" value="{{ old('direccion',$direccion) }}">
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Barrio </label>
                            <input required type="text" class="form-control" name="barrio" id="barrio" value="{{ old('barrio',$barrio) }}">
                        </div>
                    </div>
                    
                </div>
                <div class = "row">
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Celular </label>
                            <input required type="text" class="form-control" name="celular" id="celular" value="{{ old('celular',$celular) }}">
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <div class="form-group">
                            <label>Correo electrónico </label>
                            <input required type="text" class="form-control" name="correo" id="correo" value="{{ old('correo',$correo) }}">
                        </div>
                    </div>
                </div>
                <span  style = "display:none;" id = "alert-busqueda">
                        Cargando...
                        <img style="width: 30px;" src="{{ asset('uploads/cargando.gif') }}" />
                    </span>

                    @if(session()->has('mensaje'))
                        <div class="alert alert-success">
                            {{ session()->get('mensaje') }}
                        </div>
                    @endif
                    <div class="tile-footer">
                        <button type="submit" id = "btn-enviar" class="btn btn-primary">{{$boton}}</button>
                        <a href="{{ route('admin_empresas.index') }}"><button class="btn btn-success" type = "button">Regresar</button></a>
                    </div>
            </form>
            </div>
          </div>
    </div>
</div>



<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function(){
            $("#alert-busqueda").show();
        })

        $('#btn_imagen').click(function(event){
            event.preventDefault();
            $('#urlimagen').click();
        })

        $('#urlimagen').change(function() {
            if($("#urlimagen").val() != ''){
                var file = $('#urlimagen')[0].files[0].name;
                $("#btn_imagen").html("Seleccionado");
            }else{
                $("#btn_imagen").html("Seleccionar");
            }
        });

        $('#btn_archivo').click(function(event){
            event.preventDefault();
            $('#urlarchivo').click();
        })

        $('#urlarchivo').change(function() {
            if($("#urlarchivo").val() != ''){
                var file = $('#urlarchivo')[0].files[0].name;
                $("#btn_archivo").html("Seleccionado");
            }else{
                $("#btn_archivo").html("Seleccionar");
            }
        });

        $("select[name='departamento_id']").change(function(){
            var municipio = $(this).val();
            $.ajax({
                url: "{{ route('empleados.cargar_ciudades') }}",
                method: 'POST',
                data: {id:municipio, "_token": "{{ csrf_token() }}"},
                success: function(data) {
                    $("select[name='ciudad_id'").html('');
                    $("select[name='ciudad_id'").html(data.options);
                }
            });
        });
        $("select[name='pais_id']").change(function(){
            var pais = $(this).val();
            $.ajax({
                url: "{{ route('empleados.cargar_departamentos') }}",
                method: 'POST',
                data: {id:pais, "_token": "{{ csrf_token() }}"},
                success: function(data) {
                    $("select[name='departamento_id'").html('');
                    $("select[name='departamento_id'").html(data.options);
                }
            });
        });



    });
    

</script>
@endsection

