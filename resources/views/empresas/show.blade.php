@extends('layout_admin')

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> {{ $titulo }}
            <a href="{{ route('admin_empresas.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>Nuevo</a>
        </h1>
    </div>
</div>

<div class="col-md-9">
          <div class="tile">
              <div class="row">
                <div class="col-12">
                  <h2 class="page-header"><i class="fa fa-list"></i> {{ $titulo }}</h2>
                </div>
              </div>
              <br>
              <div class = "row">
                  <div class = "col-md-12">
                        <div class = "table-responsive">
                            <table class="table table-sinbordes table-list table-striped table-bordered">
                                    <tr>
                                            <td class = "bold">NIT</td>
                                            <td>{{$admin_empresa->nit}}</td>
                                    </tr>
                                    <tr>
                                            <td class = "bold">RAZÓN SOCIAL</td>
                                            <td>{{$admin_empresa->razon}}</td>
                                    </tr>
                                    <tr>
                                            <td class = "bold">RÉGIMEN</td>
                                            <td>{{$admin_empresa->regimen($admin_empresa->regimen_id)}}</td>
                                    </tr>
                                    <tr>
                                            <td class = "bold">IDENTIFICACIÓN</td>
                                            <td>{{$admin_empresa->identificacion}}</td>
                                    </tr>
                                    <tr>
                                            <td class = "bold">NOMBRES</td>
                                            <td>{{$admin_empresa->nombres}}</td>
                                    </tr>
                                    <tr>
                                            <td class = "bold">APELLIDOS</td>
                                            <td>{{$admin_empresa->apellidos}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "bold">PAIS</td>
                                        <td>{{strtoupper($admin_empresa->pais($admin_empresa->pais_id))}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "bold">DEPARTAMENTO</td>
                                        <td>{{strtoupper($admin_empresa->departamento($admin_empresa->departamento_id))}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "bold">CIUDAD</td>
                                        <td>{{strtoupper($admin_empresa->ciudad($admin_empresa->ciudad_id))}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "bold">DIRECCION</td>
                                        <td>{{$admin_empresa->direccion}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "bold">BARRIO</td>
                                        <td>{{$admin_empresa->barrio}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "bold">CELULAR</td>
                                        <td>{{$admin_empresa->celular}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "bold">CORREO ELECTRÓNICO</td>
                                        <td>{{$admin_empresa->correo}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "bold">FECHA REGISTRO</td>
                                        <td>{{$admin_empresa->created_at}}</td>
                                    </tr>
                            </table>
                        </div>
                        <a href="{{ route('admin_empresas.index') }}"><button class="btn btn-success" type = "button">Regresar</button></a>
                   </div> 
              </div>
                
              
          </div>
</div>



<script type="text/javascript">
    jQuery(function($) {
    });
    

</script>

@endsection



