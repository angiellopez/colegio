@extends('layout_admin')

@section('content')

<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>ACCESO DENEGADO</strong>
                </div>
                <div class="card-body card-block">
                <div class = "alert alert-danger"><i class = "fa fa-times-circle"></i> ¡Usted no tiene permisos para acceder a este lugar!</div>


                </div>
            </div>
        </div>
</div>
@endsection


