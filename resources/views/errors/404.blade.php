@extends('layout_defecto')

@section('contenido')
    <div class = "alert alert-danger"><i class = "fa fa-info-circle"></i> ¡El lugar al que intenta acceder no existe!</div>
@endsection
