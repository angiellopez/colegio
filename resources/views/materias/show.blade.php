@extends('layout_admin')

@section('content')





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                </div>
                <div class="card-body card-block">

                    <div class = "table-responsive">
                        <table class="table table-show table-bordered">
                                <tr>
                                        <td class = "font-bold">MATERIA</td>
                                        <td>{{$materia->nombre}}</td>
                                </tr>
                        </table>
                    </div>
                    <br>
                    <a href="{{ route('materias.index') }}"><button class="btn btn-sm btn-success" type = "button">Regresar</button></a>
                    
                
                </div>
            </div>
        </div>
</div>    



@endsection



