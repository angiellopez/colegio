@isset($ciudades)
  <option value="" selected disable>SELECCIONE</option>
  @if(!empty($ciudades))
    @foreach($ciudades as $ciudad)
      <option value="{{$ciudad->id}}">{{strtoupper($ciudad->titulo)}}</option>
    @endforeach
  @endif
@endisset


@isset($departamentos)
  <option value="" selected disable>SELECCIONE</option>
  @if(!empty($departamentos))
    @foreach($departamentos as $departamento)
      <option value="{{$departamento->id}}">{{strtoupper($departamento->titulo)}}</option>
    @endforeach
  @endif
@endisset
