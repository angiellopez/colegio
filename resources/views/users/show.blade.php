@extends('layout_admin')

@section('content')





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                </div>
                <div class="card-body card-block">



                <div class = "table-responsive">
                    <table class="table table-show table-sinbordes table-list table-striped table-bordered">
                            <tr>
                                <td class = "font-bold">IDENTIFICACIÓN</td>
                                <td>{{$user->persona->identificacion}}</td>
                            </tr>
                            <tr>
                                <td class = "font-bold">NOMBRES</td>
                                <td>{{$user->persona->nombres}}</td>
                            </tr>
                            <tr>
                                <td class = "font-bold">CELULAR</td>
                                <td>{{$user->persona->celular}}</td>
                            </tr>
                            <tr>
                                <td class = "font-bold">EMAIL</td>
                                <td>{{$user->persona->correo}}</td>
                            </tr>
                            <tr>
                                <td class = "font-bold">USUARIO</td>
                                <td>{{$user->user}}</td>
                            </tr>
                            <tr>
                                <td class = "font-bold">ESTADO</td>
                                <td>{{$user->estado == '0' ? 'INACTIVO':'ACTIVO'}}</td>
                            </tr>
                            <tr>
                                <td class = "font-bold">PERFIL</td>
                                <td>{{$user->nivel_nombre($user->nivel)}}</td>
                            </tr>
                    </table>
                </div>
                <br>
                <a href="{{ route('users.index') }}"><button class="btn btn-success btn-sm" type = "button">Regresar</button></a>


                
                </div>
            </div>
        </div>
</div>    

@endsection

