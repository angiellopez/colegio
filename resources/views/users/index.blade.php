@extends('layout_admin')

@section('content')





    <div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                    <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm  "><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo</a></header>

                </div>
                <div class="card-body card-block">


                    @if ($users->isNotEmpty())
                    <div class = "table-responsive">
                        <table class="table table-hover table-bordered" id="sampleTable" style = "min-width:800px!important;">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombres</th>
                                    <th>Celular</th>
                                    <th>Email</th>
                                    <th>Usuario</th>
                                    <th>Estado</th>
                                    <th>Fecha Registro</th>
                                    <th style = "min-width:120px!important;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->persona != null ? $user->persona->nombres : ""}}</td>
                                <td>{{ $user->persona != null ? $user->persona->celular : ""}}</td>
                                <td>{{ $user->persona != null ? $user->persona->correo : "" }}</td>
                                <td>{{ $user->user }}</td>
                                <td>{{ $user->estado == '1'?'ACTIVO':'INACTIVO' }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td><center>
                                    <form id = "form{{$user->id}}" class = "form-table" action="{{ route('users.destroy', $user->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a class="btn btn-sm btn-primary" title = "Ver" href="{{ route('users.show',['id'=>$user->id]) }}"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                        <a class="btn btn-sm btn-success"  title = "Modificar" href="{{ route('users.edit',['id'=>$user->id]) }}"><i class="fa green fa-pencil-square" aria-hidden="true"></i></a>
                                        <a class="btn btn-sm btn-danger"  title = "Eliminar" href="" onclick="eliminar({{$user->id}},event)"><i class="fa green fa-times-circle" aria-hidden="true"></i></a>
                                    </form></center>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if(session()->has('mensaje'))
                            <div class="alert alert-success">
                                {{ session()->get('mensaje') }}
                            </div>
                        @endif
                    </div>
                    @if(session()->has('alerta'))
                        <div class="alert alert-danger">
                            {{ session()->get('alerta') }}
                        </div>
                    @endif
                    @else
                        <p>No hay users registrados.</p>
                    @endif
                    <span  style = "display:none;" id = "alert-busqueda">
                        Cargando...
                        <img style="width: 30px;" src="{{ asset('dashboard/img/cargando.gif') }}" />
                    </span>
                    <br>
                    <span  style = "display:none;" id = "alert-busqueda">
                        Cargando...
                        <img style="width: 30px;" src="{{ asset('dashboard/img/cargando.gif') }}" />
                    </span>
                    <br>
                    <div class="tile-footer">
                        <a href="{{ route('configuracion.index') }}"><button class="btn btn-sm btn-success" type = "button">Regresar</button></a>
                    </div>  
                </div>
                
            </div>
        </div>
    </div>


<script type="text/javascript">
    jQuery(function($) {
        
    });
    function eliminar(id,event){
        event.preventDefault();
        bootbox.confirm({
            message: "¿Está seguro que desea eliminar el registro?",
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar',
                    className: 'btn-success'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result == true){
                    $("#alert-busqueda").show();
                    $("#form"+id).submit();
                }
            }
        });
    }
</script>
@endsection
