@isset($oferta)
<div class = "row">
    <div class = "col-md-12">
        <div class = "table-responsive">
            <table class="table table-sinbordes table-list table-striped table-bordered">
                   
                    <tr>
                            <td class = "bold">TITULO</td>
                            <td colspan = "5">{{$oferta->titulo}}</td>
                            
                    </tr>
                    <tr>
                            <td class = "bold">DESCRIPCIÓN</td>
                            <td colspan = "5">{{$oferta->descripcion}}</td>
                    </tr>
                    <tr>
                            <td class = "bold">REQUISITOS</td>
                            <td colspan = "5">{{$oferta->requisitos}}</td>
                    </tr>
                    <tr>
                            <td class = "bold">ÁREA</td>
                            <td>{{$oferta->area->titulo}}</td>
                            @if(Auth::user()->empleado->tipo_persona != '3')
                            <td class = "bold">VALOR PROYECTO</td>
                            @endif
                            <td>${{number_format($oferta->salario,2,'.',',') }}</td>
                            <td class = "bold">ESTADO OFERTA</td>
                            <td>
                            <span style = "background: {{$oferta->estado_oferta->color}};" class="label-estado">
                                {{ $oferta->estado_oferta->titulo }}
                            </span>
                            </td>
                    </tr>
                   
            </table>
        </div>
    </div> 
</div>
@endisset



@isset($freelancer)
<div class = "row">
    <div class = "col-md-12">
        <div class = "table-responsive">
        <table class="table table-sinbordes table-list table-striped table-bordered">
                    <tr>
                            <td class = "bold">IDENTIFICACIÓN</td>
                            <td>{{$freelancer->identificacion}}</td>
                            <td class = "bold">NOMBRES</td>
                            <td>{{$freelancer->nombres}}</td>
                            <td class = "bold">APELLIDOS</td>
                            <td>{{$freelancer->apellidos}}</td>
                    </tr>
                    <tr>
                        <td class = "bold">DEPARTAMENTO</td>
                        <td>{{$freelancer->departamento($freelancer->departamento_id)}}</td>
                        <td class = "bold">CIUDAD</td>
                        <td>{{$freelancer->ciudad($freelancer->ciudad_id)}}</td>
                        <td class = "bold">BARRIO</td>
                        <td>{{$freelancer->barrio}}</td>
                    </tr>
                    <tr>
                        
                        @if(Auth::user()->empleado->tipo_persona != '2')
                        <td class = "bold">DIRECCION</td>
                        <td>{{$freelancer->direccion}}</td>
                        <td class = "bold">CELULAR</td>
                        <td>{{$freelancer->celular}}</td>
                        <td class = "bold">CORREO ELECTRÓNICO</td>
                        <td>{{$freelancer->correo}}</td>
                        @endif
                        
                    </tr>
                    <tr>
                        <td class = "bold">OCUPACIÓN</td>
                        <td>{{$freelancer->ocupacion($freelancer->ocupacion_id)}}</td>
                        <td class = "bold">PROFESIÓN</td>
                        <td>{{$freelancer->profesion($freelancer->profesion_id)}}</td>
                        @if(Auth::user()->empleado->tipo_persona != '2')
                        <td class = "bold">FECHA REGISTRO</td>
                        <td>{{$freelancer->created_at}}</td>
                        @endif
                    </tr>
                    @if(Auth::user()->empleado->tipo_persona != '2')
                        @if($freelancer->hv != '')
                        <tr>
                            <td class = "bold">HOJA DE VIDA</td>
                            <td>
                                <a class="btn2 btn-primary" target = "_blank" title = "Hoja de Vida" href="{{ asset('uploads/') }}{{"/".$freelancer->hv}}"><i class="fa fa-download" aria-hidden="true"></i> Descargar</a>
                            </td>
                        </tr>
                        @endif
                    @endif
                    
            </table>
        </div>
    </div> 
</div>

@endisset


@isset($empresa)

<div class = "row">
    <div class = "col-md-12">
        <div class = "table-responsive">
        <table class="table table-sinbordes table-list table-striped table-bordered">
                    <tr>
                            <td class = "bold">NIT</td>
                            <td>{{$empresa->nit}}</td>
                            <td class = "bold">RAZÓN SOCIAL</td>
                            <td>{{$empresa->razon}}</td>
                            <td class = "bold">RÉGIMEN</td>
                            <td>{{$empresa->regimen($empresa->regimen_id)}}</td>
                    </tr>
                    @if(Auth::user()->empleado->tipo_persona != '3')
                    <tr>
                            <td class = "bold">IDENTIFICACIÓN</td>
                            <td>{{$empresa->identificacion}}</td>
                            <td class = "bold">NOMBRES</td>
                            <td>{{$empresa->nombres}}</td>
                            <td class = "bold">APELLIDOS</td>
                            <td>{{$empresa->apellidos}}</td>
                    </tr>
                    <tr>
                        <td class = "bold">DEPARTAMENTO</td>
                        <td>{{$empresa->departamento($empresa->departamento_id)}}</td>
                        <td class = "bold">CIUDAD</td>
                        <td>{{$empresa->ciudad($empresa->ciudad_id)}}</td>
                        <td class = "bold">DIRECCION</td>
                        <td>{{$empresa->direccion}}</td>
                    </tr>
                    <tr>
                        <td class = "bold">BARRIO</td>
                        <td>{{$empresa->barrio}}</td>
                        <td class = "bold">CELULAR</td>
                        <td>{{$empresa->celular}}</td>
                        <td class = "bold">CORREO ELECTRÓNICO</td>
                        <td>{{$empresa->correo}}</td>
                    </tr>
                    <tr>
                        <td class = "bold">FECHA REGISTRO</td>
                        <td>{{$empresa->created_at}}</td>
                    </tr>
                    @endif

                    
                    
            </table>
        </div>
    </div> 
</div>
@endisset