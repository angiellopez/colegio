@extends('layout_admin')

@section('content')



<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                </div>
                <div class="card-body card-block">

                    <div class = "row">
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="{{ route('configuracion.empresa') }}" >
                                        <span class="desc configuracion">Empresa</span>
                                        <div class="icon">
                                            <i class="fa fa-home"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="{{ route('users.index') }}" >
                                        <span class="desc configuracion">Usuarios</span>
                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="{{ route('cargos.index') }}" >
                                        <span class="desc configuracion">Cargos</span>
                                        <div class="icon">
                                            <i class="fa fa-list"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                    </div>  

                    <div class = "row">
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="{{ route('materias.index') }}" >
                                        <span class="desc configuracion">Materias</span>
                                        <div class="icon">
                                            <i class="fa fa-home"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="{{ route('ciclos.index') }}" >
                                        <span class="desc configuracion">Ciclos</span>
                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div class="statistic__item statistic__item--blue">
                                    <a href="{{ route('asignaciones.index') }}" >
                                        <span class="desc configuracion">Asignaciones</span>
                                        <div class="icon">
                                            <i class="fa fa-list"></i>
                                        </div>
                                    </a>
                                </div>
                        </div>
                    </div>  
                    
                </div>
            </div>
        </div>
</div>    








@endsection


