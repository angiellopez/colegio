@extends('layout_admin')

@section('content')





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                </div>
                <div class="card-body card-block">

                    <div class = "table-responsive">
                        <table class="table table-show table-bordered">
                                <tr>
                                        <td class = "font-bold">CICLO</td>
                                        <td>{{$ciclo->nombre}}</td>
                                </tr>
                                <tr>
                                        <td class = "font-bold">AÑO</td>
                                        <td>{{$ciclo->anio}}</td>
                                </tr>
                                <tr>
                                        <td class = "font-bold">FECHA INICIO</td>
                                        <td>{{$ciclo->fecha_inicio}}</td>
                                </tr>
                                <tr>
                                        <td class = "font-bold">FECHA FIN</td>
                                        <td>{{$ciclo->fecha_fin}}</td>
                                </tr>
                        </table>
                    </div>
                    <br>
                    <a href="{{ route('ciclos.index') }}"><button class="btn btn-sm btn-success" type = "button">Regresar</button></a>
                    
                
                </div>
            </div>
        </div>
</div>    



@endsection



