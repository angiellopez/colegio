@extends('layout_admin')

@section('content')

<?php
        if(isset($ciclo)){
            $nombre = $ciclo->nombre;
            $fecha_inicio = $ciclo->fecha_inicio;
            $fecha_fin = $ciclo->fecha_fin;
            $anio = $ciclo->anio;
        }else{
            $nombre = "";
            $fecha_inicio = "";
            $fecha_fin = "";
            $anio = "";
        }
    ?>






<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                </div>
                <div class="card-body card-block">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <h6>Para continuar debe corregir los siguientes errores:</h6>
                            <br>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                        </div>
                    @endif

                    <form method="POST" id = "formulario" action="{{ $accion }}" files="true" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ $metodo }}
                        <div class = "row">
                            <div class = "col-md-6">
                                <div class="form-group">
                                    <label>Nombre </label>
                                    <input required type="text" class="form-control" name="nombre" id="nombre" value="{{ old('nombre',$nombre) }}">
                                </div>
                            </div>
                            <div class = "col-md-6">
                                <div class="form-group">
                                    <label>Año </label>
                                    <select name="anio" id="anio" class = "form-control">
                                        <option value="2019">2019</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "col-md-6">
                                <div class="form-group">
                                    <label>Fecha Inicio </label>
                                    <input required type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" value="{{ old('fecha_inicio',$fecha_inicio) }}">
                                </div>
                            </div>
                            <div class = "col-md-6">
                                <div class="form-group">
                                    <label>Fecha Fin </label>
                                    <input required type="date" class="form-control" name="fecha_fin" id="fecha_fin" value="{{ old('fecha_fin',$fecha_fin) }}">
                                </div>
                            </div>
                            
                        </div>
                        <span  style = "display:none;" id = "alert-busqueda">
                                Cargando...
                                <img style="width: 30px;" src="{{ asset('uploads/cargando.gif') }}" />
                            </span>

                            @if(session()->has('mensaje'))
                                <div class="alert alert-success">
                                    {{ session()->get('mensaje') }}
                                </div>
                            @endif
                            <div class="tile-footer">
                                <button type="submit" id = "btn-enviar" class="btn btn-sm btn-primary">{{$boton}}</button>
                                <a href="{{ route('ciclos.index') }}"><button class="btn btn-sm btn-success" type = "button">Regresar</button></a>
                            </div>
                    </form>                                 



                </div>
            </div>
        </div>
</div>








<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function(){
            $("#alert-busqueda").show();
        })

        $('#btn_imagen').click(function(event){
            event.preventDefault();
            $('#urlimagen').click();
        })

        $('#urlimagen').change(function() {
            if($("#urlimagen").val() != ''){
                var file = $('#urlimagen')[0].files[0].name;
                $("#btn_imagen").html("Seleccionado");
            }else{
                $("#btn_imagen").html("Seleccionar");
            }
        });

        $('#btn_archivo').click(function(event){
            event.preventDefault();
            $('#urlarchivo').click();
        })

        $('#urlarchivo').change(function() {
            if($("#urlarchivo").val() != ''){
                var file = $('#urlarchivo')[0].files[0].name;
                $("#btn_archivo").html("Seleccionado");
            }else{
                $("#btn_archivo").html("Seleccionar");
            }
        });
    });
    

</script>
@endsection

