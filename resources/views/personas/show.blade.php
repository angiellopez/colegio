@extends('layout_admin')

@section('content')




<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                </div>
                <div class="card-body card-block">


                        <div class = "table-responsive">
                            <table class="table table-show table-sinbordes table-list table-striped table-bordered">
                                    <tr>
                                            <td class = "font-bold">IDENTIFICACIÓN</td>
                                            <td>{{$persona->identificacion}}</td>
                                    </tr>
                                    <tr>
                                            <td class = "font-bold">NOMBRES</td>
                                            <td>{{$persona->nombres}}</td>
                                    </tr>
                                    <tr>
                                            <td class = "font-bold">APELLIDOS</td>
                                            <td>{{$persona->apellidos}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "font-bold">DIRECCION</td>
                                        <td>{{$persona->direccion}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "font-bold">BARRIO</td>
                                        <td>{{$persona->barrio}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "font-bold">CELULAR</td>
                                        <td>{{$persona->celular}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "font-bold">CORREO ELECTRÓNICO</td>
                                        <td>{{$persona->correo}}</td>
                                    </tr>
                                    <tr>
                                        <td class = "font-bold">FECHA REGISTRO</td>
                                        <td>{{$persona->created_at}}</td>
                                    </tr>
                            </table>
                        </div>
                        <br>
                        <a href="{{ $regresar }}"><button class="btn btn-success btn-sm" type = "button">Regresar</button></a>
                
                
                </div>
            </div>
        </div>
</div>    




<script type="text/javascript">
    jQuery(function($) {
    });
    

</script>

@endsection



