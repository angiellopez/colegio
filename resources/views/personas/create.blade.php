@extends('layout_admin')

@section('content')

<?php
        if(isset($persona)){
            $tipo_documento_id = $persona->tipo_documento_id;
            $identificacion = $persona->identificacion;
            $nombres = $persona->nombres;
            $apellidos = $persona->apellidos;
            $departamento_id = $persona->departamento_id;
            $ciudad_id = $persona->ciudad_id;
            $direccion = $persona->direccion;
            $barrio = $persona->barrio;
            $celular = $persona->celular;
            $correo = $persona->correo;
        }else{
            $tipo_documento_id = "";
            $identificacion = "";
            $nombres = "";
            $apellidos = "";
            $area_id = "";
            $pais_id = "";
            $departamento_id = "";
            $ciudad_id = "";
            $direccion = "";
            $barrio = "";
            $celular = "";
            $correo = "";
        }
    ?>





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                </div>
                <div class="card-body card-block">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <h6>Para continuar debe corregir los siguientes errores:</h6>
                            <br>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                        </div>
                    @endif

                    <form method="POST" id = "formulario" action="{{ $accion }}" files="true" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ $metodo }}
                        <div class = "row">

                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Tipo Documento</label>
                                    <select class="form-control" name="tipo_documento_id" id="tipo_documento_id" required aria-required="true">
                                        <option value="" disabled selected>SELECCIONE</option>
                                        @foreach($tipo_documentos as $tipo_documento)
                                        <option value="{{$tipo_documento->id}}" {{ old('tipo_documento_id', $tipo_documento_id) == $tipo_documento->id ? 'selected' : ''}}>{{$tipo_documento->titulo}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Identificación </label>
                                    <input required type="number" class="form-control" name="identificacion" id="identificacion" value="{{ old('identificacion',$identificacion) }}">
                                </div>
                            </div>
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Nombres </label>
                                    <input required type="text" class="form-control" name="nombres" id="nombres" value="{{ old('nombres',$nombres) }}">
                                </div>
                            </div>
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Apellidos </label>
                                    <input required type="text" class="form-control" name="apellidos" id="apellidos" value="{{ old('apellidos',$apellidos) }}">
                                </div>
                            </div>
                        </div>
                        <div class = "row">
                            
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Departamento</label>
                                    <select class="form-control" name="departamento_id" id="departamento_id" required aria-required="true">
                                        <option value="" disabled selected>SELECCIONE</option>
                                        @foreach($departamentos as $departamento)
                                        <option value="{{$departamento->id}}" {{ old('departamento_id', $departamento_id) == $departamento->id ? 'selected' : ''}}>{{strtoupper($departamento->titulo)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Ciudad</label>
                                    <select class="form-control" name="ciudad_id" id="ciudad_id" required aria-required="true">
                                        <option value="" disabled selected>SELECCIONE</option>
                                        @foreach($ciudades as $ciudad)
                                        <option value="{{$ciudad->id}}" {{ old('ciudad_id', $ciudad_id) == $ciudad->id ? 'selected' : ''}}>{{strtoupper($ciudad->titulo)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Dirección </label>
                                    <input required type="text" class="form-control" name="direccion" id="direccion" value="{{ old('direccion',$direccion) }}">
                                </div>
                            </div>
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Barrio </label>
                                    <input required type="text" class="form-control" name="barrio" id="barrio" value="{{ old('barrio',$barrio) }}">
                                </div>
                            </div>
                            
                        </div>
                        <div class = "row">
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Celular </label>
                                    <input required type="text" class="form-control" name="celular" id="celular" value="{{ old('celular',$celular) }}">
                                </div>
                            </div>
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Correo electrónico </label>
                                    <input required type="text" class="form-control" name="correo" id="correo" value="{{ old('correo',$correo) }}">
                                </div>
                            </div>
                            
                        </div>
                                    
                        <span  style = "display:none;" id = "alert-busqueda">
                                Cargando...
                                <img style="width: 30px;" src="{{ asset('uploads/cargando.gif') }}" />
                            </span>

                            @if(session()->has('mensaje'))
                                <div class="alert alert-success">
                                    {{ session()->get('mensaje') }}
                                </div>
                            @endif
                            <div class="tile-footer">
                                <button type="submit" id = "btn-enviar" class="btn btn-primary btn-sm">{{$boton}}</button>
                                <a href="{{ $regresar }}"><button class="btn btn-success btn-sm" type = "button">Regresar</button></a>
                            </div>
                    </form>                



                </div>
            </div>
        </div>
</div>






<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function(){
            $("#alert-busqueda").show();
        })

        $('#btn_imagen').click(function(event){
            event.preventDefault();
            $('#urlimagen').click();
        })

        $('#urlimagen').change(function() {
            if($("#urlimagen").val() != ''){
                var file = $('#urlimagen')[0].files[0].name;
                $("#btn_imagen").html("Seleccionado");
            }else{
                $("#btn_imagen").html("Seleccionar");
            }
        });

        $('#btn_archivo').click(function(event){
            event.preventDefault();
            $('#urlarchivo').click();
        })

        $('#urlarchivo').change(function() {
            if($("#urlarchivo").val() != ''){
                var file = $('#urlarchivo')[0].files[0].name;
                $("#btn_archivo").html("Seleccionado");
            }else{
                $("#btn_archivo").html("Seleccionar");
            }
        });

        $("select[name='departamento_id']").change(function(){
            var departamento = $(this).val();
            $.ajax({
                url: "{{ route('personas.cargar_ciudades') }}",
                method: 'POST',
                data: {id:departamento, "_token": "{{ csrf_token() }}"},
                success: function(data) {
                    $("select[name='ciudad_id'").html('');
                    $("select[name='ciudad_id'").html(data.options);
                }
            });
        });

        $("select[name='pais_id']").change(function(){
            var pais = $(this).val();
            $.ajax({
                url: "{{ route('personas.cargar_departamentos') }}",
                method: 'POST',
                data: {id:pais, "_token": "{{ csrf_token() }}"},
                success: function(data) {
                    $("select[name='departamento_id'").html('');
                    $("select[name='departamento_id'").html(data.options);
                }
            });
        });



    });
    

</script>
@endsection

