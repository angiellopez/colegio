@extends('layout_admin')

@section('content')





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                    <a href="{{ $nuevo }}" class="btn btn-primary btn-sm  "><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo</a></header>

                </div>
                <div class="card-body card-block">
                    @if ($personas->isNotEmpty())
                    <div class = "table-responsive">
                        <table class="table table-hover table-bordered" id="sampleTable" style = "min-width:800px!important;">
                            <thead>
                                <tr>
                                        <th>ID</th>
                                        <th>Tipo_Doc.</th>
                                        <th>Num_Doc.</th>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Dirección</th>
                                        <th>Celular</th>
                                        <th>Email</th>
                                        <th>Fecha</th>
                                    <th style = "min-width:120px!important;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($personas as $persona)
                            <tr>
                                <td>{{ $persona->id}}</td>
                                <td>{{ $persona->tipo_documento->titulo }}</td>
                                <td>{{ $persona->identificacion }}</td>
                                <td>{{ $persona->nombres }}</td>
                                <td>{{ $persona->apellidos }}</td>
                                <td>{{ $persona->direccion }}</td>
                                <td>{{ $persona->celular }}</td>
                                <td>{{ $persona->correo }}</td>
                                <td>{{ $persona->created_at }}</td>
                                <td><center>
                                    <form id = "form{{$persona->id}}" class = "form-table" action="{{ route('personas.destroy', $persona->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a class="btn btn-primary btn-sm" title = "Ver" href="{{ route('personas.show',['id'=>$persona->id]) }}"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                        <a class="btn btn-success btn-sm"  title = "Modificar" href="{{ route('personas.edit',['id'=>$persona->id]) }}"><i class="fa green fa-pencil-square" aria-hidden="true"></i></a>
                                        <a class="btn btn-danger btn-sm"  title = "Eliminar" href="" onclick="eliminar({{$persona->id}},event)"><i class="fa green fa-times-circle" aria-hidden="true"></i></a>
                                    </form></center>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if(session()->has('mensaje'))
                            <div class="alert alert-success">
                                {{ session()->get('mensaje') }}
                            </div>
                        @endif
                    </div>
                    @if(session()->has('alerta'))
                        <div class="alert alert-danger">
                            {{ session()->get('alerta') }}
                        </div>
                    @endif
                    @else
                        <p>{{$sin_registros}}</p>
                    @endif
                    <span  style = "display:none;" id = "alert-busqueda">
                        Cargando...
                        <img style="width: 30px;" src="{{ asset('dashboard/img/cargando.gif') }}" />
                    </span>
                </div>
            </div>
        </div>
</div>






		<script type="text/javascript">
			jQuery(function($) {
                
			});
            function eliminar(id,event){
                event.preventDefault();
                bootbox.confirm({
                    message: "Está seguro que desea eliminar el registro?",
                    buttons: {
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirmar',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if(result == true){
                            $("#alert-busqueda").show();
                            $("#form"+id).submit();
                        }
                    }
                });
            }
		</script>
@endsection









