@extends('layout_index') @section('contenedor')

<!--------------------------------------------------------------------SLIDER ----------------------------------------------------------------------->


<div id="main-slide-grand" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach($sliders as $index => $slider)
        <li data-target="#main-slide-grand" data-slide-to="{{$index}}" class="<?php if($index == '0') echo "active";?>"></li>
        @endforeach
        
    </ol>
    <div class="carousel-inner">

        <?php 
        $contador = 0;
        foreach ($sliders as $slider) {
            ?>
            <div class="carousel-item <?php if($contador == 0) echo " active ";?>">
                <img class="d-block w-100 imagenslider" src="{{asset('uploads/')}}/{{$slider->urlimagen}}" alt="">
                <div class="carousel-caption d-md-block">
                    <h1 class="wow fadeInDown color-slider" data-wow-delay=".4s">{{$slider->titulo}}</h1>
                    <div class ="fadeInUp wow" data-wow-delay=".6s">
                        <p style=" font-family: Raleway, sans-serif" >{!!$slider->descripcion!!}</p>
                    </div>
                    
                    <center>
                    <a target="_blank" href="{{$slider->linkenlace}}" class="fadeInLeft wow btn btn-common" data-wow-delay=".6s">{{$slider->nomenlace}}</a>
                    </center>
                    <!-- <a href="" class="fadeInRight wow btn btn-border btn-lg " data-wow-delay=".6s">Explore More</a> -->
                </div>
            </div>
       

        <?php
            $contador++;
        }
        ?>

    </div>

    <a class="carousel-control-prev" href="#main-slide-grand" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#main-slide-grand" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>

</div>


<!-------------------------------------------------------------------- BARRA -------------------------------------------------------------------->

<div class = "container-fluid">
    <div class="w-auto p-3 row barra">
        <div class="col-md-9">
            <center><h1 class="letrabarra letrab">{{$barra->titulo}}</h1></center>
        </div>
        <div class="col-md-3">
            <a style=" float: right;" target="_blank" href="{{$barra->linkenlace}}" class="botonbarra" data-wow-delay=".6s">{{$barra->subtitulo}}</a>

        </div>
    </div>
</div>


<!-------------------------------------------------------------------- PUBLICACIONES -------------------------------------------------------------------->
<br>
<section id="service-infinity" class="service-infinity section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <div class="block">
                    <h2 style = "font-family: 'Open Sans'!important;">{{$publicacion->titulo}}</h2>
                    <!--<p class="pnoticias">{{$publicacion->descripcion}}</p>-->
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($publicaciones as $publicacion)
            <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                <div class="block">
                    <h3 class="">{{$publicacion->titulo}}</h3>
                    <a class="lightbox" href="{{route('index.publicaciones',['publicacion_id'=>$publicacion->id])}}">
                        <div class="contenedor-imagen">
                             <img src="{{asset('uploads/')}}/{{$publicacion->urlimagen}}" alt="" class="carousel-item-imagen">
                        </div>
                    </a>
                    <p class="pnoticias">{!!substr($publicacion->descripcion,0,180)."..."!!}
                    </p>
                </div>
            </div>
            @endforeach

        </div>
    </div>
    <!-- .container close -->
</section>


<!-------------------------------------------------------------------- REGISTRO FREELANCER -------------------------------------------------------------------->

<section class="call-to-action section" style="padding: 90px 0px; background: url({{asset('uploads/')}}/{{$freelancer->urlimagen}});
background-attachment: fixed;
background-repeat: no-repeat;
background-size: cover;
color: #fff;
position: relative;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow text-center animated" style="visibility: visible;">
                <div class="block">
                    <h2>{{$freelancer->titulo}}</h2>
                    <p>{{$freelancer->descripcion}}</p>
                    <br>
                    <a href="{{$freelancer->linkenlace}}" class="botonfreelancer site-animate fadeInUp site-animated">{{$freelancer->nomenlace}}</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-------------------------------------------------------------------- AREAS -------------------------------------------------------------------->
<br>
<section class="site-section bg-light " id="section-offer ">
    <div class="container-fluid">

        <div class="row ">
            <div class="col-md-12 text-center mb-5 site-animate fadeInUp site-animated ">
                <h2 class="tituloh2">{{$area->titulo}}</h2>
            </div>
            <div class="col-md-12 ">
                <div class="owl-carousel site-owl owl-loaded owl-drag ">

                    @foreach($areas as $area)
                    <div class="owl-item active img-fluid itemarea">
                        <div class="item ">
                            <div class="media d-block mb-4 text-center site-media site-animate border-0 fadeInUp site-animated ">
                                <a href="{{route('index.servicios',['area_id'=>$area->id])}}">
                                    <div class = "contenedor-imagen">
                                        <img src="{{asset( 'uploads/')}}/{{$area->urlimagen}}" class="carousel-item-imagen">
                                    </div>
                                </a>
                                <div class="media-body p-4">
                                    <h5 class="mt-0 h4" style="color: #104e75">{{$area->titulo}}</h5>
                                    <!--<div>
                                        <p class="pnoticias" style="color: #104e75; ">{!!substr($area->descripcion,0,150)."..."!!}</p>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
<!-------------------------------------------------------------------- REGISTRO EMPRESA -------------------------------------------------------------------->

<section class="call-to-action section" style="padding: 90px 0px;
background-image: url({{asset('uploads/')}}/{{$regempresa->urlimagen}});
background-attachment: fixed;
background-repeat: no-repeat;
background-size: cover;
color: #fff;
position: relative;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow text-center animated" style="visibility: visible;">
                <div class="block">
                    <h2>{{$regempresa->titulo}}</h2>
                    <p>{{$regempresa->descripcion}}</p>
                    <br>

                    <a href="{{$regempresa->linkenlace}}" target="_blank" class="botonempresa site-animate fadeInUp site-animated" data-toggle="modal " data-target="#reservationModal ">{{$regempresa->nomenlace}}</a>




                    <!-- /.col-lg-6 -->
                </div>
            </div>
        </div>
    </div>
</section>


<!-------------------------------------------------------------------- QUIENES SOMOS -------------------------------------------------------------------->

<section class="site-section" id="section-about">
    <div class="container-fluido">
        <div class="row">
            <div class="col-md-6 site-animate fadeInUp site-animated">
                <h2 class="tituloh2"><center>{{$quienesomos->titulo}}</center></h2>
                <p class="pnoticias">{!!substr($quienesomos->descripcion,0,400)."..."!!}</p>

                <a href="{{$quienesomos->linkenlace}}" class="btn btn-info btn-lg " style = "margin-bottom:20px!important;">{{$quienesomos->nomenlace}}</a>
            </div>

            <div class="col-md-6 site-animate img fadeInRight site-animated" data-animate-effect="fadeInRight">
                <div id="carouselExampleIndicators" class="carousel slide " data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php 
                            $contador = 0;
                            foreach ($nosotros_imagenes as $imagen) {
                              ?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $contador;?>" class="<?php if($contador == 0) echo " active "; ?>"></li>
                        <?php
                              $contador = $contador + 1;
                            }
                            ?>


                    </ol>
                    <div class="carousel-inner">

                        <?php 
                            $contador = 0;
                            foreach ($nosotros_imagenes as $imagen) {
                           
                            ?>

                        <div class="carousel-item carousel-item-quienesomos<?php if($contador == 0) echo " active "; ?>">
                            <img class="imagen-slider-nosotros" src="{{asset('uploads/')}}/{{$imagen->urlimagen}}" alt="">
                        </div>

                        <?php
                            $contador = $contador + 1;
                             }
                            ?>




                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

        </div>
    </div>
</section>



</div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.928148806979!2d-77.29526038466861!3d1.2102564623437593!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e2ed364adac7313%3A0x14c377b7094682d1!2sCra.+33+%233-2%2C+Pasto%2C+Nari%C3%B1o!5e0!3m2!1ses-419!2sco!4v1566346160468!5m2!1ses-419!2sco" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

@endsection