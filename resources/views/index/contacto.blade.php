@extends('layout_index') @section('contenedor')
<!--------------------------------------------------------------------IMAGEN ----------------------------------------------------------------------->
<div id="main-slide-grand" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 imagenslider" src="{{asset('uploads/')}}/{{$contactanos->urlimagen}}">
            <div class="carousel-caption d-md-block">
                <h1 class="wow fadeInDown color-slider" data-wow-delay=".4s">{{$contactanos->titulo}}</h1>
            </div>
        </div>
    </div>
</div>


<!--------------------------------------------------------------------TITULO ----------------------------------------------------------------------->
<div style="padding: 30px">
    <h2 style="color: #104e75; text-align: center ;">{{$contactanos->nomenlace}}</h2>
    <p style="color: #104e75; text-align: center; padding: 8px; font-size: 25px">{!!$contactanos->nomarchivo!!}</p>
</div>

<!--------------------------------------------------------------------FORMULARIO ----------------------------------------------------------------------->

<div style="position:relative!important;margin-bottom:50px!important;">

    <div class="container container-formulario">
        <h2 class="titulo-formulario titulofree" style="text-align: center; font-family: Open sans;">{{$contactanos->titulo}}</h2>
        <p>{!!$contactanos->descripcion!!}</p>
        @if ($errors->any())
        <div class="alert alert-info">
            <h6 style="font-size: Open sans-serif; font-size: 20px ">Para continuar debe corregir los siguientes errores:</h6>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form method="POST" class="formulario contenidofree" id="formulario" action="{{ url('registro_sitio_web/guardar_contacto') }}" files="true" enctype="multipart/form-data">
            {{ csrf_field() }} {{ method_field('POST') }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nombre </label>
                        <input required type="text" class="form-control" name="nombre" id="nombre" value="{{ old('nombre') }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Celular </label>
                        <input required type="text" class="form-control" name="asunto" id="asunto" value="{{ old('asunto') }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Correo Electrónico</label>
                        <input required type="text" class="form-control" name="correo" id="correo" value="{{ old('correo') }}">
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Mensaje </label>
                        <textarea name="mensaje" id="mensaje" class="form-control" rows="4">{{ old('mensaje') }}</textarea>

                    </div>
                </div>
            </div>




            <br>

            <span style="display:none;" id="alert-busqueda">
                Cargando...
                <img style="width: 30px;" src="{{ asset('uploads/cargando.gif') }}" />
            </span> @if(session()->has('mensaje'))
            <div class="alert alert-success">
                {{ session()->get('mensaje') }}
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <p class="_58mv">Al hacer clic en Registrarte, acepta los 
                            <a href="{{asset('uploads')}}/{{$terminos->urlarchivo}}" id="terms-link" target="_blank" rel="nofollow">{{$terminos->nomenlace}}</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="tile-footer">
                <button type="submit" id="btn-enviar" target="_blank" class="btn btn-outline-white btn-lg site-animate fadeInUp site-animated" style="  font-family: Raleway , sans-serif;" data-toggle="modal " data-target="#reservationModal ">Registrar</button>
                <!-- <a href="{{ route('admin_freelancers.index') }}" target="_blank" class="btn btn-outline-white btn-lg site-animate fadeInUp site-animated" style="  font-family: Raleway , sans-serif;" data-toggle="modal " data-target="#reservationModal ">Regresar</a> -->


            </div>
        </form>
    </div>
</div>



<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function() {
            $("#alert-busqueda").show();
        })

        $('#btn_imagen').click(function(event) {
            event.preventDefault();
            $('#date_new').click();
        })

        $('#date_new').change(function() {
            if ($("#date_new").val() != '') {
                var file = $('#date_new')[0].files[0].name;
                $("#btn_imagen").html("Seleccionado");
            } else {
                $("#btn_imagen").html("Seleccionar");
            }
        });

        $('#btn_archivo').click(function(event) {
            event.preventDefault();
            $('#urlarchivo').click();
        })

        $('#urlarchivo').change(function() {
            if ($("#urlarchivo").val() != '') {
                var file = $('#urlarchivo')[0].files[0].name;
                $("#btn_archivo").html("Seleccionado");
            } else {
                $("#btn_archivo").html("Seleccionar");
            }
        });
    });
</script>

@endsection