@extends('layout_index') @section('contenedor')
<div id="main-slide-grand" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 imagenslider" src="{{asset('uploads/')}}/{{$noticia->urlimagen}}">
            <div class="carousel-caption d-md-block">
                <h1 class="wow fadeInDown color-slider" data-wow-delay=".4s">{{$noticia->titulo}}</h1>
            </div>
        </div>
    </div>
</div>


<!--------------------------------------------------------------------INFORMACION ----------------------------------------------------------------------->
<section class="site-section" id="section-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="tab-content text-left">
                    <div>
                        <p class="lead pnoticias" style="text-align: justify;  margin-top: 60px; margin-bottom: 60px">{!!$noticia->descripcion!!}</p>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <a href="{{$noticia->linkenlace}}" target="_blank" class = "btn btn-info">{{$noticia->nomenlace}}</a>
                            <a href="{{asset('uploads')}}/{{$noticia->urlarchivo}}" target="_blank" class = "btn btn-success">{{$noticia->nomarchivo}}</a>
                        </div>
                    </div>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection