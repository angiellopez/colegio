@extends('layout_index') @section('contenedor')
<!--------------------------------------------------------------------IMAGEN ----------------------------------------------------------------------->

<div id="main-slide-grand" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 imagenslider" src="{{asset('uploads/')}}/{{$infoferta->urlimagen}}">
            <div class="carousel-caption d-md-block">
                <h1 class="wow fadeInDown color-slider" data-wow-delay=".4s">{{$infoferta->titulo}}</h1>
            </div>
        </div>
    </div>
</div>




<!--------------------------------------------------------------------TITULO ----------------------------------------------------------------------->
<div style="padding: 30px">
    <h2 style="color: #104e75; text-align: center ;">{{$infoferta->nomenlace}}</h2>
    <p style="color: #104e75; text-align: center; padding: 0px; font-size: 35px">{{$infoferta->nomarchivo}}</p>

</div>

<!--------------------------------------------------------------------INFORMACION ----------------------------------------------------------------------->
<section class="site-section botones" id="section-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center mb-5 site-animate fadeInUp site-animated">
                <!--<h2 class="display-4">{{$infoferta->titulo}}</h2>-->
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <p class="pnoticias">{!!$infoferta->descripcion!!}</p>
                    </div>
                </div>
            </div>

            <div class="col-md-12 text-center">

                <div class="tab-pane fade show" id="pills-buscar" role="tabpanel" aria-labelledby="pills-buscar-tab" aria-expanded="true">
                    <div class="col-md-12 site-animate fadeInUp site-animated">
                            <div class="media-body">
                                <input type="text" placeholder = "Buscar" class = "form-control" id = "identificacion" name = "identificacion">
                            </div>
                    </div><br>
                    <div id = "div-buscar"></div>

                </div>

            </div>

            <div class="col-md-12 text-center">
                <ul class="nav site-tab-nav  mb-5" id="pills-tab" role="tablist">
                    @foreach($areas as $contador => $area)
                    <li class="nav-item site-animate fadeInUp site-animated" style = "margin-bottom:20px!important;">
                        <a class="nav-link <?php if($contador == 0) echo 'active';?>" id="pills-{{$area->id}}-tab" data-toggle="pill" href="#pills-{{$area->id}}" role="tab" aria-controls="pills-{{$area->id}}" aria-selected="true" aria-expanded="true">{{$area->titulo}}</a>
                    </li>
                    @endforeach

                </ul>
                <div class="tab-content text-left">
                    @foreach($areas as $contador => $area)
                    <div class="tab-pane fade  <?php if($contador == 0) echo 'active';?> show" id="pills-{{$area->id}}" role="tabpanel" aria-labelledby="pills-{{$area->id}}-tab" aria-expanded="true">
                        @foreach($ofertas as $oferta) @if($oferta->area_id == $area->id)
                        <div class="col-md-12 site-animate fadeInUp site-animated">
                            <div class="media menu-item bordecolor">
                                <div class="media-body">
                                    <h5 class="mt-0" style="font-size: 30px">{{$oferta->titulo}}</h5>
                                    <p style="color: black">{!!substr($oferta->descripcion,0,350)."..."!!}</p>
                                    <span class = "float-right">Fecha Publicación: {{$oferta->created_at}}</span><br><br>

                                    <button onclick = "abrirModal('{{$oferta->id}}');" class= "btn btn-success">Leer Más</button>
                                    <a href="{{route('index.freelancer')}}"> <button class= "btn btn-info">Postularse</button></a>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                        @endif @endforeach

                    </div>
                    @endforeach

                    


                </div>

            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="modal-personalizado" tabindex="-1" role="dialog" aria-labelledby="modal-default" style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" style = "min-width:60%!important;" role="document">
        <div class="modal-content" style = "padding:30px!important;">
          <div class="modal-header">
            <h6 class="modal-title" id="modal-title"></h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body"  >
            
            <div class = "modal-descripcion"></div>
            <h3 class="tituloh2">REQUISITOS</h3>
            <div class = "modal-requisitos"></div>
            <div class = "modal-valor"></div>
            <div class="container">
                <form method="POST" class="form" id="formulario" action="{{ url('registro_sitio_web/guardar_cotizar') }}" files="true" enctype="multipart/form-data">
                    {{ csrf_field() }} {{ method_field('POST') }}
                    <input type="hidden" value = "" id = "oferta_id" name = "oferta_id">
                    <div class="tile-footer">
                        <a href="{{route('index.freelancer')}}"> <button type="button" id="btn-enviar" target="_blank" class="btn btn-info" style="  font-family: Raleway , sans-serif;" data-toggle="modal " data-target="#reservationModal ">Postularse</button></a>
                    </div>
                    <br>
                </form>
            </div>




          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-info  ml-auto" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
</div>

<script type="text/javascript">
    jQuery(function($) {

        $("#identificacion").keypress(function(){
            
            id = $("#identificacion").val();
            buscar_ofertas(id);
        });
        $("#identificacion").blur(function(){
            id = $("#identificacion").val();
            buscar_ofertas(id);
        });


        
    });
    function abrirModal(id){

        $.ajax({
            url: "{{ route('index.cargar_oferta') }}",
            method: 'POST',
            data: {id:id, "_token": "{{ csrf_token() }}"},
            success: function(data) {
                $("#modal-personalizado .modal-title").html(data.titulo);
                $("#modal-personalizado .modal-descripcion").html(data.descripcion);
                $("#modal-personalizado .modal-requisitos").html(data.requisitos);
                $("#modal-personalizado .modal-valor").html(data.valor);
                $("#oferta_id").val(id);
            }
        });
        $("#modal-personalizado").modal();
    }

    function buscar_ofertas(id){
        $.ajax({
            url: "{{ route('index.buscar_ofertas') }}",
            method: 'POST',
            data: {'id':id,"_token": "{{ csrf_token() }}"},
            beforeSend: function() {
                //alert("buscando");
            },
            success: function(data) {
                $("#div-buscar").html('');
                $("#div-buscar").html(data.options);
            }
        });
    }

    
</script>

@endsection