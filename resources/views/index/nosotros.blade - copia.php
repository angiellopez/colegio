@extends('layout_index') @section('contenedor')
<!--------------------------------------------------------------------IMAGEN ----------------------------------------------------------------------->
<div id="main-slide-grand" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="imagenslider" src="{{asset('uploads/')}}/{{$quienesomos->urlimagen}}">
            <div class="carousel-caption d-md-block">
                <h1 class="wow fadeInDown color-slider" data-wow-delay=".4s">{{$quienesomos->titulo}}</h1>
            </div>
        </div>
    </div>
</div>



<!--------------------------------------------------------------------INFORMACION ----------------------------------------------------------------------->

<section class=" pt-125 pb-130">
    <div class="container">
        <div class=" justify-content-center">
            <div class="col-lg-12">
                <div class="section-title text-center pb-20">
                    <h1 class="title nosotrosh2">{{$quienesomos->titulo}}</h1>
                    <p class="sub-title mb-15 nosotrosh2">{{$quienesomos->descripcion}}</p>


                </div>
                <!-- section title -->
            </div>
        </div>
        <!-- row -->



        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <h1 class="title nosotrosh2">{{$quienesomos->titulo}}</h1>
                        </button>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body ">
                        <p class="sub-title mb-15 nosotrosh2 justify-content-center">{{$quienesomos->descripcion}}</p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                    <button class="btn btn-link nosotrosh2 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Collapsible Group Item #2
                    </button>
                </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                    <button class="btn btn-link nosotrosh2 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Collapsible Group Item #3
                    </button>
                </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
                </div>
            </div>
        </div>

        

        <div class = "row">
            <div class = "col-md-6">
                <div class="single-services text-center mt-30 wow fadeInUp colorseccion1" data-wow-duration="1.5s" data-wow-delay="0.4s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.4s; animation-name: fadeInUp;">
                    <h4 class="services-title" style="font-size: 40px">{{$mision->titulo}}</h4>
                    <p class="mt-20 nosotrosh2" style = "text-align:justify!important;">{!!$mision->descripcion!!}</p>
                </div>
            </div>
            <div class = "col-md-6">
                <div class="single-services text-center mt-30 wow fadeInUp colorseccion2" data-wow-duration="1.5s" data-wow-delay="0.4s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.4s; animation-name: fadeInUp;">
                    <h4 class="services-title" style="font-size: 40px">{{$vision->titulo}}</h4>
                    <p class="mt-20 nosotrosh2" style = "text-align:justify!important;">{!!$vision->descripcion!!}</p>
                </div>
            </div>
        </div>

        <div class = "row">
            <div class = "col-md-6">
                

                <div class="single-services text-center mt-30 wow fadeInUp colorseccion1" data-wow-duration="1.5s " data-wow-delay="0.6s " style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.4s; animation-name: fadeInUp; ">
                    <h4 class="services-title " style="font-size: 40px ">{{$objetivos->titulo}}</h4>
                    <p class="mt-20 nosotrosh2 " style = "text-align:justify!important;">{!!$objetivos->descripcion!!}
                    </p>
                </div>


            </div>
            <div class = "col-md-6">
                

                <div class="single-services text-center mt-30 wow fadeInUp colorseccion2" data-wow-duration="1.5s " data-wow-delay="0.6s " style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.4s; animation-name: fadeInUp; ">
                    <h4 class="services-title " style="font-size: 40px ">{{$politicas->titulo}}</h4>
                    <p class="mt-20 nosotrosh2 " style = "text-align:justify!important;">{!!$politicas->descripcion!!}
                    </p>
                </div>

            </div>
        </div>


        



        



    </div>
    <!-- row -->
    </div>
    <!-- container -->
    <br>
    <div class="row ">
        <div class="col-md-12 wow text-center animated " style="visibility: visible; ">
            <div id="separar">
                <a href="{{asset('uploads')}}/{{$portafolio->urlarchivo}}" target="_blank" class="boton4" style="font-size: 30px; background-color: #95bf2a; border: #95bf2a">{{$portafolio->nomenlace}}</a>
            </div>
        </div>
    </div>
</section>


@endsection