@extends('layout_index') @section('contenedor')
<!--------------------------------------------------------------------IMAGEN ----------------------------------------------------------------------->
<div id="main-slide-grand" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 imagenslider" src="{{asset('uploads/')}}/{{$infocoordinadores->urlimagen}}">
            <div class="carousel-caption d-md-block">
                <h1 class="wow fadeInDown color-slider" data-wow-delay=".4s">{{$infocoordinadores->titulo}}</h1>
            </div>
        </div>
    </div>
</div>

<!--------------------------------------------------------------------TITULO ----------------------------------------------------------------------->
<div style="padding: 30px">
    <h2 style="color: #104e75; text-align: center ;">{{$infocoordinadores->nomenlace}}</h2>
    <p style="color: #104e75; text-align: center; padding: 8px; font-size: 25px">{!!$infocoordinadores->nomarchivo!!}</p>
</div>
<!--------------------------------------------------------------------TITULO ----------------------------------------------------------------------->
<div class="bodycoordinadores">
    <div class="container gallery-container">

        <h1 style="padding-top: 5%">{{$infocoordinadores->titulo}}</h1>

        <p class="page-description text-center">{!!$infocoordinadores->descripcion!!}</p>

        <div class="tz-gallery">

            <div class="row">
                @foreach($imagenes as $imagen)

                <div class="col-sm-6 col-md-4">
                    <a class="lightbox">
                        <img src="{{asset('uploads')}}/{{$imagen->urlimagen}}" alt="Park">
                    </a>
                </div>
                @endforeach

            </div>

        </div>

    </div>

</div>

@endsection