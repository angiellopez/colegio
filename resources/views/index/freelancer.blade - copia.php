@extends('layout_index') @section('contenedor')
<!--------------------------------------------------------------------IMAGEN ----------------------------------------------------------------------->
<div id="main-slide-grand" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="imagenslider" src="{{asset('uploads/')}}/{{$freelancer->urlimagen}}">
            <div class="carousel-caption d-md-block">
                <h1 class="wow fadeInDown color-slider" data-wow-delay=".4s">{{$freelancer->titulo}}</h1>
                <p class="pnoticias wow fadeInDown" style = "font-weight:300!important;" data-wow-delay=".4s">sasa{{$freelancer->descripcion}}</p>
            </div>
        </div>
    </div>
</div>
<!--------------------------------------------------------------------TITULO ----------------------------------------------------------------------->
<!--<div style="padding: 30px">
    <h2 style="color: #104e75; text-align: center ;">{{$txtfreelancer->titulo}}</h2>
    <p style="color: #104e75; text-align: center; padding: 8px; font-size: 25px">{{$txtfreelancer->descripcion}}</p>
</div>-->

<!--------------------------------------------------------------------FORMULARIO ----------------------------------------------------------------------->
<br><br>
<div style="position:relative!important;margin-bottom:50px!important;">

    <div class="container container-formulario">
        <h2 class="titulo-formulario titulofree" style="text-align: center; font-family: Open sans; color: #85bb1d">Crea tu Cuenta</h2>
        @if ($errors->any())
        <div class="alert alert-info">
            <h6>Para continuar debe corregir los siguientes errores:</h6>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form method="POST" class="formulario contenidofree" id="formulario" action="{{ url('registro_sitio_web/guardar_freelancer') }}" files="true" enctype="multipart/form-data">
            {{ csrf_field() }} {{ method_field('POST') }}
            <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tipo Documento</label>
                        <select class="form-control" name="documento_id" id="documento_id" required aria-required="true">
                                <option value="" disabled selected>SELECCIONE</option>
                                @foreach($documentos as $documento)
                                <option value="{{$documento->id}}" {{ old('documento_id') == $documento->id ? 'selected' : ''}}>{{strtoupper($documento->titulo)}}</option>
                                @endforeach
                            </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Identificación </label>
                        <input required type="text" class="form-control" name="identificacion" id="identificacion" value="{{ old('identificacion') }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Nombres </label>
                        <input required type="text" class="form-control" name="nombres" id="nombres" value="{{ old('nombres') }}">
                    </div>
                </div>

            </div>



            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">

                        <label>Apellidos </label>
                        <input required type="text" class="form-control" name="apellidos" id="apellidos" value="{{ old('apellidos') }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Pais</label>
                        <select class="form-control" name="pais_id" id="pais_id" required aria-required="true">
                <option value="" disabled selected>SELECCIONE</option>
                @foreach($paises as $pais)
                <option value="{{$pais->id}}" {{ old('pais_id') == $pais->id ? 'selected' : ''}}>
                    {{strtoupper($pais->titulo)}}</option>
                @endforeach
            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Departamento</label>
                        <select class="form-control" name="departamento_id" id="departamento_id" required aria-required="true">
                <option value="" disabled selected>SELECCIONE</option>
                @foreach($departamentos as $departamento)
                <option value="{{$departamento->id}}"
                    {{ old('departamento_id') == $departamento->id ? 'selected' : ''}}>
                    {{strtoupper($departamento->titulo)}}</option>
                @endforeach
            </select>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Ciudad</label>
                        <select class="form-control" name="ciudad_id" id="ciudad_id" required aria-required="true">
                            <option value="" disabled selected>SELECCIONE</option>
                            @foreach($ciudades as $ciudad)
                            <option value="{{$ciudad->id}}" {{ old('ciudad_id') == $ciudad->id ? 'selected' : ''}}>
                                {{strtoupper($ciudad->titulo)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Dirección </label>
                        <input required type="text" class="form-control" name="direccion" id="direccion" value="{{ old('direccion') }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Barrio </label>
                        <input required type="text" class="form-control" name="barrio" id="barrio" value="{{ old('barrio') }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Ocupación</label>
                        <select class="form-control" name="ocupacion_id" id="ocupacion_id" required aria-required="true">
                                <option value="" disabled selected>SELECCIONE</option>
                                @foreach($ocupaciones as $ocupacion)
                                <option value="{{$ocupacion->id}}" {{ old('ocupacion_id') }}>{{$ocupacion->titulo}}</option>
                                @endforeach
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Profesión</label>
                        <select class="form-control" name="profesion_id" id="profesion_id" required aria-required="true">
                                <option value="" disabled selected>SELECCIONE</option>
                                @foreach($profesiones as $profesion)
                                <option value="{{$profesion->id}}" {{ old('profesion_id') }}>{{$profesion->titulo}}</option>
                                @endforeach
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Celular </label>
                        <input required type="text" class="form-control" name="celular" id="celular" value="{{ old('celular') }}">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Correo electrónico </label>
                        <input required type="text" class="form-control" name="correo" id="correo" value="{{ old('correo') }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Área</label>
                        <select class="form-control" name="area_id" id="area_id" required aria-required="true">
                                <option value="" disabled selected>SELECCIONE</option>
                                @foreach($areas as $area)
                                <option value="{{$area->id}}" {{ old('area_id') == $area->id ? 'selected' : ''}}>{{strtoupper($area->titulo)}}</option>
                                @endforeach
                            </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Hoja de Vida </label>
                        <input type="file" class="form-control" name="hv" id="hv" value="{{ old('hv') }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <p class="_58mv">Al hacer clic en Registrarte, acepta los 
                            <a href="{{asset('uploads')}}/{{$terminos->urlarchivo}}" id="terms-link" target="_blank" rel="nofollow">{{$terminos->nomenlace}}</a>
                        </p>
                    </div>
                </div>
            </div>


            <span style="display:none;" id="alert-busqueda">
                        Cargando...
                        <img style="width: 30px;" src="{{ asset('uploads/cargando.gif') }}" />
                    </span> @if(session()->has('mensaje'))
            <div class="alert alert-success">
                {{ session()->get('mensaje') }}
            </div>
            @endif
            <div class="tile-footer">
                <button type="submit" id="btn-enviar" target="_blank" class="btn btn-outline-white btn-lg site-animate fadeInUp site-animated" style="  font-family: Raleway , sans-serif;" data-toggle="modal " data-target="#reservationModal ">Registrar</button>

            </div>
        </form>
    </div>
</div>




<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function() {
            $("#alert-busqueda").show();
        })

        $("select[name='pais_id']").change(function() {
            var pais = $(this).val();
            $.ajax({
                url: "{{ route('registro_sitio_web.cargar_departamentos') }}",
                method: 'POST',
                data: {
                    id: pais,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(data) {
                    $("select[name='departamento_id'").html('');
                    $("select[name='departamento_id'").html(data.options);
                }
            });
        });

        $("select[name='departamento_id']").change(function() {
            var municipio = $(this).val();
            $.ajax({
                url: "{{ route('registro_sitio_web.cargar_ciudades') }}",
                method: 'POST',
                data: {
                    id: municipio,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(data) {
                    $("select[name='ciudad_id'").html('');
                    $("select[name='ciudad_id'").html(data.options);
                }
            });
        });




    });
</script>

@endsection