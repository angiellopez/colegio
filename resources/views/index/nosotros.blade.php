@extends('layout_index') @section('contenedor')
<!--------------------------------------------------------------------IMAGEN ----------------------------------------------------------------------->
<div id="main-slide-grand" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="imagenslider" src="{{asset('uploads/')}}/{{$quienesomos->urlimagen}}">
            <div class="carousel-caption d-md-block">
                <h1 class="wow fadeInDown color-slider" data-wow-delay=".4s">{{$quienesomos->titulo}}</h1>
            </div>
        </div>
    </div>
</div>



<!--------------------------------------------------------------------INFORMACION ----------------------------------------------------------------------->

<section class=" pt-125 pb-130">
    <div class="container-fluido">

        <div class="row ">
            <div class="col-md-12 wow text-center animated " style="visibility: visible; ">
                <div id="separar">
                    <a href="{{asset('uploads')}}/{{$portafolio->urlarchivo}}" target="_blank" class="boton4" style="background-color: #95bf2a; border: #95bf2a">{{$portafolio->nomenlace}}</a>
                </div>
            </div>
        </div>
        <div class = "row">
            <div class = "col-md-6">
            
                <div id="accordion">
                    <div class="card" style = "margin-bottom:10px!important;">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <h1 class="title nosotrosh2 titulo-mediano">{{$quienesomos->titulo}}</h1>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body ">
                            <center><img class="img-responsive img-carousel" src="{{asset('uploads/')}}/{{$quienesomos->urlimagen}}" alt=""></center>
                            <p class="mt-20 nosotrosh2" style = "text-align:justify!important;">{!!$quienesomos->descripcion!!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card" style = "margin-bottom:10px!important;">
                        <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link nosotrosh2 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <h1 class="title nosotrosh2 titulo-mediano">{{$mision->titulo}}</h1>
                            </button>
                        </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                        <center><img class="img-responsive img-carousel" src="{{asset('uploads/')}}/{{$mision->urlimagen}}" alt=""></center>
                        <p class="mt-20 nosotrosh2" style = "text-align:justify!important;">{!!$mision->descripcion!!}</p>
                        </div>
                        </div>
                    </div>
                    <div class="card" style = "margin-bottom:10px!important;">
                        <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link nosotrosh2 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <h1 class="title nosotrosh2 titulo-mediano">{{$vision->titulo}}</h1>
                            </button>
                        </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                        <center><img class="img-responsive img-carousel" src="{{asset('uploads/')}}/{{$vision->urlimagen}}" alt=""></center>
                        <p class="mt-20 nosotrosh2" style = "text-align:justify!important;">{!!$vision->descripcion!!}</p>
                        </div>
                        </div>
                    </div>
                    <!--<div class="card" style = "margin-bottom:10px!important;">
                        <div class="card-header" id="heading4">
                        <h5 class="mb-0">
                            <button class="btn btn-link nosotrosh2 collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            <h1 class="title nosotrosh2 titulo-mediano">{{$objetivos->titulo}}</h1>
                            </button>
                        </h5>
                        </div>
                        <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                        <div class="card-body">
                        <center><img class="img-responsive img-carousel" src="{{asset('uploads/')}}/{{$objetivos->urlimagen}}" alt=""></center>
                        <p class="mt-20 nosotrosh2" style = "text-align:justify!important;">{!!$objetivos->descripcion!!}</p>
                        </div>
                        </div>
                    </div>-->
                    <div class="card" style = "margin-bottom:10px!important;">
                        <div class="card-header" id="heading5">
                        <h5 class="mb-0">
                            <button class="btn btn-link nosotrosh2 collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                            <h1 class="title nosotrosh2 titulo-mediano">{{$politicas->titulo}}</h1>
                            </button>
                        </h5>
                        </div>
                        <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                        <div class="card-body">
                        <center><img class="img-responsive img-carousel" src="{{asset('uploads/')}}/{{$politicas->urlimagen}}" alt=""></center>
                        <p class="mt-20 nosotrosh2" style = "text-align:justify!important;">{!!$politicas->descripcion!!}</p>
                        </div>
                        </div>
                    </div>
                </div>
            
            
            
            </div>
            <div class = "col-md-6">
                <img style = "width:100%;" class = "img-responsive" src="{{asset('uploads')}}/{{$empresa->urlimagen}}" alt="">
                <br><br>
                <img style = "width:80px;" src="{{asset('uploads')}}/certificado.png" alt="">
                <a target = "_blank" class = "btn btn-info" style = "display:inline;font-size:18px!important;" href="{{asset('uploads')}}/{{$portafolio->urlimagen}}">{{$portafolio->linkenlace}}</a>
            </div>
        </div>
    
        
    </div>
    <!-- container -->
</section>


@endsection