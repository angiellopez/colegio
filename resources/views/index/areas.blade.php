@extends('layout_index') @section('contenedor')
<!--------------------------------------------------------------------IMAGEN ----------------------------------------------------------------------->
<div id="main-slide-grand" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="imagenslider" src="{{asset('uploads/')}}/{{$infarea->urlimagen}}">
            <div class="carousel-caption d-md-block">
                <h1 class="wow fadeInDown color-slider" data-wow-delay=".4s">{{$infarea->titulo}}</h1>
            </div>
        </div>
    </div>
</div>

<!--------------------------------------------------------------------TITULO ----------------------------------------------------------------------->
<div style="padding: 30px">
    <h2 style="color: #104e75; text-align: center ;">{{$infarea->nomenlace}}</h2>
    <p style="color: #104e75; text-align: center; padding: 8px; font-size: 30px">{{$infarea->nomarchivo}}</p>
</div>
<!--------------------------------------------------------------------TITULO ----------------------------------------------------------------------->
<div class="bodyareas">
    <div class="container-fluid galleryar-containerar">

        <h3 style="color: #104e75; text-align: center ;">{{$infarea->titulo}}</h3>
        <p style="color: #888; font-size: 18px;" class="page-description text-center">{{$infarea->descripcion}}</p>

        <div class="tz-galleryar">

            <div class="row">
                @foreach($areas as $area)
                <div class="col-sm-6 col-xs-6 col-md-3">
                    <div class="thumbnail" style="background-color: {{$area->nomimagen}}">
                        <a class="" href="{{route('index.servicios',['area_id'=>$area->id])}}">
                            <div class = "contenedor-imagen-areas">
                                <img class="carousel-item-areas" src="{{asset('uploads')}}/{{$area->urlimagen}}">
                            </div>
                        </a>
                        <div class="caption center texto-areas" style="height: 100px;">
                            <h2>{{$area->titulo}}</h2>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>

        </div>

    </div>
</div>

@endsection