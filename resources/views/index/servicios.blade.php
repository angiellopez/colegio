@extends('layout_index') @section('contenedor')
<div id="main-slide-grand" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 imagenslider" src="{{asset('uploads/')}}/{{$area->urlimagen}}">
            <div class="carousel-caption d-md-block">
                <h1 class="wow fadeInDown color-slider" data-wow-delay=".4s">{{$area->titulo}}</h1>
            </div>
        </div>
    </div>
</div>


<!--------------------------------------------------------------------INFORMACION ----------------------------------------------------------------------->
<section class="site-section" id="section-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center mb-5 site-animate fadeInUp site-animated">
                <!--<h2 class="tituloh2">{{$area->titulo}}</h2><br>-->
                <br><br>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <p class="pnoticias">{!!$area->descripcion!!}</p>
                    </div>
                </div>
            </div>

            @if ($errors->any())
                <div class="alert alert-info">
                    <h6 style="font-size: Open sans-serif; font-size: 20px ">Para continuar debe corregir los siguientes errores:</h6>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <span style="display:none;" id="alert-busqueda">
                Cargando...
                <img style="width: 30px;" src="{{ asset('uploads/cargando.gif') }}" />
            </span> 
            @if(session()->has('mensaje'))
                <div class="alert alert-success">
                    {{ session()->get('mensaje') }}
                </div>
            @endif

            <div class="col-md-12 text-center">



                <div class="tab-content text-left">
                    <div class="row">

                        @foreach($areas as $area) @foreach($servicios as $servicio) @if($servicio->area_id == $area->id)
                        <div class="col-md-6 site-animate fadeInUp site-animated">

                            <div class="media menu-item">
                                <div class="media-body">
                                    <h5 class="mt-0 servicios">{{$servicio->titulo}}</h5>
                                    <!--<p class="pnoticias">{!!substr($servicio->descripcion,0,250)."..."!!}</p>-->
                                    <button onclick = "abrirModal('{{$servicio->id}}');" class= "btn btn-success">Cotización</button>
                                </div>
                            </div>
                        </div>

                        @endif @endforeach @endforeach


                    </div>
                </div>
            </div>


        </div>

    </div>
    </div>
</section>
<br><br>

<div class="modal fade" id="modal-personalizado" tabindex="-1" role="dialog" aria-labelledby="modal-default" style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" style = "min-width:60%!important;" role="document">
        <div class="modal-content" style = "padding:30px!important;">
          <div class="modal-header">
            <h6 class="modal-title" id="modal-title"></h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body"  >
            
            <div class = "modal-descripcion"></div>
            <br>


            <div class="container container-formulario">
                <h2 class="tituloh2">{{$cotizacion->titulo}}</h2>
                <p class="pnoticias">{!!$cotizacion->descripcion!!}</p>
                
                <form method="POST" class="form" id="formulario" action="{{ url('registro_sitio_web/guardar_cotizar') }}" files="true" enctype="multipart/form-data">
                    {{ csrf_field() }} {{ method_field('POST') }}
                    <input type="hidden" value = "" id = "servicio_id" name = "servicio_id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nombre </label>
                                <input required type="text" class="form-control" name="nombre" id="nombre" value="{{ old('nombre') }}">
                            </div>
                        </div>
                    </div>
                    <div class = "row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Celular </label>
                                <input required type="text" class="form-control" name="celular" id="celular" value="{{ old('celular') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Correo electrónico </label>
                                <input required type="text" class="form-control" name="correo" id="correo" value="{{ old('correo') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Mensaje </label>
                                <textarea name="mensaje" id="mensaje" class="form-control" rows="4">{{ old('mensaje') }}</textarea>

                            </div>
                        </div>
                    </div>




                    <br>

                    
                    <div class="tile-footer">
                        <button type="submit" id="btn-enviar" target="_blank" class="btn btn-success" style="  font-family: Raleway , sans-serif;" data-toggle="modal " data-target="#reservationModal ">Registrar</button>
                    </div>
                    <br>
                </form>
            </div>




          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-info  ml-auto" data-dismiss="modal">CERRAR</button>
          </div>
        </div>
      </div>
</div>

<script type="text/javascript">
    jQuery(function($) {
        
    });
    function abrirModal(id){

        $.ajax({
            url: "{{ route('index.cargar_servicio') }}",
            method: 'POST',
            data: {id:id, "_token": "{{ csrf_token() }}"},
            success: function(data) {
                $("#modal-personalizado .modal-title").html(data.titulo);
                //$("#modal-personalizado .modal-descripcion").html(data.descripcion);
                $("#servicio_id").val(id);
            }
        });
        $("#modal-personalizado").modal();
    }
    
</script>


@endsection