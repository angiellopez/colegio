@isset($personas_tabla)
    @if ($personas_tabla->isNotEmpty())
    <div class = "table-responsive">
        <table class="table table-show table-hover table-bordered " id="sampleTable">
            

            <!--    COMIENZA CONTENIDO TABLA   -->
            <thead>
                <tr>
                    <th >DNI</th>
                    <th >Nombres</th>
                    <th >Apellidos</th>
                    <th >Celular</th>
                    <th >Email</th>
                    <th >Tipo Persona</th>
                </tr>
                </thead> 
                <tbody>
                @foreach($personas_tabla as $persona)
                <tr ondblclick= "agregar_persona('{{$persona->identificacion}}', event)">
                    <td>{{ $persona->identificacion }}</td>
                    <td>{{ $persona->nombres }}</td>
                    <td>{{ $persona->apellidos }}</td>
                    <td>{{ $persona->celular }}</td>
                    <td>{{ $persona->correo }}</td>
                    <td>{{ $persona->tipo_persona_nombre($persona->tipo_persona)}}</td>
                </tr>
                @endforeach
                </tbody>


            <!--    FIN CONTENIDO TABLA   -->

        </table>
        @if(session()->has('mensaje'))
            <div class="alert alert-success">
                {{ session()->get('mensaje') }}
            </div>
        @endif
    </div>
    @else
        <p>No hay personas registrados.</p>
    @endif
@endisset
