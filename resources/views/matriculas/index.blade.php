@extends('layout_admin')

@section('content')





    <div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                    <a href="{{ route('matriculas.create') }}" class="btn btn-primary btn-sm  "><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo</a></header>

                </div>
                <div class="card-body card-block">


                    @if ($matriculas->isNotEmpty())
                    <div class = "table-responsive">
                        <table class="table table-hover table-bordered" id="sampleTable" style = "min-width:800px!important;">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Ciclo</th>
                                    <th>Estudiante</th>
                                    <th>Fecha Matricula</th>
                                    <th>Fecha Registro</th>
                                    <th style = "min-width:120px!important;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($matriculas as $matricula)
                            <tr>
                                <td>{{ $matricula->id }}</td>
                                <td>{{ $matricula->ciclo->nombre }} :: {{ $matricula->ciclo->anio }}</td>
                                <td>{{ $matricula->estudiante->apellidos }} {{ $matricula->estudiante->nombres }}</td>
                                <td>{{ $matricula->fecha_inicio }}</td>
                                <td>{{ $matricula->created_at }}</td>
                                <td><center>
                                    <form id = "form{{$matricula->id}}" class = "form-table" action="{{ route('matriculas.destroy', $matricula->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a class="btn btn-sm btn-primary" title = "Ver" href="{{ route('matriculas.show',['id'=>$matricula->id]) }}"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                        <a class="btn btn-sm btn-success"  title = "Modificar" href="{{ route('matriculas.edit',['id'=>$matricula->id]) }}"><i class="fa green fa-pencil-square" aria-hidden="true"></i></a>
                                        <a class="btn btn-sm btn-danger"  title = "Eliminar" href="" onclick="eliminar({{$matricula->id}},event)"><i class="fa green fa-times-circle" aria-hidden="true"></i></a>
                                    </form></center>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if(session()->has('mensaje'))
                            <div class="alert alert-success">
                                {{ session()->get('mensaje') }}
                            </div>
                        @endif
                    </div>
                    @if(session()->has('alerta'))
                        <div class="alert alert-danger">
                            {{ session()->get('alerta') }}
                        </div>
                    @endif
                    @else
                        <p>No hay matriculas registrados.</p>
                    @endif
                    <span  style = "display:none;" id = "alert-busqueda">
                        Cargando...
                        <img style="width: 30px;" src="{{ asset('dashboard/img/cargando.gif') }}" />
                    </span>
                    <br>
                    <span  style = "display:none;" id = "alert-busqueda">
                        Cargando...
                        <img style="width: 30px;" src="{{ asset('dashboard/img/cargando.gif') }}" />
                    </span>
                </div>
            </div>
        </div>
    </div>


<script type="text/javascript">
    jQuery(function($) {
        
    });
    function eliminar(id,event){
        event.preventDefault();
        bootbox.confirm({
            message: "¿Está seguro que desea eliminar el registro?",
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar',
                    className: 'btn-success'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result == true){
                    $("#alert-busqueda").show();
                    $("#form"+id).submit();
                }
            }
        });
    }
</script>
@endsection
