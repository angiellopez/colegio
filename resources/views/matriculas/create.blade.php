@extends('layout_admin')

@section('content')

<?php
        if(isset($persona)){
            $persona_id = $persona->id;
            $nombres = $persona->nombres;
            $apellidos = $persona->apellidos;
            $celular = $persona->celular;
            $correo = $persona->correo;
            $identificacion = $persona->identificacion;
            $fecha = $persona->date_new;
            $ciclo_id = $matricula->ciclo_id;
            $fecha_inicio = $matricula->fecha_inicio;

            
            
            
            
        }else{
            $persona_id ="";
            $nombres ="";
            $apellidos ="";
            $celular ="";
            $correo ="";
            $identificacion = "";
            $fecha ="";
            $ciclo_id ="";
            $fecha_inicio ="";

        }
    ?>


<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                </div>
                <div class="card-body card-block">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <h6>Para continuar debe corregir los siguientes errores:</h6>
                            <br>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                        </div>
                    @endif

                    <form method="POST" id = "formulario" action="{{ $accion }}">
                        {{ csrf_field() }}
                        {{ $metodo }}
                        <div class = "row">
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Identificación Estudiante</label>
                                    @isset($persona)
                                    <input readonly type="text" class="form-control" value="{{ old('identificacion',$identificacion) }}">
                                    @else
                                    <div class="input-group">
                                        <input required type="text" class="form-control" name="cedula" id="cedula" value="{{ old('cedula',$identificacion) }}">
                                        <div id = "btn_buscar" class="input-group-append" data-toggle="modal" data-target="#exampleModal">
                                            <span style = "cursor:pointer;" class="input-group-text">
                                                <i class = "fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    @endisset
                                </div>
                            </div>   
                        </div>

                        <div class = "row">
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="hidden" id="persona_id" name="persona_id" value="{{ old('persona_id',$persona_id) }}">
                                    <input readonly type="text" id = "nombre_persona" name = "nombre_persona" class="form-control" value="{{ old('nombre_persona',$nombres) }}">
                                </div>
                            </div>   
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input readonly type="text" id = "apellido_persona" name = "apellido_persona" class="form-control" value="{{ old('apellido_persona',$apellidos) }}">
                                </div>       
                            </div> 
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Celular</label>
                                    <input readonly type="text" id = "celular" name = "celular" class="form-control" value="{{ old('celular',$celular) }}">
                                </div>
                            </div> 
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Correo Electrónico</label>
                                    <input readonly type="mail" id = "correo" name = "correo" class="form-control" value="{{ old('correo',$correo) }}">
                                </div>       
                            </div>  
                        </div>
                        <div class = "row">
                            <div class = "col-md-3">
                                <div class="form-group">
                                    <label>Ciclo</label>
                                    <select class="form-control" name="ciclo_id" id="ciclo_id" >
                                        <option value="" disabled selected>SELECCIONE</option>
                                        @foreach($ciclos as $ciclo)
                                        <option value="{{$ciclo->id}}" {{ old('ciclo_id', $ciclo_id) == $ciclo->id ? 'selected' : ''}}>{{$ciclo->nombre}} :: {{$ciclo->anio}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class = "col-md-3">
                                    <div class="form-group">
                                        <label>Fecha Inicio </label>
                                        <input required type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" value="{{ old('fecha_inicio',$fecha_inicio) }}">
                                    </div>
                                </div>
                        </div>

                        
                        <br>

                            <span  style = "display:none;" id = "alert-busqueda">
                                Cargando...
                                <img style="width: 30px;" src="{{ asset('uploads/cargando.gif') }}" />
                            </span>

                            @if(session()->has('mensaje'))
                                <div class="alert alert-success">
                                    {{ session()->get('mensaje') }}
                                </div>
                            @endif
                            @if(session()->has('alerta'))
                                <div class="alert alert-danger">
                                    {{ session()->get('alerta') }}
                                </div>
                            @endif
                            <div class="tile-footer">
                                <button type="submit" id = "btn-enviar" class="btn btn-sm btn-primary">{{$boton}}</button>
                                <a href="{{ route('matriculas.index') }}"><button class="btn btn-sm btn-success" type = "button">Regresar</button></a>
                            </div>
                    </form>                                  



                </div>
            </div>
        </div>
</div>




<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function(event){
            $("#alert-busqueda").show();
            
        })

        $("#cedula").blur(function(){
            id = $("#cedula").val();
            buscar_persona(id);
        });

        $("#btn_buscar").click(function(){
            $.ajax({
                url: "{{ route('personas.cargar_personas') }}",
                method: 'POST',
                data: {'tipo_persona':'2',"_token": "{{ csrf_token() }}"},
                success: function(data) {
                    $("#exampleModal .modal-body .contenido-modal").html('');
                    $("#exampleModal .modal-body .contenido-modal").html(data.options);
                }
            });
        });
        $("#identificacion").keypress(function(){
            id = $("#identificacion").val();
            buscar_personas(id);
        });
        $("#identificacion").blur(function(){
            id = $("#identificacion").val();
            
            buscar_personas(id);
        });

    });
    


    function buscar_personas(id){
        $.ajax({
            url: "{{ route('personas.filtrar_tabla') }}",
            method: 'POST',
            data: {'id':id,"_token": "{{ csrf_token() }}"},
            beforeSend: function() {
                //alert("buscando");
            },
            success: function(data) {
                $("#exampleModal .modal-body .contenido-modal").html('');
                $("#exampleModal .modal-body .contenido-modal").html(data.options);
            }
        });
    }

    function buscar_persona(id){

        $.ajax({
            url: "{{ route('personas.buscar') }}",
            method: 'POST',
            data: {'id':id,"_token": "{{ csrf_token() }}"},
            beforeSend: function() {
            },
            success: function(data) {
                $("#persona_id").val(data.id_persona);
                $("#cedula").val(id);
                $("#nombre_persona").val(data.nombres);
                $("#apellido_persona").val(data.apellidos);
                $("#cargo_persona").val(data.cargo);
                $("#correo").val(data.correo);
                $("#celular").val(data.celular);
            }
        });
    } 
    function agregar_persona(cedula,event){
        event.preventDefault();
        buscar_persona(cedula);
        $("#exampleModal").modal("toggle");

    }


</script>
@endsection













