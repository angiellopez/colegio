@extends('layout_admin')

@section('content')





<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                </div>
                <div class="card-body card-block">



                <div class = "table-responsive">
                    <table class="table table-show table-sinbordes table-list table-striped table-bordered">
                            <tr>
                                <td class = "font-bold">CICLO</td>
                                <td>{{$matricula->ciclo->nombre}} :: {{$matricula->ciclo->anio}}</td>
                            </tr>
                            <tr>
                                <td class = "font-bold">ESTUDIANTE</td>
                                <td>{{$matricula->estudiante->apellidos}} {{$matricula->estudiante->nombres}}</td>
                            </tr>
                            <tr>
                                <td class = "font-bold">FECHA MATRICULA</td>
                                <td>{{$matricula->fecha_inicio}}</td>
                            </tr>
                            <tr>
                                <td class = "font-bold">FECHA REGISTRO</td>
                                <td>{{$matricula->created_at}}</td>
                            </tr>
                    </table>
                </div>
                <br>
                <a href="{{ route('matriculas.index') }}"><button class="btn btn-success btn-sm" type = "button">Regresar</button></a>


                
                </div>
            </div>
        </div>
</div>    

@endsection

