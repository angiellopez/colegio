@extends('layout_admin')

@section('content')

<?php
        if(isset($asignacion)){
            $ciclo_id = $asignacion->ciclo_id;
            $materia_id = $asignacion->materia_id;
            $docente_id = $asignacion->docente_id;

        }else{
            $ciclo_id = "";
            $materia_id = "";
            $docente_id = "";

        }
    ?>






<div class = "row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>{{ $title }}</strong>
                </div>
                <div class="card-body card-block">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <h6>Para continuar debe corregir los siguientes errores:</h6>
                            <br>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                        </div>
                    @endif

                    <form method="POST" id = "formulario" action="{{ $accion }}" files="true" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ $metodo }}
                        <div class = "row">
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Ciclo</label>
                                    <select class="form-control" name="ciclo_id" id="ciclo_id" >
                                        <option value="" disabled selected>SELECCIONE</option>
                                        @foreach($ciclos as $ciclo)
                                        <option value="{{$ciclo->id}}" {{ old('ciclo_id', $ciclo_id) == $ciclo->id ? 'selected' : ''}}>{{$ciclo->nombre}} :: {{$ciclo->anio}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Materia</label>
                                    <select class="form-control" name="materia_id" id="materia_id" >
                                        <option value="" disabled selected>SELECCIONE</option>
                                        @foreach($materias as $materia)
                                        <option value="{{$materia->id}}" {{ old('materia_id', $materia_id) == $materia->id ? 'selected' : ''}}>{{$materia->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class = "col-md-4">
                                <div class="form-group">
                                    <label>Docente</label>
                                    <select class="form-control" name="docente_id" id="docente_id" >
                                        <option value="" disabled selected>SELECCIONE</option>
                                        @foreach($docentes as $docente)
                                        <option value="{{$docente->id}}" {{ old('docente_id', $docente_id) == $docente->id ? 'selected' : ''}}>{{$docente->apellidos}} {{$docente->nombres}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                        </div>

                        <div class = "row">
                            <div class = "col-md-6">
                                    <button type="button" id = "btn-agregar" class="btn btn-sm btn-warning" onclick = "agregar();">Agregar</button>
                            </div>
                        </div>
                        <br>


                        <div class="row">
                            <div class="col-md-12">
                            <div class="table-responsive">
                                <table id = "tabla_registro" class="table table-show table-list table-striped table-bordered">
                                <thead style = "font-weight:bold!important;">
                                    <tr>
                                        <th>CICLO</th>
                                        <th>MATERIA</th>
                                        <th>DOCENTE</th>
                                        <th>ELIMINAR</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    
                                </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                        <br>
                        
                        <span  style = "display:none;" id = "alert-busqueda">
                                Cargando...
                                <img style="width: 30px;" src="{{ asset('uploads/cargando.gif') }}" />
                            </span>

                            @if(session()->has('mensaje'))
                                <div class="alert alert-success">
                                    {{ session()->get('mensaje') }}
                                </div>
                            @endif
                            <div class="tile-footer">
                                <button type="submit" id = "btn-enviar" class="btn btn-sm btn-primary">{{$boton}}</button>
                                <a href="{{ route('asignaciones.index') }}"><button class="btn btn-sm btn-success" type = "button">Regresar</button></a>
                            </div>
                    </form>                                 



                </div>
            </div>
        </div>
</div>








<script type="text/javascript">
    jQuery(function($) {
        $("#formulario").submit(function(){
            $("#alert-busqueda").show();
        })

        $(document).on('click', '.btn-delete', function (event) {
            event.preventDefault();
            $(this).closest('tr').remove();
        });
    });

    function agregar(){
      ciclo = $("#ciclo_id").val();
      ciclo_texto = $("#ciclo_id option:selected").text();
      materia = $("#materia_id").val();
      materia_texto = $("#materia_id option:selected").text();
      docente = $("#docente_id").val();
      docente_texto = $("#docente_id option:selected").text();

      if(ciclo == null || materia == null || docente == null){
        alert("¡Todos los campos son obligatorios!");
      }else{
        cadena = "<tr>";
        cadena += "<td><input type='hidden' name='ciclo[]' value='"+ciclo+"' /><span>"+ciclo_texto+"</span></td>";
        cadena += "<td><input type='hidden' name='materia[]' value='"+materia+"' /><span>"+materia_texto+"</span></td>";
        cadena += "<td><input type='hidden' name='docente[]' value='"+docente+"' /><span>"+docente_texto+"</span></td>";
        cadena +=  "<td><button type='button' class = 'btn-delete' ><i class='fa fa-times-circle red bigger-24' aria-hidden='true'></i></button></td>";
        cadena += "</tr>";

        $("#tabla_registro tbody").append(cadena);

        $("#materia_id").val("");
        $("#docente_id").val("");
      }
    }

</script>
@endsection

