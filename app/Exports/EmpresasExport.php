<?php

namespace App\Exports;

use App\Persona;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmpresasExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    protected $empresas;

    public function __construct($empresas = null)
    {
        $this->empresas = $empresas;
    }

    public function collection()
    {
        foreach ($this->empresas as $index => $consul) {
            $datos = [
                $consul->id,
                $consul->nit,
                $consul->razon,
                $consul->regimen($consul->regimen_id),
                strtoupper($consul->documento($consul->documento_id)),
                $consul->identificacion,
                $consul->nombres,
                $consul->apellidos,
                strtoupper($consul->pais($consul->pais_id)),
                strtoupper($consul->departamento($consul->departamento_id)),
                strtoupper($consul->ciudad($consul->ciudad_id)),
                $consul->direccion,
                $consul->barrio,
                $consul->celular,
                $consul->correo,
                $consul->date_new,
                $consul->created_at,
            ];
            if($index == 0){
                $coleccion = collect([$datos]);
            }else{
                $coleccion->push($datos);
            }
        }
        return $coleccion;
    }

    public function headings(): array
    {
        return [
            'ID.', 
            'NIT',
            'Razón Social',
            'Régimen',
            'Tipo Doc. Identificación',
            'identificación', 
            'Nombres',
            'Apellidos',
            'Pais',
            'Departamento',
            'Ciudad',
            'Direccion',
            'Barrio',
            'Celular', 
            'Email', 
            'Fecha registro',
            'Detalle registro',           
        ];
    }

}
