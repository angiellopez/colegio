<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';
    public $remember_token=false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function getAuthPassword()
    {
        return $this->password;
    }
    public function empleado(){
        return $this->belongsTo(Persona::class,'empleado_id','id');
    }
    public function persona(){
        return $this->belongsTo(Persona::class,'persona_id','id');
    }
    public function nivel_nombre($id){
        $nombre = "";
        $consul = Rol::where('id',$id)->first();
        if($consul != null){
            $nombre = $consul->nombre;
        }
        return $nombre;
    }

    public function validar_permiso($permiso){
        $rol = Rol::where('id',Auth::user()->nivel)->first();
        if($rol != null){
            $permisos = $rol->permisos;
            $continuar = false;
    
            $porciones = explode(",", $permisos);
            if(in_array($permiso,$porciones)){
                $continuar = true;
            }else{
                $continuar = false;
            }
        }else{
            $continuar = false;
        }
        
        return $continuar;
    }

}
