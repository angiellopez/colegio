<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Asignacion;
use App\Persona;
use App\Ciclo;
use App\Materia;


use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class AsignacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    public function index()
    {

        if(Auth::user()->validar_permiso('con_cargos_list')){
            $asignaciones = Asignacion::paginate(20);
            $title = "Lista de Asignaciones";
            $controlador = "configuracion";
            return view('asignaciones.index', compact('title','asignaciones','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }

    public function create()
    {
        if(Auth::user()->validar_permiso('con_cargos_create')){
            
            $accion = url('asignaciones/guardar');
            $metodo = method_field('POST');
            $title = "Crear Asignación";
            $boton = "Crear";
            $regresar = $regresar = route('asignaciones.index');
            $controlador = "configuracion";

            $docentes = persona::where('tipo_persona','1')->orderBy('apellidos','asc')->get();
            $materias = Materia::orderBy('nombre','asc')->get();
            $ciclos = Ciclo::orderBy('nombre','asc')->get();

            return view('asignaciones.create',compact('docentes','materias','ciclos','accion','metodo','title','boton','regresar','controlador'));
            
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function edit($id)
    {
        if(Auth::user()->validar_permiso('con_cargos_edit')){

            $asignacion = Asignacion::findOrFail($id);
            $accion = url("asignaciones/actualizar/{$asignacion->id}");
            $metodo = method_field('PUT');
            $title = "Actualizar Asignación";
            $boton = "Actualizar";         
            $controlador = "configuracion";

            return view('asignaciones.create',compact('accion','metodo','title','boton','asignacion','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function store(Request $r)
    {
        $this->validate($r,[
            'ciclo'=>'required',
        ],[
            'ciclo.required'=>"El campo Ciclo es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $ciclo_array = $r->ciclo;
        $materia_array = $r->materia;
        $docente_array = $r->docente;

        if($ciclo_array != null){
            $n = count($ciclo_array);
            for ($i = 0; $i < $n; $i++ ) {

                $asignacion = new Asignacion;
                $asignacion->ciclo_id = $ciclo_array[$i];
                $asignacion->materia_id = $materia_array[$i];
                $asignacion->docente_id = $docente_array[$i];
           
                $asignacion->date_new = $dateonly;
                $asignacion->created_at = $datehour;
                $asignacion->updated_at = $datehour;
                $asignacion->user_new = Auth::id();
                $asignacion->user_update = Auth::id();

                $asignacion->save();
            }
        }

        return redirect()->route('asignaciones.index')->with('mensaje', 'Registro ingresado con éxito!');
    }    
    public function update(Request $r, $id)
    {
        $asignacion = Asignacion::findOrfail($id);
        $this->validate($r,[
            'nombre'=>'required',
        ],[
            'nombre.required'=>"El campo Titulo es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $asignacion->nombre = $r->nombre;

        $asignacion->date_new = $asignacion->date_new;
        $asignacion->created_at = $asignacion->created_at;
        $asignacion->updated_at = $datehour;
        $asignacion->user_new = $asignacion->user_new;
        $asignacion->user_update = Auth::id();

        $asignacion->save();

        return back()->with('mensaje', 'Actualización realizada con éxito!');
    }


    public function show($id)
    {
        if(Auth::user()->validar_permiso('con_cargos_show')){
            $asignacion = Asignacion::findOrFail($id);
            $title = 'Detalle de Asignación';
            $controlador = "configuracion";
            return view('asignaciones.show',compact('title','asignacion','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
      
    }

    public function destroy($id)
    {   
        if(Auth::user()->validar_permiso('con_cargos_delete')){
            $asignacion = Asignacion::findOrFail($id);
            try {
                $asignacion->delete($id);
                return back()->with('mensaje', 'Registro eliminado con éxito!');
            } 
            catch(QueryException $e) {
                return back()->with('alerta', 'No se pudo eliminar el registro!');
            } 
        }else{
            return view('errors.access_denied_admin');
        }
    }
}