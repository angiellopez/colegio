<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Configuracion;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class ConfiguracionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    public function index()
    {
        if(Auth::user()->validar_permiso('con_access')){
            $title = "Configuración";
            $controlador = "configuracion";
            return view('configuracion.index', compact('title','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }

    public function empresa()
    {
        if(Auth::user()->validar_permiso('con_empre')){
            
            $empresa = Configuracion::where('tipo','EMPRESA')->first();
            $controlador = "configuracion";

            if($empresa == null){
                $accion = url('configuracion/guardar_empresa');
                $metodo = method_field('POST');
                $title = "Crear Empresa";
                $boton = "Crear";
                $regresar = route('configuracion.index');
    
                return view('configuracion.empresa',compact('accion','metodo','title','boton','regresar','controlador'));
            }else{
                $accion = url("configuracion/guardar_empresa");
                $metodo = method_field('POST');
                $title = "Actualizar Empresa";
                $boton = "Actualizar";       
                $regresar = route('configuracion.index');    
                return view('configuracion.empresa',compact('accion','metodo','title','boton','empresa','regresar','controlador'));
            }
        }else{
            return view('errors.access_denied_admin');
        }
    }

    

    public function guardar_empresa(Request $r)
    {
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);
        $imagen_requerida = "";

        $empresa = Configuracion::where('tipo','EMPRESA')->first();
        if($empresa == null){
            $configuracion = new Configuracion;
            $configuracion->date_new = $dateonly;
            $configuracion->created_at = $datehour;
            $configuracion->user_new = Auth::id();
            $imagen_requerida = "required";
        }else{
            $configuracion = $empresa;
        }

        $this->validate($r,[
            'titulo'=>'required',
            'subtitulo'=>'required',
            'descripcion'=>'required',
            'nomenlace'=>'',
            'linkenlace'=>'',
            'nomimagen'=>'',
            'urlimagen'=>$imagen_requerida,
        ],[
            'titulo.required'=>"El campo NIT es requerido",
            'subtitulo.required'=>"El campo Razon social es requerido",
            'descripcion.required'=>"El campo Dirección es requerido",
            'urlimagen.required'=>"El campo Imagen es requerido",
        ]);


        $configuracion->titulo = $r->titulo;
        $configuracion->subtitulo = $r->subtitulo;
        $configuracion->descripcion = $r->descripcion;
        $configuracion->nomenlace = $r->nomenlace;
        $configuracion->linkenlace = $r->linkenlace;
        $configuracion->nomimagen = $r->nomimagen;
        $configuracion->nomarchivo = "";
        $configuracion->tipo = "EMPRESA";
        $configuracion->updated_at = $datehour;
        $configuracion->user_update = Auth::id();

        if($r->file('urlimagen') != null){
            if($configuracion->urlimagen != ""){
                Storage::disk('local')->delete($configuracion->urlimagen);
            }
            $configuracion->urlimagen = $r->file('urlimagen')->store('','local');
        }else{
           if($configuracion->urlimagen == null){
                $configuracion->urlimagen = "";
           } 
        }
        if($r->file('urlarchivo') != null){
            if($configuracion->urlarchivo != ""){
                Storage::disk('local')->delete($configuracion->urlarchivo);
            }
            $configuracion->urlarchivo = $r->file('urlarchivo')->store('','local');
        }else{
            if($configuracion->urlarchivo == null){
                 $configuracion->urlarchivo = "";
            } 
         }
        $configuracion->save();

        return redirect()->route('configuracion.empresa')->with('mensaje', 'Registro ingresado con éxito!');
    } 

}



