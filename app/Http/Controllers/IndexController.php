<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Slider;
use App\Institucion;
use App\Oferta;
use App\Area;
use App\Servicio;
use App\Noticia;
use App\Coordinador_imagen;

use App\Documento;
use App\Pais;
use App\Departamento;
use App\Ciudad;
use App\Ocupacion;
use App\Profesion;
use App\Regimen;

use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class IndexController extends Controller
{
    public function index()
    {
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $sliders = Slider::where('estado','1')->orderBy('id', 'desc')->take(5)->get();
        $publicaciones = Noticia::where('estado','1')->orderBy('id','desc')->take(3)->get();
       $barra = Institucion::where('tipo', 'barra')->first();
       $publicacion = Institucion::where('tipo', 'PUBLICACION')->first();
       $freelancer = Institucion::where('tipo', 'FREELANCER')->first();
       $area= Institucion::where('tipo', 'AREA')->first();
       $quienesomos=Institucion::where('tipo', 'quienesomos')->first();
       $contactanos=Institucion::where('tipo','contacto')->first();
        $redes = Institucion::where('tipo','redsocial')->first();
        $regempresa = Institucion::where('tipo','regempresa')->first();
        $nosotros_imagenes = Coordinador_imagen::where('tipo_imagen','nosotros')->get();


        $oferta = Oferta::first();
        $areas = Area::orderBy('titulo','asc')->get();
        
        $controlador = "inicio";
        $pie=Institucion::where('tipo','PIE')->first();

        return view('index.index',compact('nosotros_imagenes','pie','regempresa','redes','contactanos','quienesomos','area','freelancer','publicacion','barra','publicaciones','controlador','oferta','areas','sliders'));
    }
    public function nosotros()
    {
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $sliders = Slider::where('estado','1')->orderBy('id', 'desc')->take(3)->get();
        $quienesomos=Institucion::where('tipo', 'quienesomos')->first();
        $contactanos=Institucion::where('tipo','contacto')->first();
        $redes = Institucion::where('tipo','redsocial')->first();
        $mision=Institucion::where('tipo', 'MISION')->first(); 
        $vision=Institucion::where('tipo', 'vision')->first(); 
        $objetivos =Institucion::where('tipo', 'objetivo')->first(); 
        $politicas =Institucion::where('tipo', 'politica')->first(); 
        $empresa =Institucion::where('tipo', 'empresa')->first(); 
        $portafolio = Institucion::where('tipo', 'PORTAFOLIO')->first();

        
        $controlador = "nosotros";
        $pie=Institucion::where('tipo','PIE')->first();

        return view('index.nosotros',compact('empresa','pie','portafolio','politicas','objetivos','vision','mision','quienesomos','redes','contactanos','controlador','sliders'));
    }
    public function empresarial()
    {
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $sliders = Slider::where('estado','1')->orderBy('id', 'desc')->take(3)->get();
        $contactanos=Institucion::where('tipo','contacto')->first();
        $redes = Institucion::where('tipo','redsocial')->first();
        $regimenes = Regimen::all(); 
        $documentos = Documento::all();
        $paises = Pais::orderBy('titulo','asc')->get();
        $departamentos = Departamento::take(0)->get();
        $ciudades = Ciudad::take(0)->get();
        $terminos = Institucion::where('tipo','TERMINOS')->first();
        $txtempresa = Institucion::where('tipo','txtempresa')->first();
        $empresarial = Institucion::where('tipo','regempresa')->first();

        
        $controlador = "inicio";
        $pie=Institucion::where('tipo','PIE')->first();
        



        return view('index.empresarial',compact('pie','empresarial','txtempresa','terminos','redes','contactanos','controlador','regimenes','paises','documentos','departamentos','ciudades','sliders'));
    }
    public function areas()
    {
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $sliders = Slider::where('estado','1')->orderBy('id', 'desc')->take(3)->get();
        $contactanos=Institucion::where('tipo','contacto')->first();
        $redes = Institucion::where('tipo','redsocial')->first();
        $areas = Area::orderBy('titulo','asc')->get();
        $infarea = Institucion::where('tipo','AREA')->first();     
        $txtarea = Institucion::where('tipo','txtareas')->first();

  
        
        $controlador = "areas";
        $pie=Institucion::where('tipo','PIE')->first();



        return view('index.areas',compact('pie','txtarea','infarea','redes','contactanos','controlador','areas','sliders'));
    }
    public function freelancer()
    {
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $sliders = Slider::where('estado','1')->orderBy('id', 'desc')->take(3)->get();
        $contactanos=Institucion::where('tipo','contacto')->first();
        $redes = Institucion::where('tipo','redsocial')->first();
        $terminos = Institucion::where('tipo','TERMINOS')->first();
        $txtfreelancer = Institucion::where('tipo','txtfreelancer')->first();
        $freelancer = Institucion::where('tipo', 'FREELANCER')->first();

        //$noticias=Noticia::orderBy('created_at','desc')->take(4)->get();
        
        $controlador = "inicio";

        $documentos = Documento::all();
        $paises = Pais::orderBy('titulo','asc')->get();
        $departamentos = Departamento::take(0)->get();
        $ciudades = Ciudad::take(0)->get();
        $areas = Area::orderBy('titulo','asc')->get();
        $ocupaciones = Ocupacion::all();
        $profesiones = Profesion::all();
        $pie=Institucion::where('tipo','PIE')->first();



        return view('index.freelancer',compact('pie','freelancer','txtfreelancer','terminos','redes','contactanos','documentos','paises','departamentos','ciudades','areas','ocupaciones','profesiones','controlador','sliders'));
    }
    public function contacto()
    {
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);
        $redes = Institucion::where('tipo','redsocial')->first();
        $contactanos=Institucion::where('tipo','contacto')->first();
        $terminos = Institucion::where('tipo','TERMINOS')->first();
        $txtcontactanos = Institucion::where('tipo','txtcontactanos')->first();

        $sliders = Slider::where('estado','1')->orderBy('id', 'desc')->take(3)->get();  
        
        $controlador = "contacto";
        $pie=Institucion::where('tipo','PIE')->first();



        return view('index.contacto',compact('pie','txtcontactanos','terminos','redes','contactanos','controlador','sliders'));
    }

    public function ofertas()
    {
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $sliders = Slider::where('estado','1')->orderBy('id', 'desc')->take(3)->get();
        $contactanos=Institucion::where('tipo','contacto')->first();
        $redes = Institucion::where('tipo','redsocial')->first();
        $ofertas = Oferta::orderBy('created_at','desc')->get();
        $areas = Area::orderBy('titulo','asc')->get();
        $infoferta = Institucion::where('tipo','OFERTA')->first();
        $txtoferta = Institucion::where('tipo','txtofertas')->first();
        
        $controlador = "ofertas";
        $pie=Institucion::where('tipo','PIE')->first();



        return view('index.ofertas',compact('pie','txtoferta','infoferta','redes','contactanos','controlador','ofertas','areas','sliders'));
    }
    public function servicios($area_id)
    {
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $area = Area::where('id',$area_id)->firstOrFail();
        $servicios = Servicio::where('area_id',$area->id)->get();

        $sliders = Slider::where('estado','1')->orderBy('id', 'desc')->take(3)->get();
        $contactanos=Institucion::where('tipo','contacto')->first();       
        $redes = Institucion::where('tipo','redsocial')->first();
        $areas = Area::orderBy('titulo','asc')->get();
        $infservicios = Institucion::where('tipo','SERVICIO')->first();
        $cotizacion = Institucion::where('tipo','cotizacion')->first();

        //$noticias=Noticia::orderBy('created_at','desc')->take(4)->get();
        
        $controlador = "areas";
        $pie=Institucion::where('tipo','PIE')->first();



        return view('index.servicios',compact('cotizacion','pie','area','servicios','infservicios','redes','contactanos','controlador','areas','sliders'));
    }
    public function coordinadores()
    {
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $sliders = Slider::where('estado','1')->orderBy('id', 'desc')->take(3)->get();
        $contactanos=Institucion::where('tipo','contacto')->first();
        $redes = Institucion::where('tipo','redsocial')->first();
        $oferta = Oferta::first();
        $areas = Area::orderBy('titulo','asc')->get();
        $infocoordinadores  = Institucion::where('tipo','coordinadores')->first();
        $imagenes = Coordinador_imagen::where('tipo_imagen','coordi')->get();
        
        $controlador = "coordinadores";
        $pie=Institucion::where('tipo','PIE')->first();



        return view('index.coordinadores',compact('imagenes','pie','infocoordinadores','redes','contactanos','controlador','oferta','areas','sliders'));
    }
    public function publicaciones()
    {
        $variable=request('publicacion_id');        
        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $sliders = Slider::where('estado','1')->orderBy('id', 'desc')->take(3)->get();
        $contactanos=Institucion::where('tipo','contacto')->first();
        $redes = Institucion::where('tipo','redsocial')->first();
        $noticia = Noticia::where('id', $variable)->first();

        $controlador = "publicaciones";
        $pie=Institucion::where('tipo','PIE')->first();

        return view('index.publicaciones',compact('pie','noticia','redes','contactanos','controlador','sliders'));
    }

    public function filtro(Request $r){
        $id=$ir->id;
        $filofertas = Oferta::where('titulo', 'LIKE', "%$id%")
        ->orWhere('descripcion', 'LIKE', "%$id%")
        ->orWhere('requisitos', 'LIKE', "%$id%")
        ->orWhere('salario', 'LIKE', "%$id%")->get();
        $ofer = view('cargar_tabla', compact('filofertas'))->render();
        return response()->json([]);
            
    }

    public function cargar_servicio(Request $r){
        $servicio = Servicio::where('id',$r->id)->first();
        //$imagen = asset('uploads')."/".$servicio->urlimagen;
        //$textoimagen = "<img src = '{$imagen}' class = 'img-responsive' style = 'width:50%;'>";
    	return response()->json(['titulo'=>$servicio->titulo,'descripcion'=>$servicio->descripcion]);
    }

    public function cargar_oferta(Request $r){
        $oferta = Oferta::where('id',$r->id)->first();
        //$imagen = asset('uploads')."/".$servicio->urlimagen;
        //$textoimagen = "<img src = '{$imagen}' class = 'img-responsive' style = 'width:50%;'>";
    	return response()->json(['titulo'=>$oferta->titulo,'descripcion'=>$oferta->descripcion,'requisitos'=>$oferta->requisitos]);
    }

    public function buscar_ofertas(Request $r){
        $id = $r->id;
        $ofertas = Oferta::where('titulo', 'LIKE', "%$id%")
        ->orWhere('descripcion', 'LIKE', "%$id%")
        ->orWhere('requisitos', 'LIKE', "%$id%")
        ->orWhere('salario', 'LIKE', "%$id%")
        ->take(10)->get();
        $opciones = view('cargar_tabla',compact('ofertas'))->render();
    	return response()->json(['options'=>$opciones]);
    }

}



