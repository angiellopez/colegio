<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Matricula;
use App\Persona;
use App\Ciclo;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class MatriculaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    public function index()
    {

        if(Auth::user()->validar_permiso('con_cargos_list')){
            $matriculas = Matricula::paginate(20);
            $title = "Lista de Matriculas";
            $controlador = "matriculas";
            return view('matriculas.index', compact('title','matriculas','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }

    public function create()
    {
        if(Auth::user()->validar_permiso('con_cargos_create')){
            
            $accion = url('matriculas/guardar');
            $metodo = method_field('POST');
            $title = "Crear Matricula";
            $boton = "Crear";
            $regresar = route('matriculas.index');
            $controlador = "matriculas";
            $ciclos = Ciclo::orderBy('nombre','asc')->get();

            return view('matriculas.create',compact('ciclos','accion','metodo','title','boton','regresar','controlador'));
            
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function edit($id)
    {
        if(Auth::user()->validar_permiso('con_cargos_edit')){

            $matricula = Matricula::findOrFail($id);
            $persona = Persona::where('id',$matricula->estudiante_id)->first();
            $accion = url("matriculas/actualizar/{$matricula->id}");
            $metodo = method_field('PUT');
            $title = "Actualizar Matricula";
            $boton = "Actualizar";         
            $controlador = "matriculas";
            $ciclos = Ciclo::orderBy('nombre','asc')->get();

            return view('matriculas.create',compact('persona','ciclos','accion','metodo','title','boton','matricula','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function store(Request $r)
    {
        $this->validate($r,[
            'persona_id'=>'required',
        ],[
            'persona_id.required'=>"El campo Estudiante es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $matricula = new Matricula;
        $matricula->estudiante_id = $r->persona_id;
        $matricula->ciclo_id = $r->ciclo_id;
        $matricula->fecha_inicio = $r->fecha_inicio;



        $matricula->date_new = $dateonly;
        $matricula->created_at = $datehour;
        $matricula->updated_at = $datehour;
        $matricula->user_new = Auth::id();
        $matricula->user_update = Auth::id();

        $matricula->save();

        return redirect()->route('matriculas.index')->with('mensaje', 'Registro ingresado con éxito!');
    }    
    public function update(Request $r, $id)
    {
        $matricula = Matricula::findOrfail($id);
        $this->validate($r,[
            'persona_id'=>'required',
        ],[
            'persona_id.required'=>"El campo Persona es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $matricula->ciclo_id = $r->ciclo_id;
        $matricula->fecha_inicio = $r->fecha_inicio;

        $matricula->updated_at = $datehour;
        $matricula->user_update = Auth::id();

        $matricula->save();

        return back()->with('mensaje', 'Actualización realizada con éxito!');
    }


    public function show($id)
    {
        if(Auth::user()->validar_permiso('con_cargos_show')){
            $matricula = Matricula::findOrFail($id);
            $title = 'Detalle de Matricula';
            $controlador = "matriculas";
            return view('matriculas.show',compact('title','matricula','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
      
    }

    public function destroy($id)
    {   
        if(Auth::user()->validar_permiso('con_cargos_delete')){
            $matricula = Matricula::findOrFail($id);
            try {
                $matricula->delete($id);
                return back()->with('mensaje', 'Registro eliminado con éxito!');
            } 
            catch(QueryException $e) {
                return back()->with('alerta', 'No se pudo eliminar el registro!');
            } 
        }else{
            return view('errors.access_denied_admin');
        }
    }
}