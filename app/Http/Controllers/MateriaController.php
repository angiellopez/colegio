<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Materia;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class MateriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    public function index()
    {

        if(Auth::user()->validar_permiso('con_cargos_list')){
            $materias = Materia::paginate(20);
            $title = "Lista de Materias";
            $controlador = "configuracion";
            return view('materias.index', compact('title','materias','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }

    public function create()
    {
        if(Auth::user()->validar_permiso('con_cargos_create')){
            
            $accion = url('materias/guardar');
            $metodo = method_field('POST');
            $title = "Crear Materia";
            $boton = "Crear";
            $regresar = $regresar = route('materias.index');
            $controlador = "configuracion";

            return view('materias.create',compact('accion','metodo','title','boton','regresar','controlador'));
            
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function edit($id)
    {
        if(Auth::user()->validar_permiso('con_cargos_edit')){

            $materia = Materia::findOrFail($id);
            $accion = url("materias/actualizar/{$materia->id}");
            $metodo = method_field('PUT');
            $title = "Actualizar Materia";
            $boton = "Actualizar";         
            $controlador = "configuracion";

            return view('materias.create',compact('accion','metodo','title','boton','materia','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function store(Request $r)
    {
        $this->validate($r,[
            'nombre'=>'required',
        ],[
            'nombre.required'=>"El campo Titulo es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $materia = new Materia;
        $materia->nombre = $r->nombre;

        $materia->date_new = $dateonly;
        $materia->created_at = $datehour;
        $materia->updated_at = $datehour;
        $materia->user_new = Auth::id();
        $materia->user_update = Auth::id();

        $materia->save();

        return redirect()->route('materias.index')->with('mensaje', 'Registro ingresado con éxito!');
    }    
    public function update(Request $r, $id)
    {
        $materia = Materia::findOrfail($id);
        $this->validate($r,[
            'nombre'=>'required',
        ],[
            'nombre.required'=>"El campo Titulo es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $materia->nombre = $r->nombre;

        $materia->date_new = $materia->date_new;
        $materia->created_at = $materia->created_at;
        $materia->updated_at = $datehour;
        $materia->user_new = $materia->user_new;
        $materia->user_update = Auth::id();

        $materia->save();

        return back()->with('mensaje', 'Actualización realizada con éxito!');
    }


    public function show($id)
    {
        if(Auth::user()->validar_permiso('con_cargos_show')){
            $materia = Materia::findOrFail($id);
            $title = 'Detalle de Materia';
            $controlador = "configuracion";
            return view('materias.show',compact('title','materia','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
      
    }

    public function destroy($id)
    {   
        if(Auth::user()->validar_permiso('con_cargos_delete')){
            $materia = Materia::findOrFail($id);
            try {
                $materia->delete($id);
                return back()->with('mensaje', 'Registro eliminado con éxito!');
            } 
            catch(QueryException $e) {
                return back()->with('alerta', 'No se pudo eliminar el registro!');
            } 
        }else{
            return view('errors.access_denied_admin');
        }
    }
}