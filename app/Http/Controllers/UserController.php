<?php

namespace App\Http\Controllers;

use App\User;
use App\Persona;
use App\Rol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Database\QueryException;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
            

            if(Auth::user()->validar_permiso('adm_usuarios_list')){
                $users = User::orderBy('id','desc')->get();
                $title = "Lista de Usuarios";
                $controlador = "configuracion";
                $regresar = route('users.index');
                return view('users.index', compact('title','users','controlador'));
            }else{
                return view('errors.access_denied_admin_admin');
            }



    }

    public function create()
    {
            
            if(Auth::user()->validar_permiso('adm_usuarios_create')){
            
                $accion = url('users/guardar');
                $metodo = method_field('POST');
                $title = "Crear Usuario";
                $boton = "Crear";
                $regresar = $regresar = route('users.index');
                $controlador = "configuracion";
                $roles = Rol::orderBy('nombre','asc')->get();
    
                return view('users.create',compact('roles','accion','metodo','title','boton','regresar','controlador'));
                
            }else{
                return view('errors.access_denied_admin');
            }



    }
    public function edit($id)
    {
            

            if(Auth::user()->validar_permiso('adm_usuarios_edit')){

                $user = User::findOrFail($id);
                $persona = Persona::where('id',$user->persona_id)->firstOrFail();
                $accion = url("users/actualizar/{$user->id}");
                $metodo = method_field('PUT');
                $title = "Actualizar Usuario";
                $boton = "Actualizar";    
                $controlador = "configuracion";       
                $roles = Rol::orderBy('nombre','asc')->get();
                return view('users.create',compact('roles','persona','accion','metodo','title','boton','user','controlador'));
            }else{
                return view('errors.access_denied_admin');
            }


    }
    public function store(Request $r)
    {
            


            $this->validate($r,[
                'persona_id'=>'required',
                'user'=>'required',
                'password'=>'required',
                'user'=>Rule::unique('users')->where(function ($query) {
                    $query->where('user','!=','');
                }),
                'email' => Rule::unique('users')->where(function ($query) {
                    $query->where('email','!=','');
                }),
            ],[
                'persona_id.required'=>"El campo Persona es requerido",
                'user.required'=>"El campo Usuario es requerido",
                'password.required'=>"El campo Contraseña es requerido",
                'user.unique'=>"El Usuario ingresado ya está en uso",
                'email.unique'=>"El correo electrónico debe estar disponible",
            ]);
    
            date_default_timezone_set('America/Bogota');
            $fechahora=time();
            $dateonly=date("Y-m-d", $fechahora);
            $datehour= date("Y-m-d H:i:s", $fechahora);
    
            $user = new User;
            $user->persona_id = $r->persona_id;
            $user->user = $r->user;
            $user->password = bcrypt($r->password);
            $user->estado = "1";
            $user->nivel = $r->rol_id;
            $user->date_new = $dateonly;
            $user->created_at = $datehour;
            $user->updated_at = $datehour;
            $user->user_new = Auth::id();
            $user->user_update = Auth::id();
            $user->save();
    
            return redirect()->route('users.index')->with('mensaje', 'Registro ingresado con éxito!');




    }    
    public function update(Request $r, $id)
    {
            

            $user = User::findOrfail($id);
            $this->validate($r,[
                'persona_id'=>'required',
                'user'=>'required',
                'password'=>'',
                'user'=>Rule::unique('users')->where(function ($query) {
                    $query->where('user','!=','');
                })->ignore($user->id),
            ],[
                'persona_id.required'=>"El campo Empleado es requerido",
                'user.unique'=>"El Usuario ingresado ya está en uso",
            ]);
    
            date_default_timezone_set('America/Bogota');
            $fechahora=time();
            $dateonly=date("Y-m-d", $fechahora);
            $datehour= date("Y-m-d H:i:s", $fechahora);
    
            if($r->password != ""){
                $user->password = bcrypt($r->password);
            }
            $user->estado = "1";
            $user->nivel = $user->nivel;
            $user->updated_at = $datehour;
            $user->user_update = Auth::id();
            $user->save();
    
            return back()->with('mensaje', 'Actualización realizada con éxito!');


       
    }


    public function show($id)
    {
            


            if(Auth::user()->validar_permiso('adm_usuarios_show')){
                $user = User::findOrFail($id);
                $title = 'Detalle de Usuario';
                $controlador = "configuracion";
                return view('users.show',compact('title','user','controlador'));
            }else{
                return view('errors.access_denied_admin');
            }


        
      
    }

    public function destroy($id)
    {   
            

            if(Auth::user()->validar_permiso('adm_usuarios_delete')){
                $user = User::findOrFail($id);
                try {
                    $user->delete($id);
                    return back()->with('mensaje', 'Registro eliminado con éxito!');
                } 
                catch(QueryException $e) {
                    return back()->with('alerta', 'No se pudo eliminar el registro!');
                } 
            }else{
                return view('errors.access_denied_admin');
            }



        
    }
    public function perfil()
    {
            
            $user = User::findOrFail(Auth::id());
            $persona = Persona::where('id',$user->persona_id)->firstOrFail();
            $accion = url("users/actualizar/{$user->id}");
            $metodo = method_field('PUT');
            $title = "Mi perfil";
            $boton = "Actualizar";
            $perfil = true;
    
            $user = User::findOrFail(Auth::id());
            $controlador = "institucion";
    
            return view('users.create',compact('persona','accion','metodo','title','boton','user','controlador','perfil'));

 

        
    }



}