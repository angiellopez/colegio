<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cargo;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class CargoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    public function index()
    {

        if(Auth::user()->validar_permiso('con_cargos_list')){
            $cargos = Cargo::paginate(20);
            $title = "Lista de Cargos";
            $controlador = "configuracion";
            return view('cargos.index', compact('title','cargos','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }

    public function create()
    {
        if(Auth::user()->validar_permiso('con_cargos_create')){
            
            $accion = url('cargos/guardar');
            $metodo = method_field('POST');
            $title = "Crear Cargo";
            $boton = "Crear";
            $regresar = $regresar = route('cargos.index');
            $controlador = "configuracion";

            return view('cargos.create',compact('accion','metodo','title','boton','regresar','controlador'));
            
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function edit($id)
    {
        if(Auth::user()->validar_permiso('con_cargos_edit')){

            $cargo = Cargo::findOrFail($id);
            $accion = url("cargos/actualizar/{$cargo->id}");
            $metodo = method_field('PUT');
            $title = "Actualizar Cargo";
            $boton = "Actualizar";         
            $controlador = "configuracion";

            return view('cargos.create',compact('accion','metodo','title','boton','cargo','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function store(Request $r)
    {
        $this->validate($r,[
            'nombre'=>'required',
        ],[
            'nombre.required'=>"El campo Titulo es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $cargo = new Cargo;
        $cargo->nombre = $r->nombre;

        $cargo->date_new = $dateonly;
        $cargo->created_at = $datehour;
        $cargo->updated_at = $datehour;
        $cargo->user_new = Auth::id();
        $cargo->user_update = Auth::id();

        $cargo->save();

        return redirect()->route('cargos.index')->with('mensaje', 'Registro ingresado con éxito!');
    }    
    public function update(Request $r, $id)
    {
        $cargo = Cargo::findOrfail($id);
        $this->validate($r,[
            'nombre'=>'required',
        ],[
            'nombre.required'=>"El campo Titulo es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $cargo->nombre = $r->nombre;

        $cargo->date_new = $cargo->date_new;
        $cargo->created_at = $cargo->created_at;
        $cargo->updated_at = $datehour;
        $cargo->user_new = $cargo->user_new;
        $cargo->user_update = Auth::id();

        $cargo->save();

        return back()->with('mensaje', 'Actualización realizada con éxito!');
    }


    public function show($id)
    {
        if(Auth::user()->validar_permiso('con_cargos_show')){
            $cargo = Cargo::findOrFail($id);
            $title = 'Detalle de Cargo';
            $controlador = "configuracion";
            return view('cargos.show',compact('title','cargo','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
      
    }

    public function destroy($id)
    {   
        if(Auth::user()->validar_permiso('con_cargos_delete')){
            $cargo = Cargo::findOrFail($id);
            try {
                $cargo->delete($id);
                return back()->with('mensaje', 'Registro eliminado con éxito!');
            } 
            catch(QueryException $e) {
                return back()->with('alerta', 'No se pudo eliminar el registro!');
            } 
        }else{
            return view('errors.access_denied_admin');
        }
    }
}