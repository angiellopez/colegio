<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Ciclo;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class CicloController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    public function index()
    {

        if(Auth::user()->validar_permiso('con_cargos_list')){
            $ciclos = Ciclo::paginate(20);
            $title = "Lista de Ciclos";
            $controlador = "configuracion";
            return view('ciclos.index', compact('title','ciclos','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }

    public function create()
    {
        if(Auth::user()->validar_permiso('con_cargos_create')){
            
            $accion = url('ciclos/guardar');
            $metodo = method_field('POST');
            $title = "Crear Ciclo";
            $boton = "Crear";
            $regresar = $regresar = route('ciclos.index');
            $controlador = "configuracion";

            return view('ciclos.create',compact('accion','metodo','title','boton','regresar','controlador'));
            
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function edit($id)
    {
        if(Auth::user()->validar_permiso('con_cargos_edit')){

            $ciclo = Ciclo::findOrFail($id);
            $accion = url("ciclos/actualizar/{$ciclo->id}");
            $metodo = method_field('PUT');
            $title = "Actualizar Ciclo";
            $boton = "Actualizar";         
            $controlador = "configuracion";

            return view('ciclos.create',compact('accion','metodo','title','boton','ciclo','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function store(Request $r)
    {
        $this->validate($r,[
            'nombre'=>'required',
            'fecha_inicio'=>'required',
            'fecha_fin'=>'required',
            'anio'=>'required',
        ],[
            'nombre.required'=>"El campo Nombre es requerido",
            'fecha_inicio.required'=>"El campo Fecha Inicio es requerido",
            'fecha_fin.required'=>"El campo Fecha Fin es requerido",
            'anio.required'=>"El campo Año es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $ciclo = new Ciclo;
        $ciclo->nombre = $r->nombre;
        $ciclo->fecha_inicio = $r->fecha_inicio;
        $ciclo->fecha_fin = $r->fecha_fin;
        $ciclo->anio = $r->anio;

        $ciclo->date_new = $dateonly;
        $ciclo->created_at = $datehour;
        $ciclo->updated_at = $datehour;
        $ciclo->user_new = Auth::id();
        $ciclo->user_update = Auth::id();

        $ciclo->save();

        return redirect()->route('ciclos.index')->with('mensaje', 'Registro ingresado con éxito!');
    }    
    public function update(Request $r, $id)
    {
        $ciclo = Ciclo::findOrfail($id);
        $this->validate($r,[
            'nombre'=>'required',
        ],[
            'nombre.required'=>"El campo Titulo es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $ciclo->nombre = $r->nombre;

        $ciclo->date_new = $ciclo->date_new;
        $ciclo->created_at = $ciclo->created_at;
        $ciclo->updated_at = $datehour;
        $ciclo->user_new = $ciclo->user_new;
        $ciclo->user_update = Auth::id();

        $ciclo->save();

        return back()->with('mensaje', 'Actualización realizada con éxito!');
    }


    public function show($id)
    {
        if(Auth::user()->validar_permiso('con_cargos_show')){
            $ciclo = Ciclo::findOrFail($id);
            $title = 'Detalle de Ciclo';
            $controlador = "configuracion";
            return view('ciclos.show',compact('title','ciclo','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
      
    }

    public function destroy($id)
    {   
        if(Auth::user()->validar_permiso('con_cargos_delete')){
            $ciclo = Ciclo::findOrFail($id);
            try {
                $ciclo->delete($id);
                return back()->with('mensaje', 'Registro eliminado con éxito!');
            } 
            catch(QueryException $e) {
                return back()->with('alerta', 'No se pudo eliminar el registro!');
            } 
        }else{
            return view('errors.access_denied_admin');
        }
    }
}