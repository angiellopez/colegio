<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Persona;
use App\Documento;
use App\Vehiculo;
use App\Cargo;
use App\Departamento;
use App\Pais;
use App\Ciudad;
use App\Regimen;
use App\Area;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class Admin_empresaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    public function index()
    {
        if(Auth::user()->empleado->tipo_persona == '1'){
    
            if(Auth::user()->validar_permiso('adm_empre_list')){
                $admin_empresas = Persona::where('tipo_persona','2')->get();
                $titulo = "Lista de Empresas";
                $controlador = "admin_empresas";
                return view('admin_empresas.index', compact('titulo','admin_empresas','controlador'));
            }else{
                return view('errors.access_denied_admin');
            }

            
        }else{
            return view('errors.404');
        }
    }

    public function create()
    {
        if(Auth::user()->empleado->tipo_persona == '1'){
            


            if(Auth::user()->validar_permiso('adm_empre_create')){
            
                $accion = url('admin_empresas/guardar');
                $metodo = method_field('POST');
                $titulo = "Crear Empresa";
                $boton = "Crear Empresa";
                $regresar = $regresar = route('admin_empresas.index');
                $controlador = "admin_empresas";
                $documentos = Documento::all();
                $cargos = Cargo::all();
                $areas = Area::orderBy('titulo','asc')->get();
                $paises = Pais::orderBy('titulo','asc')->get();
                $departamentos = Departamento::take(0)->get();
                $ciudades = Ciudad::take(0)->get();
                $regimenes = Regimen::all(); 
                
                return view('admin_empresas.create',compact('areas','paises','regimenes','departamentos','ciudades','cargos','accion','metodo','titulo','boton','regresar','controlador','documentos'));
                
            }else{
                return view('errors.access_denied_admin');
            }

            
        }else{
            return view('errors.404');
        }
        
    }
    public function edit($id)
    {
        if(Auth::user()->empleado->tipo_persona == '1'){
            



            if(Auth::user()->validar_permiso('adm_empre_edit')){

                $admin_empresa = Persona::where('tipo_persona','2')->findOrFail($id);
                $accion = url("admin_empresas/actualizar/{$admin_empresa->id}");
                $metodo = method_field('PUT');
                $titulo = "Actualizar Empresa";
                $boton = "Actualizar";         
                $controlador = "admin_empresas";
                $documentos = Documento::all();
                $cargos = Cargo::all();
                $paises = Pais::orderBy('titulo','asc')->get();
                $areas = Area::orderBy('titulo','asc')->get();
                $departamentos = Departamento::where('pais_id',$admin_empresa->pais_id)->get();
                $ciudades = Ciudad::where('departamento_id',$admin_empresa->departamento_id)->get();
                $regimenes = Regimen::all();
    
                return view('admin_empresas.create',compact('areas','paises','regimenes','departamentos','ciudades','cargos','accion','metodo','titulo','boton','admin_empresa','controlador','documentos'));
            }else{
                return view('errors.access_denied_admin');
            }

            
        }else{
            return view('errors.404');
        }
    }
    public function store(Request $r)
    {
        if(Auth::user()->empleado->tipo_persona == '1'){
            


            $this->validate($r,[
                'nit'=>'required',
                'razon'=>'required',
                'regimen_id'=>'required',
                'documento_id'=>'required',
                'identificacion'=>['required','unique:personas,identificacion'],
                'nombres'=>'required',
                'apellidos'=>'required',
                'pais_id'=>'required',
                'departamento_id'=>'required',
                'ciudad_id'=>'required',
                'direccion'=>'required',
                'barrio'=>'required',
                'celular'=>'required',
                'correo'=>['required','unique:personas,correo'],
            ],[
                'nit.required'=>"El campo NIT es requerido",
                'razon.required'=>"El campo Razón Social es requerido",
                'regimen_id.required'=>"El campo Régimen es requerido",
                'documento_id.required'=>"El campo Documento es requerido",
                'identificacion.required'=>"El campo Identificación es requerido",
                'nombres.required'=>"El campo Nombre es requerido",
                'apellidos.required'=>"El campo Nombre es requerido",
                'pais_id.required'=>"El campo Pais es requerido",
                'departamento_id.required'=>"El campo Departamento es requerido",
                'ciudad_id.required'=>"El campo Ciudad es requerido",
                'direccion.required'=>"El campo Dirección es requerido",
                'barrio.required'=>"El campo Barrio es requerido",
                'celular.required'=>"El campo Celular es requerido",
                'correo.required'=>"El campo Correo es requerido",
                'identificacion.unique'=>"El campo Identificación ya existe",
                'correo.unique'=>"El campo Correo ya existe",
            ]);
    
            date_default_timezone_set('America/Bogota');
            $fechahora=time();
            $dateonly=date("Y-m-d", $fechahora);
            $datehour= date("Y-m-d H:i:s", $fechahora);
    
            $admin_empresa = new Persona;
            $admin_empresa->nit = $r->nit;
            $admin_empresa->razon = $r->razon;
            $admin_empresa->regimen_id = $r->regimen_id;
            $admin_empresa->documento_id = $r->documento_id;
            $admin_empresa->identificacion = $r->identificacion;
            $admin_empresa->nombres = $r->nombres;
            $admin_empresa->apellidos = $r->apellidos;
            $admin_empresa->pais_id = $r->pais_id;
            $admin_empresa->departamento_id = $r->departamento_id;
            $admin_empresa->ciudad_id = $r->ciudad_id;
            $admin_empresa->direccion = $r->direccion;
            $admin_empresa->barrio = $r->barrio;
            $admin_empresa->celular = $r->celular;
            $admin_empresa->correo = $r->correo;
            $admin_empresa->tipo_persona = "2";
    
            $admin_empresa->date_new = $dateonly;
            $admin_empresa->created_at = $datehour;
            $admin_empresa->updated_at = $datehour;
            $admin_empresa->user_new = Auth::id();
            $admin_empresa->user_update = Auth::id();
    
            $admin_empresa->save();
    
            return redirect()->route('admin_empresas.index')->with('mensaje_admin_empresa', 'Admin_empresa registrado con éxito!');
        


            
        }else{
            return view('errors.404');
        }}    
    public function update(Request $r, $id)
    {
        if(Auth::user()->empleado->tipo_persona == '1'){
            


            $admin_empresa = Persona::findOrfail($id);
            $this->validate($r,[
                'nit'=>'required',
                'razon'=>'required',
                'regimen_id'=>'required',
                'documento_id'=>'required',
                'identificacion'=>'required|unique:personas,identificacion,'.$admin_empresa->id,
                'nombres'=>'required',
                'apellidos'=>'required',
                'pais_id'=>'required',
                'departamento_id'=>'required',
                'ciudad_id'=>'required',
                'direccion'=>'required',
                'barrio'=>'required',
                'celular'=>'required',
                'correo'=> 'required|unique:personas,correo,'.$admin_empresa->id,
            ],[
                'nit.required'=>"El campo NIT es requerido",
                'razon.required'=>"El campo Razón Social es requerido",
                'regimen_id.required'=>"El campo Régimen es requerido",
                'documento_id.required'=>"El campo Documento es requerido",
                'identificacion.required'=>"El campo Identificación es requerido",
                'nombres.required'=>"El campo Nombres es requerido",
                'apellidos.required'=>"El campo Apellidos es requerido",
                'pais_id.required'=>"El campo Pais es requerido",
                'departamento_id.required'=>"El campo Departamento es requerido",
                'ciudad_id.required'=>"El campo Ciudad es requerido",
                'direccion.required'=>"El campo Dirección es requerido",
                'barrio.required'=>"El campo Barrio es requerido",
                'celular.required'=>"El campo Celular es requerido",
                'correo.required'=>"El campo Correo es requerido",
                'identificacion.unique'=>"El campo Identificación ya existe",
                'correo.unique'=>"El campo Correo electrónico ya existe",
            ]);
    
            date_default_timezone_set('America/Bogota');
            $fechahora=time();
            $dateonly=date("Y-m-d", $fechahora);
            $datehour= date("Y-m-d H:i:s", $fechahora);
    
            $admin_empresa->nit = $r->nit;
            $admin_empresa->razon = $r->razon;
            $admin_empresa->regimen_id = $r->regimen_id;
            $admin_empresa->documento_id = $r->documento_id;
            $admin_empresa->identificacion = $r->identificacion;
            $admin_empresa->nombres = $r->nombres;
            $admin_empresa->apellidos = $r->apellidos;
            $admin_empresa->pais_id = $r->pais_id;
            $admin_empresa->departamento_id = $r->departamento_id;
            $admin_empresa->ciudad_id = $r->ciudad_id;
            $admin_empresa->direccion = $r->direccion;
            $admin_empresa->barrio = $r->barrio;
            $admin_empresa->celular = $r->celular;
            $admin_empresa->correo = $r->correo;
    
            $admin_empresa->updated_at = $datehour;
            $admin_empresa->user_update = Auth::id();
            
            $admin_empresa->save();
    
            return back()->with('mensaje', 'Actualización realizada con éxito!');


            
        }else{
            return view('errors.404');
        }
    }


    public function show($id)
    {
        if(Auth::user()->empleado->tipo_persona == '1'){
            


            if(Auth::user()->validar_permiso('adm_empre_show')){
                $admin_empresa = Persona::where('tipo_persona','2')->findOrFail($id);
                $titulo = 'Detalle de Empresa';
                $controlador = "admin_empresas";
                return view('admin_empresas.show',compact('titulo','admin_empresa','controlador'));
            }else{
                return view('errors.access_denied_admin');
            }


            
        }else{
            return view('errors.404');
        }
      
    }
    public function destroy($id)
    {   
        if(Auth::user()->empleado->tipo_persona == '1'){
            


            if(Auth::user()->validar_permiso('adm_empre_delete')){
                $admin_empresa = Persona::findOrFail($id);
                try {
                    $admin_empresa->delete($id);
                    return back()->with('mensaje', 'Registro eliminado con éxito!');
                } 
                catch(QueryException $e) {
                    return back()->with('alerta', 'No se pudo eliminar el registro!');
                } 
            }else{
                return view('errors.access_denied_admin');
            }


            
        }else{
            return view('errors.404');
        }
    }


    public function cargar_ciudades(Request $r){
        $ciudades = Ciudad::where("departamento_id",$r->id)->get();
        $opciones = view('cargar_select',compact('ciudades'))->render();
    	return response()->json(['options'=>$opciones]);
    }
    public function buscar(Request $r){

        $identificacion = $r->id;

        $nombres = "";
        $apellidos = "";
        $celular = "";
        $correo = "";
        $id_admin_empresa = "";

        $admin_empresa = Persona::where('identificacion',$identificacion)->first();
        if($admin_empresa != null){
            $nombres = $admin_empresa->nombres;
            $apellidos = $admin_empresa->apellidos;
            $celular = $admin_empresa->celular;
            $correo = $admin_empresa->correo;
            $id_admin_empresa = $admin_empresa->id;
        }

        return response()->json(['id_admin_empresa'=>$id_admin_empresa,'nombres'=>$nombres,'apellidos'=>$apellidos,
        'celular'=>$celular,'correo'=>$correo,'identificacion'=>$identificacion]);
    }


    public function filtrar_tabla(Request $r){
        $id = $r->id;
        $admin_empresas_tabla = Persona::where('identificacion', 'LIKE', "%$id%")
        ->orWhere('nombres', 'LIKE', "%$id%")
        ->orWhere('apellidos', 'LIKE', "%$id%")
        ->take(10)->get();
        $opciones = view('cargar_tabla',compact('admin_empresas_tabla'))->render();
    	return response()->json(['options'=>$opciones]);
    }

    public function cargar_admin_empresas(Request $r){
        $admin_empresas_tabla = Persona::orderBy('date_new','desc')->take(10)->get();
        $opciones = view('cargar_tabla',compact('admin_empresas_tabla'))->render();
    	return response()->json(['options'=>$opciones]);
    }

}