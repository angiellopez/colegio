<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class PrincipalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        if(Auth::user()->nivel == '9'){
            $title = "Principal";
            $controlador = "principal";
    
            return view('principal.index', compact('title','controlador'));
        }else{
            return view('errors.404');
        }
        
      
    }
}
