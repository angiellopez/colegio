<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Persona;
use App\Tipo_documento;
use App\Departamento;
use App\Ciudad;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class PersonaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    public function listar($tipo_persona,$nom_persona)
    {

            if(($tipo_persona == '1' && $nom_persona == 'docentes') || 
                ($tipo_persona == '2' && $nom_persona == 'estudiantes') || 
                ($tipo_persona == '3' && $nom_persona == 'funcionarios')){

                if(Auth::user()->nivel == '9'){

                    $personas = Persona::where('tipo_persona',$tipo_persona)->get();
                    $title = "Lista de ".$nom_persona;
                    $controlador = $nom_persona;
                    $sin_registros = "No hay {$nom_persona} registrados";
                    $nuevo = route('personas.create',['tipo_persona'=>$tipo_persona,'nom_persona'=>$nom_persona]);
                    return view('personas.listar', compact('nuevo','sin_registros','title','personas','controlador'));

                }else{
                    return view('errors.access_denied_admin');
                }
            }else{
                return view('errors.404');
            }
    }

    public function create($tipo_persona,$nom_persona)
    {

        if(($tipo_persona == '1' && $nom_persona == 'docentes') || 
            ($tipo_persona == '2' && $nom_persona == 'estudiantes') || 
            ($tipo_persona == '3' && $nom_persona == 'funcionarios')){

            if(Auth::user()->validar_permiso('adm_colab_create')){
            
                $accion = route('personas.store',['tipo_persona'=>$tipo_persona,'nom_persona'=>$nom_persona]);
                $metodo = method_field('POST');
                $title = "Crear ".$nom_persona;
                $boton = "Crear";
                $regresar = route('personas.listar',['tipo_persona'=>$tipo_persona,'nom_persona'=>$nom_persona]);
                $controlador = $nom_persona;
                $tipo_documentos = Tipo_documento::all();
                $departamentos = Departamento::orderBy('titulo','asc')->get();
                $ciudades = Ciudad::take(0)->get();
    
                return view('personas.create',compact('departamentos','ciudades','accion','metodo','title','boton','regresar','controlador','tipo_documentos'));
                
            }else{
                return view('errors.access_denied_admin');
            }
        }else{
            return view('errors.404');
        }
    }
    public function edit($id)
    {


        if(Auth::user()->validar_permiso('adm_colab_edit')){

            $persona = Persona::findOrFail($id);
            $nom_persona = "";
            if($persona->tipo_persona == '1') $nom_persona = 'docentes';
            if($persona->tipo_persona == '2') $nom_persona = 'estudiantes';
            if($persona->tipo_persona == '3') $nom_persona = 'funcionarios';

            $accion = route('personas.update',['id'=>$persona->id]);
            $metodo = method_field('PUT');
            $title = "Actualizar ".$nom_persona;
            $boton = "Actualizar";         
            $controlador = $nom_persona;
            $tipo_documentos = Tipo_documento::all();
            $departamentos = Departamento::orderBy('titulo','asc')->get();
            $ciudades = Ciudad::where('departamento_id',$persona->departamento_id)->get();
            $regresar = route('personas.listar',['tipo_persona'=>$persona->tipo_persona,'nom_persona'=>$nom_persona]);


            return view('personas.create',compact('regresar','departamentos','ciudades','accion','metodo','title','boton','persona','controlador','tipo_documentos'));
        }else{
            return view('errors.access_denied_admin');
        }
        

    }
    public function store(Request $r,$tipo_persona,$nom_persona)
    {
        if(($tipo_persona == '1' && $nom_persona == 'docentes') || 
            ($tipo_persona == '2' && $nom_persona == 'estudiantes') || 
            ($tipo_persona == '3' && $nom_persona == 'funcionarios')){
            
            $this->validate($r,[
                'tipo_documento_id'=>'required',
                'identificacion'=>['required','unique:personas,identificacion'],
                'nombres'=>'required',
                'apellidos'=>'required',
                'departamento_id'=>'required',
                'ciudad_id'=>'required',
                'direccion'=>'required',
                'barrio'=>'required',
                'celular'=>'required',
                'correo'=>['required','unique:personas,correo'],
            ],[
                'tipo_documento_id.required'=>"El campo Tipo Documento es requerido",
                'identificacion.required'=>"El campo Identificación es requerido",
                'nombres.required'=>"El campo Nombre es requerido",
                'apellidos.required'=>"El campo Nombre es requerido",
                'departamento_id.required'=>"El campo Departamento es requerido",
                'ciudad_id.required'=>"El campo Ciudad es requerido",
                'direccion.required'=>"El campo Dirección es requerido",
                'barrio.required'=>"El campo Barrio es requerido",
                'celular.required'=>"El campo Celular es requerido",
                'correo.required'=>"El campo Correo es requerido",
                'identificacion.unique'=>"El campo Identificación ya existe",
                'correo.unique'=>"El campo Correo ya existe",
            ]);
    
            date_default_timezone_set('America/Bogota');
            $fechahora=time();
            $dateonly=date("Y-m-d", $fechahora);
            $datehour= date("Y-m-d H:i:s", $fechahora);
    
            $persona = new Persona;
            $persona->tipo_documento_id = $r->tipo_documento_id;
            $persona->identificacion = $r->identificacion;
            $persona->nombres = $r->nombres;
            $persona->apellidos = $r->apellidos;
            $persona->departamento_id = $r->departamento_id;
            $persona->ciudad_id = $r->ciudad_id;
            $persona->direccion = $r->direccion;
            $persona->barrio = $r->barrio;
            $persona->celular = $r->celular;
            $persona->correo = $r->correo;
            $persona->tipo_persona = $tipo_persona;
    
    
            $persona->date_new = $dateonly;
            $persona->created_at = $datehour;
            $persona->updated_at = $datehour;
            $persona->user_new = Auth::id();
            $persona->user_update = Auth::id();
    
            $persona->save();
    
            return redirect()->route('personas.listar',['tipo_persona'=>$tipo_persona,'nom_persona'=>$nom_persona])->with('mensaje_persona', 'Docente registrado con éxito!');

            
        }else{
            return view('errors.404');
        }
    }    
    public function update(Request $r, $id)
    {
            $persona = Persona::findOrfail($id);
            $this->validate($r,[
                'tipo_documento_id'=>'required',
                'identificacion'=>'required|unique:personas,identificacion,'.$persona->id,
                'nombres'=>'required',
                'apellidos'=>'required',
                'departamento_id'=>'required',
                'ciudad_id'=>'required',
                'direccion'=>'required',
                'barrio'=>'required',
                'celular'=>'required',
                'correo'=> 'required|unique:personas,correo,'.$persona->id,
            ],[
                'tipo_documento_id.required'=>"El campo Documento es requerido",
                'identificacion.required'=>"El campo Identificación es requerido",
                'nombres.required'=>"El campo Nombres es requerido",
                'apellidos.required'=>"El campo Apellidos es requerido",
                'departamento_id.required'=>"El campo Departamento es requerido",
                'ciudad_id.required'=>"El campo Ciudad es requerido",
                'direccion.required'=>"El campo Dirección es requerido",
                'barrio.required'=>"El campo Barrio es requerido",
                'celular.required'=>"El campo Celular es requerido",
                'correo.required'=>"El campo Correo es requerido",
                'identificacion.unique'=>"El campo Identificación ya existe",
                'correo.unique'=>"El campo Correo electrónico ya existe",
            ]);
    
            date_default_timezone_set('America/Bogota');
            $fechahora=time();
            $dateonly=date("Y-m-d", $fechahora);
            $datehour= date("Y-m-d H:i:s", $fechahora);
    
            
            $persona->tipo_documento_id = $r->tipo_documento_id;
            $persona->identificacion = $r->identificacion;
            $persona->nombres = $r->nombres;
            $persona->apellidos = $r->apellidos;
            $persona->departamento_id = $r->departamento_id;
            $persona->ciudad_id = $r->ciudad_id;
            $persona->direccion = $r->direccion;
            $persona->barrio = $r->barrio;
            $persona->celular = $r->celular;
            $persona->correo = $r->correo;
    
            $persona->updated_at = $datehour;
            $persona->user_update = Auth::id();
            
            $persona->save();
    
            return back()->with('mensaje', 'Actualización realizada con éxito!');
    }

    public function show($id){
        
        if(Auth::user()->validar_permiso('adm_colab_show')){
            $persona = Persona::findOrFail($id);

            $nom_persona = "";
            if($persona->tipo_persona == '1') $nom_persona = 'docentes';
            if($persona->tipo_persona == '2') $nom_persona = 'estudiantes';
            if($persona->tipo_persona == '3') $nom_persona = 'funcionarios';
            
            $title = 'Detalle '.$nom_persona;
            $controlador = $nom_persona;
            $nuevo = route('personas.create',['tipo_persona'=>$persona->tipo_persona,'nom_persona'=>$nom_persona]);
            $regresar = route('personas.listar',['tipo_persona'=>$persona->tipo_persona,'nom_persona'=>$nom_persona]);
            return view('personas.show',compact('title','persona','controlador','nuevo','regresar'));
        }else{
            return view('errors.access_denied_admin');
        }
      
    }

    public function destroy($id)
    {   
        if(Auth::user()->persona->tipo_persona == '1'){
            


            if(Auth::user()->validar_permiso('adm_colab_delete')){
                $persona = Persona::findOrFail($id);
                try {
                    $persona->delete($id);
                    return back()->with('mensaje', 'Registro eliminado con éxito!');
                } 
                catch(QueryException $e) {
                    return back()->with('alerta', 'No se pudo eliminar el registro!');
                } 
            }else{
                return view('errors.access_denied_admin');
            }


            
        }else{
            return view('errors.404');
        }
    }
    public function cargar_departamentos(Request $r){
        $departamentos = Departamento::where("pais_id",$r->id)->get();
        $opciones = view('cargar_select',compact('departamentos'))->render();
    	return response()->json(['options'=>$opciones]);
    }

    public function cargar_ciudades(Request $r){
        $ciudades = Ciudad::where("departamento_id",$r->id)->get();
        $opciones = view('cargar_select',compact('ciudades'))->render();
    	return response()->json(['options'=>$opciones]);
    }
    public function buscar(Request $r){

        $identificacion = $r->id;

        $nombres = "";
        $apellidos = "";
        $celular = "";
        $correo = "";
        $id_persona = "";

        $persona = Persona::where('identificacion',$identificacion)->first();
        if($persona != null){
            $nombres = $persona->nombres;
            $apellidos = $persona->apellidos;
            $celular = $persona->celular;
            $correo = $persona->correo;
            $id_persona = $persona->id;
        }

        return response()->json(['id_persona'=>$id_persona,'nombres'=>$nombres,'apellidos'=>$apellidos,
        'celular'=>$celular,'correo'=>$correo,'identificacion'=>$identificacion]);
    }


    public function filtrar_tabla(Request $r){
        $id = $r->id;
        $personas_tabla = Persona::where('identificacion', 'LIKE', "%$id%")
        ->orWhere('nombres', 'LIKE', "%$id%")
        ->orWhere('apellidos', 'LIKE', "%$id%")
        ->take(10)->get();
        $opciones = view('cargar_tabla',compact('personas_tabla'))->render();
    	return response()->json(['options'=>$opciones]);
    }

    public function cargar_personas(Request $r){
        if(isset($r->tipo_persona)){
            $personas_tabla = Persona::orderBy('date_new','desc')->where('tipo_persona',$r->tipo_persona)->take(30)->get();

        }else{
            $personas_tabla = Persona::orderBy('date_new','desc')->take(30)->get();

        }
        $opciones = view('cargar_tabla',compact('personas_tabla'))->render();
    	return response()->json(['options'=>$opciones]);
    }

}