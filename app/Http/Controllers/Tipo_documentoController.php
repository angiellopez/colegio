<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Documento;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Auth;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');

class Admin_documentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    public function index()
    {
        if(Auth::user()->empleado->tipo_persona != '1'){
            return view('errors.404');
        }
        if(Auth::user()->validar_permiso('con_doc_list')){
            $admin_documentos = Documento::paginate(20);
            $title = "Lista de Tipo Documentos";
            $controlador = "admin_configuracion";
            return view('admin_documentos.index', compact('title','admin_documentos','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }

    public function create()
    {
        if(Auth::user()->empleado->tipo_persona != '1'){
            return view('errors.404');
        }
        if(Auth::user()->validar_permiso('con_doc_create')){
            
            $accion = url('admin_documentos/guardar');
            $metodo = method_field('POST');
            $title = "Crear Tipo Documento";
            $boton = "Crear";
            $regresar = $regresar = route('admin_documentos.index');
            $controlador = "admin_configuracion";

            return view('admin_documentos.create',compact('accion','metodo','title','boton','regresar','controlador'));
            
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function edit($id)
    {
        if(Auth::user()->empleado->tipo_persona != '1'){
            return view('errors.404');
        }
        if(Auth::user()->validar_permiso('con_doc_edit')){

            $admin_documento = Documento::findOrFail($id);
            $accion = url("admin_documentos/actualizar/{$admin_documento->id}");
            $metodo = method_field('PUT');
            $title = "Actualizar Tipo Documento";
            $boton = "Actualizar";         
            $controlador = "admin_configuracion";

            return view('admin_documentos.create',compact('accion','metodo','title','boton','admin_documento','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
    }
    public function store(Request $r)
    {
        if(Auth::user()->empleado->tipo_persona != '1'){
            return view('errors.404');
        }
        $this->validate($r,[
            'titulo'=>'required',
            'abreviatura'=>'required',
        ],[
            'titulo.required'=>"El campo Nombre es requerido",
            'abreviatura.required'=>"El campo Abreviatura es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $admin_documento = new Documento;
        $admin_documento->titulo = $r->titulo;
        $admin_documento->abreviatura = $r->abreviatura;

        $admin_documento->date_new = $dateonly;
        $admin_documento->created_at = $datehour;
        $admin_documento->updated_at = $datehour;
        $admin_documento->user_new = Auth::id();
        $admin_documento->user_update = Auth::id();

        
        $admin_documento->save();

        return redirect()->route('admin_documentos.index')->with('mensaje', 'Registro ingresado con éxito!');
    }    
    public function update(Request $r, $id)
    {
        if(Auth::user()->empleado->tipo_persona != '1'){
            return view('errors.404');
        }
        $admin_documento = Documento::findOrfail($id);
        $this->validate($r,[
            'titulo'=>'required',
            'abreviatura'=>'required',
        ],[
            'titulo.required'=>"El campo Nombre es requerido",
            'abreviatura.required'=>"El campo Abreviatura es requerido",
        ]);

        date_default_timezone_set('America/Bogota');
        $fechahora=time();
        $dateonly=date("Y-m-d", $fechahora);
        $datehour= date("Y-m-d H:i:s", $fechahora);

        $admin_documento->titulo = $r->titulo;
        $admin_documento->abreviatura = $r->abreviatura;

        $admin_documento->date_new = $admin_documento->date_new;
        $admin_documento->created_at = $admin_documento->created_at;
        $admin_documento->updated_at = $datehour;
        $admin_documento->user_new = $admin_documento->user_new;
        $admin_documento->user_update = Auth::id();
        

        $admin_documento->save();

        return back()->with('mensaje', 'Actualización realizada con éxito!');
    }


    public function show($id)
    {
        if(Auth::user()->empleado->tipo_persona != '1'){
            return view('errors.404');
        }
        if(Auth::user()->validar_permiso('con_doc_show')){
            $admin_documento = Documento::findOrFail($id);
            $title = 'Detalle Tipo Documento';
            $controlador = "admin_configuracion";
            return view('admin_documentos.show',compact('title','admin_documento','controlador'));
        }else{
            return view('errors.access_denied_admin');
        }
      
    }

    public function destroy($id)
    {   
        if(Auth::user()->empleado->tipo_persona != '1'){
            return view('errors.404');
        }
        if(Auth::user()->validar_permiso('con_doc_delete')){
            $title = 'Detalle de admin_documento';
            $admin_documento = Documento::findOrFail($id);
            try {
                $admin_documento->delete($id);
                return back()->with('mensaje', 'Registro eliminado con éxito!');
            } 
            catch(QueryException $e) {
                return back()->with('alerta', 'No se pudo eliminar el registro!');
            } 
        }else{
            return view('errors.access_denied_admin');
        }
    }
}