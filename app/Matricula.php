<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    public $timestamps = false;

    public function ciclo(){
        return $this->belongsTo(Ciclo::class,'ciclo_id','id');
    }
    public function estudiante(){
        return $this->belongsTo(Persona::class,'estudiante_id','id');
    }

}
