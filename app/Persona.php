<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    public $timestamps = false;

    public function tipo_documento(){
        return $this->belongsTo(Tipo_documento::class,'tipo_documento_id','id');
    }

    public function tipo_persona_nombre($tipo_persona){
        $nom_persona = "";
        if($tipo_persona == '1') $nom_persona = 'DOCENTE';
        if($tipo_persona == '2') $nom_persona = 'ESTUDIANTE';
        if($tipo_persona == '3') $nom_persona = 'FUNCIONARIO';
        return $nom_persona;
    }

    
}
