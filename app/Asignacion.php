<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    public $timestamps = false;
    protected $table = "asignaciones";

    public function ciclo(){
        return $this->belongsTo(Ciclo::class,'ciclo_id','id');
    }
    public function docente(){
        return $this->belongsTo(Persona::class,'docente_id','id');
    }
    public function materia(){
        return $this->belongsTo(Materia::class,'materia_id','id');
    }
}
